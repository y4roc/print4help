import { get } from '../../api/Api';
import { Piece } from './Piece';

export class PieceApi {
  public static async fetchList(): Promise<Piece[]> {
    const response = await get('/piece/list');

    if (response.status !== 200) {
      throw new Error(`Unexpected API response ${response.status}`);
    }

    const data = await response.json();
    return data.data.map((entry: any) => Piece.deserialize(entry));
  }

  public static async fetch(articleNumber: string): Promise<Piece> {
    const response = await get(`/piece/${articleNumber}/detail`);

    if (response.status !== 200 && response.status !== 404) {
      throw new Error(`Unexpected API response ${response.status}`);
    }

    const { data, errors } = await response.json();

    if (response.status === 404) {
      return Promise.reject(errors);
    }

    return Piece.deserialize(data);
  }
}
