import React from 'react';
import { Container } from 'react-bootstrap';
import Header from './Header';
import Footer from './Footer';

interface PageProps {
  children: React.ReactNode;
}

class Page extends React.PureComponent<PageProps> {
  render() {
    const { children } = this.props;

    return (
      <>
        <Header />
        <Container className="flex-row">{children}</Container>
        <Footer />
      </>
    );
  }
}

export default Page;
