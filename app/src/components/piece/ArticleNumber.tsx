import React from 'react';
import { Badge } from 'react-bootstrap';

interface Props {
  articleNumber: string;
}

export default function ArticleNumber({ articleNumber }: Props): JSX.Element {
  // todo click to copy
  return <Badge variant="primary">{articleNumber}</Badge>;
}
