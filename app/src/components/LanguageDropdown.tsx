import React from 'react';
import i18next from 'i18next';
import { WithTranslation, withTranslation } from 'react-i18next';
import { Nav, NavDropdown } from 'react-bootstrap';

interface State {
  language: string;
}

class LanguageDropdown extends React.PureComponent<WithTranslation, State> {
  state = {
    language: i18next.language,
  };

  changeLanguage = (language: string): void => {
    const { language: currentLanguage } = this.state;
    if (currentLanguage === language) {
      return;
    }

    i18next.changeLanguage(language).then(() => {
      i18next.options.lng = language;
    });
    this.setState({
      language,
    });
  };

  render() {
    const { t } = this.props;
    const { language } = this.state;

    return (
      <Nav>
        <NavDropdown id="language" title={t('global:nav.change_locale')}>
          <NavDropdown.Item active={language === 'en'} onClick={() => this.changeLanguage('en')}>
            {t('global:nav.locale.en')}
          </NavDropdown.Item>
          <NavDropdown.Item active={language === 'de'} onClick={() => this.changeLanguage('de')}>
            {t('global:nav.locale.de')}
          </NavDropdown.Item>
        </NavDropdown>
      </Nav>
    );
  }
}

export default withTranslation()(LanguageDropdown);
