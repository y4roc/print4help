import React from 'react';
import { WithTranslation, withTranslation } from 'react-i18next';
import { Col, Row, Spinner } from 'react-bootstrap';
import { RouteComponentProps } from 'react-router';
import { Piece as PieceModel } from '../domain/market/Piece';
import { PieceApi } from '../domain/market/PieceApi';
import Page from '../components/global/Page';
import PieceDetail from '../components/piece/PieceDetail';

interface MatchParams {
  articleNumber: string;
}

interface IProps extends RouteComponentProps<MatchParams>, WithTranslation {}

interface IState {
  piece: PieceModel | null;
  loading: boolean;
}

class Piece extends React.PureComponent<IProps, IState> {
  state: IState = {
    piece: null,
    loading: false,
  };

  async componentDidMount() {
    await this.fetchPiece();
  }

  async fetchPiece() {
    const { match } = this.props;
    this.setState({
      loading: true,
    });
    const piece = await PieceApi.fetch(match.params.articleNumber);
    this.setState({
      piece,
      loading: false,
    });
  }

  render() {
    const { piece, loading } = this.state;

    return (
      <Page>
        <Row>
          {loading && (
            <Col className="justify-content-center d-flex">
              <Spinner animation="border" />
            </Col>
          )}
          {!loading && piece && <PieceDetail piece={piece} />}
        </Row>
      </Page>
    );
  }
}

export default withTranslation()(Piece);
