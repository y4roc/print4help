import React from 'react';
import { WithTranslation, withTranslation } from 'react-i18next';
import { Col, Container, Row } from 'react-bootstrap';
import Page from '../components/global/Page';

class Index extends React.PureComponent<WithTranslation, {}> {
  render() {
    return (
      <Page>
        <Container>
          <Row>
            <Col>Index</Col>
          </Row>
        </Container>
      </Page>
    );
  }
}

export default withTranslation()(Index);
