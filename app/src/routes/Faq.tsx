import React from 'react';
import { WithTranslation, withTranslation } from 'react-i18next';
import { Col, Row } from 'react-bootstrap';
import ReactMarkdown from 'react-markdown';
import Page from '../components/global/Page';

class Faq extends React.PureComponent<WithTranslation, {}> {
  render() {
    const { t } = this.props;

    return (
      <Page>
        <Row>
          <Col dangerouslySetInnerHTML={{ __html: t('faq:title') }} />
          <Col>
            <ReactMarkdown>{t('faq:title')}</ReactMarkdown>
          </Col>
        </Row>
      </Page>
    );
  }
}

export default withTranslation()(Faq);
