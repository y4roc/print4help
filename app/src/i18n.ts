import i18next from 'i18next';
import { initReactI18next } from 'react-i18next';
import LanguageDetector from 'i18next-browser-languagedetector';

import catalogueEn from './locales/catalogue.en.json';
import faqEn from './locales/faq.en.json';
import globalEn from './locales/global.en.json';

import catalogueDe from './locales/catalogue.de.json';
import faqDe from './locales/faq.de.json';
import globalDe from './locales/global.de.json';

export const resources = {
  en: {
    catalogue: catalogueEn,
    faq: faqEn,
    global: globalEn,
  },
  de: {
    catalogue: catalogueDe,
    faq: faqDe,
    global: globalDe,
  },
};

const i18n = i18next.use(LanguageDetector).use(initReactI18next);

i18n
  .use(initReactI18next) // passes i18n down to react-i18next
  .init({
    resources,
    lng: 'en',
    ns: ['global', 'faq'],
    keySeparator: '.',
    interpolation: {
      escapeValue: false,
    },
  });

export default i18n;
