<?php

declare(strict_types=1);

namespace App\Domain;

use Ramsey\Uuid\Uuid;
use ReflectionClass;

trait UuidBehaviour
{
    private string $value;

    private function __construct(string $value)
    {
        if (Uuid::isValid($value) === false) {
            throw new UuidNotValidException($value, (new ReflectionClass($this))->getShortName());
        }

        $this->value = $value;
    }

    public static function create(): self
    {
        return new self(UuidGenerator::generate()->toString());
    }

    public static function fromString(string $value): self
    {
        return new self($value);
    }

    public function toString(): string
    {
        return $this->value;
    }

    public function equals(self $other): bool
    {
        return $this->value === $other->value;
    }
}
