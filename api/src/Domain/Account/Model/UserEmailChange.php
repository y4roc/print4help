<?php

declare(strict_types=1);

namespace App\Domain\Account\Model;

use App\Domain\DateTimeHelper;
use App\Domain\Email;
use App\Domain\UuidGenerator;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\UuidInterface;

/**
 * @final
 * @ORM\Entity()
*/
class UserEmailChange
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     */
    private UuidInterface $id;

    /**
     * @ORM\OneToOne(targetEntity="User", inversedBy="emailChange")
     */
    private User $user;

    /**
     * @ORM\Column(type="email", unique=true)
     */
    private Email $email;

    /**
     * @ORM\Column(type="string", unique=true)
     */
    private string $emailChangeToken;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private DateTimeImmutable $emailChangeTokenGenerationDate;

    private function __construct(
        User $user,
        Email $email,
        string $emailChangeToken,
        DateTimeImmutable $emailChangeTokenGenerationDate
    ) {
        $this->id = UuidGenerator::generate();
        $this->user = $user;
        $this->email = $email;
        $this->emailChangeToken = $emailChangeToken;
        $this->emailChangeTokenGenerationDate = $emailChangeTokenGenerationDate;
    }

    public static function create(User $user, Email $email): self
    {
        $emailChangeTokenGenerationDate = DateTimeHelper::create();
        $emailChangeToken = self::generateEmailChangeToken($user, $emailChangeTokenGenerationDate);

        return new self($user, $email, $emailChangeToken, $emailChangeTokenGenerationDate);
    }

    private static function generateEmailChangeToken(User $user, DateTimeImmutable $date): string
    {
        return substr(
            hash(
                'sha1',
                sprintf(
                    '%s-%s',
                    $user->getId()->toString(),
                    $date->format(DateTimeImmutable::ATOM)
                )
            ),
            0,
            10
        );
    }

    public function getEmailChangeToken(): string
    {
        return $this->emailChangeToken;
    }

    public function isEmailChangeTokenValid(): bool
    {
        return DateTimeHelper::create()->modify('-24 hours') < $this->emailChangeTokenGenerationDate;
    }

    public function getEmail(): Email
    {
        return $this->email;
    }

    public function getUser(): User
    {
        return $this->user;
    }
}
