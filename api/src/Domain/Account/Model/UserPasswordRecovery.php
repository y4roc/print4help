<?php

declare(strict_types=1);

namespace App\Domain\Account\Model;

use App\Domain\DateTimeHelper;
use App\Domain\UuidGenerator;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\UuidInterface;

/**
 * @final
 * @ORM\Entity()
*/
class UserPasswordRecovery
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     */
    private UuidInterface $id;

    /**
     * @ORM\OneToOne(targetEntity="User", inversedBy="passwordRecovery")
     */
    private User $user;

    /**
     * @ORM\Column(type="string", unique=true)
     */
    private string $passwordRecoveryToken;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private DateTimeImmutable $passwordRecoveryTokenGenerationDate;

    private function __construct(
        User $user,
        string $passwordRecoveryToken,
        DateTimeImmutable $passwordRecoveryTokenGenerationDate
    ) {
        $this->id = UuidGenerator::generate();
        $this->user = $user;
        $this->passwordRecoveryToken = $passwordRecoveryToken;
        $this->passwordRecoveryTokenGenerationDate = $passwordRecoveryTokenGenerationDate;
    }

    public static function create(User $user): self
    {
        $passwordRecoveryTokenGenerationDate = DateTimeHelper::create();
        $passwordRecoveryToken = self::generatePasswordRecoveryToken($user, $passwordRecoveryTokenGenerationDate);

        return new self($user, $passwordRecoveryToken, $passwordRecoveryTokenGenerationDate);
    }

    private static function generatePasswordRecoveryToken(User $user, DateTimeImmutable $dateTimeImmutable): string
    {
        return substr(
            hash(
                'sha1',
                sprintf(
                    '%s-%s',
                    $user->getId()->toString(),
                    $dateTimeImmutable->format(DateTimeImmutable::ATOM)
                )
            ),
            0,
            10
        );
    }

    public function getPasswordRecoveryToken(): string
    {
        return $this->passwordRecoveryToken;
    }

    public function isPasswordRecoveryTokenValid(): bool
    {
        return DateTimeHelper::create()->modify('-24 hours') < $this->passwordRecoveryTokenGenerationDate;
    }

    public function getUser(): User
    {
        return $this->user;
    }
}
