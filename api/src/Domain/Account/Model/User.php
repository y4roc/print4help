<?php

declare(strict_types=1);

namespace App\Domain\Account\Model;

use App\Domain\Account\UserId;
use App\Domain\DateTimeHelper;
use App\Domain\Email;
use App\Domain\Market\Cart\CartId;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @final
 * @ORM\Entity
 */
class User
{
    /**
     * @ORM\Id
     * @ORM\Column(type="guid")
     */
    private string $id;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $firstName;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $lastName;

    /**
     * @ORM\Column(type="string")
     */
    private string $password;
    /**
     * @ORM\Column(type="email", unique=true)
     */
    private Email $email;

    /**
     * @ORM\Column(type="boolean", options={"default": "0"})
     */
    private bool $active;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $doubleOptInToken = null;

    /**
     * @ORM\Column(type="date_immutable", nullable=true)
     */
    private ?DateTimeImmutable $doubleOptInGenerationDate = null;

    /**
     * @ORM\Column(type="guid", nullable=true)
     */
    private ?string $defaultShippingAddressId;

    /**
     * @var Collection<int, UserAddress>
     * @ORM\OneToMany(targetEntity="UserAddress", mappedBy="user", cascade={"all"}, orphanRemoval=true)
     */
    private Collection $addresses;

    /**
     * @ORM\Column(type="guid", nullable=true)
     */
    private ?string $cartId;

    /**
     * @ORM\OneToOne(targetEntity="App\Domain\Account\Model\UserRegistration", mappedBy="user", cascade={"all"})
     */
    private ?UserRegistration $registration;

    /**
     * @ORM\OneToOne(targetEntity="App\Domain\Account\Model\UserPasswordRecovery", mappedBy="user", cascade={"all"})
     */
    private ?UserPasswordRecovery $passwordRecovery;

    /**
     * @ORM\OneToOne(targetEntity="App\Domain\Account\Model\UserEmailChange", mappedBy="user", cascade={"all"})
     */
    private ?UserEmailChange $emailChange;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private DateTimeImmutable $createdAt;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     */
    private ?DateTimeImmutable $updatedAt = null;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     */
    private ?DateTimeImmutable $registrationFinishedAt = null;

    private function __construct(UserId $userId, Email $email, string $firstName, string $lastName)
    {
        $this->id = $userId->toString();
        $this->email = $email;
        $this->firstName = $firstName;
        $this->lastName = $lastName;

        // base password that gets generated: no user input, different hash algorithm => can never login with it
        $this->password = hash('sha1', sprintf('%s:%s', $this->id, (string)time()));

        $this->addresses = new ArrayCollection();

        $this->active = false;
        $this->registration = null;
        $this->passwordRecovery = null;
        $this->emailChange = null;
        $this->defaultShippingAddressId = null;
        $this->cartId = null;
        $this->createdAt = DateTimeHelper::create();
    }

    public function getId(): UserId
    {
        return UserId::fromString($this->id);
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function getUsername(): string
    {
        return $this->getEmail()->toString();
    }

    public function getEmail(): Email
    {
        return $this->email;
    }

    public function activate(): void
    {
        $this->active = true;
    }

    public function deactivate(): void
    {
        $this->active = false;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public static function create(UserId $userId, Email $email, string $firstName, string $lastName): self
    {
        return new self($userId, $email, $firstName, $lastName);
    }

    public function createRegistration(): void
    {
        $this->registration = UserRegistration::create($this);
    }

    public function getRegistration(): ?UserRegistration
    {
        return $this->registration;
    }

    public function createPasswordRecovery(): void
    {
        $this->passwordRecovery = UserPasswordRecovery::create($this);
    }

    public function getPasswordRecovery(): ?UserPasswordRecovery
    {
        return $this->passwordRecovery;
    }

    public function createEmailChange(Email $email): void
    {
        $this->emailChange = UserEmailChange::create($this, $email);
    }

    public function getEmailChange(): ?UserEmailChange
    {
        return $this->emailChange;
    }

    public function updateName(string $firstName, string $lastName): void
    {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
    }

    public function updatePassword(string $password): void
    {
        $this->password = $password;
    }

    public function finishRegistration(string $password): void
    {
        $this->password = $password;
        $this->registrationFinishedAt = DateTimeHelper::create();
        $this->registration = null;
    }

    public function finishPasswordRecovery(string $password): void
    {
        $this->password = $password;
        $this->passwordRecovery = null;
    }

    public function finishEmailChange(Email $email): void
    {
        $this->email = $email;
        $this->emailChange = null;
    }

    public function getCartId(): ?CartId
    {
        return is_string($this->cartId) ? CartId::fromString($this->cartId) : null;
    }

    public function setCartId(CartId $cartId): void
    {
        $this->cartId = $cartId->toString();
    }
}
