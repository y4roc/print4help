<?php

declare(strict_types=1);

namespace App\Domain\Account\Handler;

use App\Domain\Account\Command\ChangeUserPassword;
use App\Domain\Account\Repository\UserRepository;
use App\Domain\CommandHandlerInterface;

final class ChangeUserPasswordHandler implements CommandHandlerInterface
{
    public function __construct(private UserRepository $userRepository)
    {
    }

    public function __invoke(ChangeUserPassword $changeUserPassword): void
    {
        $user = $this->userRepository->get($changeUserPassword->getUserId());
        $user->updatePassword($changeUserPassword->getNewPassword());

        $this->userRepository->save($user);
    }
}
