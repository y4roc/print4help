<?php

declare(strict_types=1);

namespace App\Domain\Account\Handler;

use App\Domain\Account\Command\FinishUserEmailChange;
use App\Domain\Account\Repository\UserRepository;
use App\Domain\CommandHandlerInterface;

final class FinishUserEmailChangeHandler implements CommandHandlerInterface
{
    private UserRepository $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function __invoke(FinishUserEmailChange $command): void
    {
        $userEmailChange = $this->userRepository->getEmailChangeByToken($command->getEmailChangeToken());

        $user = $userEmailChange->getUser();

        $user->finishEmailChange($command->getEmail());

        $this->userRepository->save($user);

        // todo send email to user that new email is confirmed
    }
}
