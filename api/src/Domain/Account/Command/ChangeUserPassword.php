<?php

declare(strict_types=1);

namespace App\Domain\Account\Command;

use App\Domain\Account\UserId;

final class ChangeUserPassword
{
    private string $userId;
    private string $newPassword;

    public function __construct(UserId $userId, string $newPassword)
    {
        $this->userId = $userId->toString();
        $this->newPassword = $newPassword;
    }

    public function getUserId(): UserId
    {
        return UserId::fromString($this->userId);
    }

    public function getNewPassword(): string
    {
        return $this->newPassword;
    }
}
