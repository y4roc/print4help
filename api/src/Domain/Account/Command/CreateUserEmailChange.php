<?php

declare(strict_types=1);

namespace App\Domain\Account\Command;

use App\Domain\Account\UserId;
use App\Domain\Email;

final class CreateUserEmailChange
{
    private string $userId;
    private string $email;

    public function __construct(UserId $userId, Email $email)
    {
        $this->userId = $userId->toString();
        $this->email = $email->toString();
    }

    public function getUserId(): UserId
    {
        return UserId::fromString($this->userId);
    }

    public function getEmail(): Email
    {
        return Email::fromString($this->email);
    }
}
