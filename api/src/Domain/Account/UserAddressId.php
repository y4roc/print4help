<?php

declare(strict_types=1);

namespace App\Domain\Account;

use App\Domain\UuidBehaviour;

class UserAddressId
{
    use UuidBehaviour;
}
