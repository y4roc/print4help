<?php

declare(strict_types=1);

namespace App\Domain\Account\Exception;

use App\Domain\Account\UserAddressId;
use App\Domain\Account\UserId;
use App\Domain\DomainException;

final class UserAddressNotFoundInUser extends DomainException
{
    public function __construct(UserAddressId $userAddressId, UserId $userId)
    {
        parent::__construct(sprintf(
            'The UserAddress with id [%s] does not exist for User with id [%s]',
            $userAddressId->toString(),
            $userId->toString()
        ));
    }
}
