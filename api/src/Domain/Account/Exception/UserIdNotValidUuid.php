<?php

declare(strict_types=1);

namespace App\Domain\Account\Exception;

use App\Domain\DomainException;
use Throwable;

final class UserIdNotValidUuid extends DomainException
{
    public function __construct(string $userId, Throwable $previous = null)
    {
        parent::__construct(sprintf('UserId [%s] is not a valid Uuid', $userId), 0, $previous);
    }
}
