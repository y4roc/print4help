<?php

declare(strict_types=1);

namespace App\Domain\Account\Exception;

use App\Domain\DomainException;
use Throwable;

final class UserEmailChangeByEmailChangeTokenDoesNotExist extends DomainException
{
    public function __construct(string $token, Throwable $previous = null)
    {
        parent::__construct(sprintf('A UserEmailChange with EmailChangeToken [%s] does not exist', $token), 0, $previous);
    }
}
