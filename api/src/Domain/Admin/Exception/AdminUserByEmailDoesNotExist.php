<?php

declare(strict_types=1);

namespace App\Domain\Admin\Exception;

use App\Domain\DomainException;
use App\Domain\Email;
use Throwable;

final class AdminUserByEmailDoesNotExist extends DomainException
{
    public function __construct(Email $email, Throwable $previous = null)
    {
        parent::__construct(sprintf('AdminUser with Email [%s] does not exist', $email->toString()), 0, $previous);
    }
}
