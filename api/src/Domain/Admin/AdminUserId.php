<?php

declare(strict_types=1);

namespace App\Domain\Admin;

use App\Domain\UuidBehaviour;

class AdminUserId
{
    use UuidBehaviour;
}
