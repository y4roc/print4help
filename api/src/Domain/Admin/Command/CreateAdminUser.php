<?php

declare(strict_types=1);

namespace App\Domain\Admin\Command;

use App\Domain\Admin\AdminUserId;
use App\Domain\Email;

final class CreateAdminUser
{
    private string $adminUserId;
    private string $email;
    private string $password;

    public function __construct(AdminUserId $userId, Email $email, string $password)
    {
        $this->adminUserId = $userId->toString();
        $this->email = $email->toString();
        $this->password = $password;
    }

    public function getAdminUserId(): AdminUserId
    {
        return AdminUserId::fromString($this->adminUserId);
    }

    public function getEmail(): Email
    {
        return Email::fromString($this->email);
    }

    public function getPassword(): string
    {
        return $this->password;
    }
}
