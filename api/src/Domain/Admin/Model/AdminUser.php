<?php

declare(strict_types=1);

namespace App\Domain\Admin\Model;

use App\Domain\Admin\AdminUserId;
use App\Domain\DateTimeHelper;
use App\Domain\Email;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;

/**
 * @final
 * @ORM\Entity()
 */
class AdminUser
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="guid")
     */
    private string $id;

    /**
     * @ORM\Column(type="email", unique=true)
     */
    private Email $email;

    /**
     * @ORM\Column(type="string")
     */
    private string $password;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private DateTimeImmutable $createdAt;

    public function __construct(AdminUserId $id, Email $email, DateTimeImmutable $createdAt)
    {
        $this->id = $id->toString();
        $this->email = $email;
        $this->password = '';
        $this->createdAt = $createdAt;
    }

    public static function create(AdminUserId $id, Email $email): self
    {
        return new self($id, $email, DateTimeHelper::create());
    }

    public function getId(): AdminUserId
    {
        return AdminUserId::fromString($this->id);
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function updatePassword(string $password): void
    {
        $this->password = $password;
    }

    public function getEmail(): Email
    {
        return $this->email;
    }

    // please dont add methods under here, only above
    // here comes methods only used by symfony security
    public function getUsername(): string
    {
        return $this->getEmail()->toString();
    }

    public function getSalt(): string
    {
        return '';
    }

    public function eraseCredentials(): void
    {
        // nothing
    }
}
