<?php

declare(strict_types=1);

namespace App\Domain\Admin\Repository;

use App\Domain\Admin\AdminUserId;
use App\Domain\Admin\Exception\AdminUserByAdminUserIdDoesNotExist;
use App\Domain\Admin\Exception\AdminUserByEmailDoesNotExist;
use App\Domain\Admin\Model\AdminUser;
use App\Domain\Email;

interface AdminUserRepository
{
    /**
     * @throws AdminUserByAdminUserIdDoesNotExist
     */
    public function get(AdminUserId $adminUserId): AdminUser;

    /**
     * @throws AdminUserByEmailDoesNotExist
     */
    public function getByEmail(Email $email): AdminUser;

    public function save(AdminUser $adminUser): void;

    public function remove(AdminUser $adminUser): void;
}
