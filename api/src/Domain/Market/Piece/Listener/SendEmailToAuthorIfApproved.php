<?php

declare(strict_types=1);

namespace App\Domain\Market\Piece\Listener;

use App\Domain\Market\Piece\Event\PieceApproved;
use Patchlevel\EventSourcing\Aggregate\AggregateChanged;
use Patchlevel\EventSourcing\EventBus\Listener;

class SendEmailToAuthorIfApproved implements Listener
{
    public function __invoke(AggregateChanged $event): void
    {
        if (!$event instanceof PieceApproved) {
            return;
        }

        // todo: send email to maker
    }
}
