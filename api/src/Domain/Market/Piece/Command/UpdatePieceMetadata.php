<?php

declare(strict_types=1);

namespace App\Domain\Market\Piece\Command;

use App\Domain\Market\Piece\CategoryId;
use App\Domain\Market\Piece\PieceId;

class UpdatePieceMetadata
{
    private string $pieceId;
    private string $name;
    private ?string $summary;
    private ?string $description;
    private ?string $manufacturingNotes;
    private ?string $licence;
    private ?string $sourceUrl;
    /**
     * @var array<int, string>
     */
    private array $categoryIds;

    /**
     * @param array<int, CategoryId> $categoryIds
     */
    public function __construct(
        PieceId $pieceId,
        string $name,
        ?string $summary,
        ?string $description,
        ?string $manufacturingNotes,
        ?string $licence,
        ?string $sourceUrl,
        array $categoryIds = []
    ) {
        $this->pieceId = $pieceId->toString();
        $this->name = $name;
        $this->summary = $summary;
        $this->description = $description;
        $this->manufacturingNotes = $manufacturingNotes;
        $this->licence = $licence;
        $this->sourceUrl = $sourceUrl;
        $this->categoryIds = array_map(fn (CategoryId $categoryId) => $categoryId->toString(), $categoryIds);
    }

    public function pieceId(): PieceId
    {
        return PieceId::fromString($this->pieceId);
    }

    public function name(): string
    {
        return $this->name;
    }

    public function summary(): ?string
    {
        return $this->summary;
    }

    public function description(): ?string
    {
        return $this->description;
    }

    public function manufacturingNotes(): ?string
    {
        return $this->manufacturingNotes;
    }

    public function licence(): ?string
    {
        return $this->licence;
    }

    public function sourceUrl(): ?string
    {
        return $this->sourceUrl;
    }

    /**
     * @return array<int, CategoryId>
     */
    public function categoryIds(): array
    {
        return array_map(fn (string $categoryId) => CategoryId::fromString($categoryId), $this->categoryIds);
    }
}
