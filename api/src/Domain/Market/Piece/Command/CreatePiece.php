<?php

declare(strict_types=1);

namespace App\Domain\Market\Piece\Command;

use App\Domain\Account\UserId;
use App\Domain\Market\Piece\ArticleNumber;
use App\Domain\Market\Piece\PieceId;

class CreatePiece
{
    private string $userId;
    private string $pieceId;
    private string $name;
    private string $articleNumber;

    public function __construct(UserId $userId, PieceId $pieceId, ArticleNumber $articleNumber, string $name)
    {
        $this->userId = $userId->toString();
        $this->pieceId = $pieceId->toString();
        $this->articleNumber = $articleNumber->toString();
        $this->name = $name;
    }

    public function userId(): UserId
    {
        return UserId::fromString($this->userId);
    }

    public function pieceId(): PieceId
    {
        return PieceId::fromString($this->pieceId);
    }

    public function arcticleNumber(): ArticleNumber
    {
        return ArticleNumber::fromString($this->articleNumber);
    }

    public function name(): string
    {
        return $this->name;
    }
}
