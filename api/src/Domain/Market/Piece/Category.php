<?php

declare(strict_types=1);

namespace App\Domain\Market\Piece;

class Category
{
    private CategoryId $id;
    private string $name;
    /**
     * @var array<string, Category>
     */
    private array $children;
    private ?Category $parent;

    /**
     * @param array<string, Category> $children
     */
    public function __construct(
        CategoryId $id,
        array $children = []
    ) {
        $this->id = $id;
        $this->name = $id->toString();
        $this->children = $children;
        $this->parent = null;

        foreach ($children as $child) {
            $child->setParent($this);
        }
    }

    public function name(): string
    {
        return $this->name;
    }

    public function id(): CategoryId
    {
        return $this->id;
    }

    /**
     * @return array<int, CategoryId>
     */
    public function allParentIds(): array
    {
        if (!$this->parent instanceof self) {
            return [$this->id];
        }

        $allIds = [
            $this->id,
            ...$this->parent->allParentIds(),
        ];

        usort($allIds, static function (CategoryId $a, CategoryId $b): int {
            return strnatcmp($a->toString(), $b->toString());
        });

        return $allIds;
    }

    /**
     * @return array<int, CategoryId|string>
     */
    public function allParentIdsAsString(): array
    {
        return array_map(static fn (CategoryId $categoryId): string => $categoryId->toString(), $this->allParentIds());
    }

    public function parentName(): string
    {
        if (!$this->parent instanceof self) {
            return $this->name;
        }

        $parentKey = $this->parent->parentName();

        return sprintf('%s.%s', $parentKey, $this->name);
    }

    public function setParent(Category $parent): void
    {
        $this->parent = $parent;
    }

    /**
     * @return array<string, Category>
     */
    public function children(): array
    {
        return $this->children;
    }

    public function hasChildren(): bool
    {
        return count($this->children) > 0;
    }

    /**
     * @return array<int, CategoryId>
     */
    public function allChildrenIds(): array
    {
        $allIds = [];
        foreach ($this->children as $child) {
            $allIds[] = [
                $child->id(),
                ...$child->allChildrenIds(),
            ];
        }

        $merged = array_merge(...$allIds);

        usort($merged, static function (CategoryId $a, CategoryId $b): int {
            return strnatcmp($a->toString(), $b->toString());
        });

        return $merged;
    }

    /**
     * @return array<int, string>
     */
    public function allChildrenIdsAsString(): array
    {
        return array_map(fn (CategoryId $categoryId): string => $categoryId->toString(), $this->allChildrenIds());
    }

    /**
     * @param CategoryId|string $categoryId
     */
    public function hasChild(mixed $categoryId): bool
    {
        if ($categoryId instanceof CategoryId) {
            return in_array($categoryId->toString(), $this->allChildrenIdsAsString());
        }
        return in_array($categoryId, $this->allChildrenIdsAsString(), true);
    }

    public function getChild(CategoryId $categoryId): ?Category
    {
        foreach ($this->children as $child) {
            if ($child->id()->toString() === $categoryId->toString()) {
                return $child;
            }
            if ($child->hasChild($categoryId)) {
                return $child->getChild($categoryId);
            }
        }

        return null;
    }
}
