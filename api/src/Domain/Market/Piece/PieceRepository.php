<?php

declare(strict_types=1);

namespace App\Domain\Market\Piece;

interface PieceRepository
{
    public function get(PieceId $pieceId): Piece;
    public function save(Piece $piece): void;
}
