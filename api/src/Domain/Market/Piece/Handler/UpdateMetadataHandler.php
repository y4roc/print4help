<?php declare(strict_types=1);

namespace App\Domain\Market\Piece\Handler;

use App\Domain\CommandHandlerInterface;
use App\Domain\Market\Piece\Command\UpdatePieceMetadata;
use App\Domain\Market\Piece\PieceRepository;

class UpdateMetadataHandler implements CommandHandlerInterface
{
    public function __construct(private PieceRepository $pieceRepository)
    {
    }

    public function __invoke(UpdatePieceMetadata $command): void
    {
        $piece = $this->pieceRepository->get($command->pieceId());

        $piece->updateMetadata(
            $command->name(),
            $command->summary(),
            $command->description(),
            $command->manufacturingNotes(),
            $command->licence(),
            $command->sourceUrl(),
            $command->categoryIds()
        );

        $this->pieceRepository->save($piece);
    }
}
