<?php declare(strict_types=1);

namespace App\Domain\Market\Piece\Handler;

use App\Domain\CommandHandlerInterface;
use App\Domain\Market\Piece\Command\CreatePiece;
use App\Domain\Market\Piece\Piece;
use App\Domain\Market\Piece\PieceRepository;

class CreatePieceHandler implements CommandHandlerInterface
{
    public function __construct(private PieceRepository $pieceRepository)
    {
    }

    public function __invoke(CreatePiece $command): void
    {
        $piece = Piece::create(
            $command->userId(),
            $command->pieceId(),
            $command->arcticleNumber(),
            $command->name()
        );

        $this->pieceRepository->save($piece);
    }
}
