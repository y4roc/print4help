<?php

declare(strict_types=1);

namespace App\Domain\Market\Piece;

use App\Domain\Account\UserId;
use App\Domain\Market\Piece\Event\MetadataUpdated;
use App\Domain\Market\Piece\Event\PieceApproved;
use App\Domain\Market\Piece\Event\PieceBlocked;
use App\Domain\Market\Piece\Event\PieceCreated;
use App\Domain\Market\Piece\Event\PieceRejected;
use App\Domain\Market\Piece\Event\PieceReviewRequested;
use DateTimeImmutable;
use Patchlevel\EventSourcing\Aggregate\AggregateRoot;
use Webmozart\Assert\Assert;

class Piece extends AggregateRoot
{
    private PieceId $id;
    private ArticleNumber $articleNumber;
    private PieceStatus $status;
    private UserId $userId;
    private string $name;
    private ?string $summary;
    private ?string $description;
    private ?string $manufacturingNotes;
    private ?string $licence;
    private ?string $sourceUrl;
    /**
     * @var array<int, CategoryId>
     */
    private ?array $categoryIds;
    private ?DateTimeImmutable $createdAt;
    private ?DateTimeImmutable $updatedAt;

    public static function create(UserId $userId, PieceId $pieceId, ArticleNumber $articleNumber, string $name): self
    {
        $self = new self();
        Assert::notEmpty($name);
        Assert::minLength($name, 3);
        Assert::maxLength($name, 255);

        $self->apply(PieceCreated::raise($userId, $pieceId, $articleNumber, $name, PieceStatus::draft()));

        return $self;
    }

    public function aggregateRootId(): string
    {
        return $this->id->toString();
    }

    /**
     * @param array<int, CategoryId> $categoryIds
     */
    public function updateMetadata(
        string $name,
        ?string $summary,
        ?string $description,
        ?string $manufacturingNotes,
        ?string $licence,
        ?string $sourceUrl,
        array $categoryIds = []
    ): void {
        Assert::nullOrMaxLength($name, 255);
        Assert::nullOrMaxLength($summary, 2000);
        Assert::nullOrMaxLength($description, 10000);
        Assert::nullOrMaxLength($manufacturingNotes, 10000);
        Assert::nullOrMaxLength($licence, 10000);
        Assert::nullOrRegex($sourceUrl, '/https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&\/=]*)/');
        Assert::allIsInstanceOf($categoryIds, CategoryId::class);

        $this->apply(
            MetadataUpdated::raise(
                $this->id,
                $name,
                $summary,
                $description,
                $manufacturingNotes,
                $licence,
                $sourceUrl,
                $categoryIds
            )
        );
    }

    public function requestReview(?string $message): void
    {
        $this->apply(PieceReviewRequested::raise($this->id, $message));
    }

    public function approve(?string $message): void
    {
        $this->apply(PieceApproved::raise($this->id, $message));
    }

    public function reject(?string $message): void
    {
        $this->apply(PieceRejected::raise($this->id, $message));
    }

    public function block(?string $message): void
    {
        $this->apply(PieceBlocked::raise($this->id, $message));
    }

    protected function applyPieceCreated(PieceCreated $event): void
    {
        $this->id = $event->pieceId();
        $this->userId = $event->userId();
        $this->name = $event->name();
        $this->articleNumber = $event->articleNumber();
        $this->status = $event->status();
        $this->createdAt = $event->createdAt();
    }

    protected function applyMetaDataUpdated(MetadataUpdated $event): void
    {
        $this->name = $event->name();
        $this->summary = $event->summary();
        $this->description = $event->description();
        $this->manufacturingNotes = $event->manufacturingNotes();
        $this->licence = $event->licence();
        $this->sourceUrl = $event->sourceUrl();
        $this->updatedAt = $event->updatedAt();
        $this->categoryIds = $event->categoryIds();
    }

    protected function applyPieceReviewRequested(PieceReviewRequested $event): void
    {
        $this->status = $event->status();
        $this->updatedAt = $event->updatedAt();
    }

    protected function applyPieceBlocked(PieceBlocked $event): void
    {
        $this->status = $event->status();
        $this->updatedAt = $event->updatedAt();
    }

    protected function applyPieceApproved(PieceApproved $event): void
    {
        $this->status = $event->status();
        $this->updatedAt = $event->updatedAt();
    }

    protected function applyPieceRejected(PieceRejected $event): void
    {
        $this->status = $event->status();
        $this->updatedAt = $event->updatedAt();
    }

    public function id(): PieceId
    {
        return $this->id;
    }

    public function articleNumber(): ArticleNumber
    {
        return $this->articleNumber;
    }

    public function getId(): PieceId
    {
        return $this->id;
    }

    public function status(): PieceStatus
    {
        return $this->status;
    }

    public function userId(): UserId
    {
        return $this->userId;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function summary(): ?string
    {
        return $this->summary;
    }

    public function description(): ?string
    {
        return $this->description;
    }

    public function manufacturingNotes(): ?string
    {
        return $this->manufacturingNotes;
    }

    public function licence(): ?string
    {
        return $this->licence;
    }

    public function sourceUrl(): ?string
    {
        return $this->sourceUrl;
    }

    /**
     * @return array<int, CategoryId>
     */
    public function getCategoryIds(): ?array
    {
        return $this->categoryIds;
    }
}
