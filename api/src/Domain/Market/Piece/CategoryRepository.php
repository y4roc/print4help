<?php

declare(strict_types=1);

namespace App\Domain\Market\Piece;

use App\Domain\Market\Piece\Exception\CategoryByIdNotFound;

class CategoryRepository
{
    /**
     * @var array<string, Category>
     */
    private array $categories;

    public function __construct()
    {
        $this->categories = [
            CategoryId::ART_DESIGN => new Category(CategoryId::artDesign(), [
                CategoryId::ART_DESIGN_SCUPLTURES => new Category(CategoryId::artDesign_Sculptures()),
                CategoryId::ART_DESIGN_WALL_MOUNTED => new Category(CategoryId::artDesign_Wall_Mounted()),
            ]),
            CategoryId::COSTUMES_ACCESSORIES => new Category(CategoryId::costumesAccessories(), [
                CategoryId::COSTUMES_ACCESSORIES_MASKS => new Category(CategoryId::costumesAccessories_Masks()),
                CategoryId::COSTUMES_ACCESSORIES_PROPS => new Category(CategoryId::costumesAccessories_Props()),
                CategoryId::COSTUMES_ACCESSORIES_OTHER => new Category(CategoryId::costumesAccessories_Other()),
            ]),
            CategoryId::FASHION => new Category(CategoryId::fashion(), [
                CategoryId::FASHION_MEN => new Category(CategoryId::fashion_Men()),
                CategoryId::FASHION_WOMEN => new Category(CategoryId::fashion_Women()),
                CategoryId::FASHION_ACCESSORIES => new Category(CategoryId::fashion_Accessories()),
                CategoryId::FASHION_BRACELETS => new Category(CategoryId::fashion_Bracelets()),
                CategoryId::FASHION_JEWELERY => new Category(CategoryId::fashion_Jewelery()),
                CategoryId::FASHION_RINGS => new Category(CategoryId::fashion_Rings()),
                CategoryId::FASHION_OTHER => new Category(CategoryId::fashion_Other()),
            ]),
            CategoryId::GADGET => new Category(CategoryId::gadget(), [
                CategoryId::GADGET_AUDIO => new Category(CategoryId::gadget_Audio()),
                CategoryId::GADGET_COMPUTER => new Category(CategoryId::gadget_Computer()),
                CategoryId::GADGET_PHOTO_VIDEO => new Category(CategoryId::gadget_PhotoVideo()),
                CategoryId::GADGET_PORTABLES => new Category(CategoryId::gadget_Portables()),
                CategoryId::GADGET_MOBILE => new Category(CategoryId::gadget_Mobile()),
                CategoryId::GADGET_OTHER => new Category(CategoryId::gadget_Other()),
            ]),
            CategoryId::HEALTHCARE => new Category(CategoryId::healthcare(), [
                CategoryId::HEALTHCARE_HOME_MEDICAL_TOOLS => new Category(CategoryId::healthcare_HomeMedicalTools()),
                CategoryId::HEALTHCARE_MEDICAL_TOOLS => new Category(CategoryId::healthcare_MedicalTools()),
            ]),
            CategoryId::HOBBY_MAKERS => new Category(CategoryId::hobbyMakers(), [
                CategoryId::HOBBY_MAKERS_AUTOMOTIVE => new Category(CategoryId::hobbyMakers_Automotive()),
                CategoryId::HOBBY_MAKERS_ELECTRONICS => new Category(CategoryId::hobbyMakers_Electronics()),
                CategoryId::HOBBY_MAKERS_MECHANICAL_PARTS => new Category(CategoryId::hobbyMakers_MechanicalParts()),
                CategoryId::HOBBY_MAKERS_MUSIC => new Category(CategoryId::hobbyMakers_Music()),
                CategoryId::HOBBY_MAKERS_ORGANIZERS => new Category(CategoryId::hobbyMakers_Organizers()),
                CategoryId::HOBBY_MAKERS_RC_ROBOTICS => new Category(CategoryId::hobbyMakers_RcRobotics()),
                CategoryId::HOBBY_MAKERS_TOOLS => new Category(CategoryId::hobbyMakers_Tools()),
                CategoryId::HOBBY_MAKERS_OTHER => new Category(CategoryId::hobbyMakers_Other()),
            ]),
            CategoryId::HOUSEHOLD => new Category(CategoryId::household(), [
                CategoryId::HOUSEHOLD_BATHROOM => new Category(CategoryId::household_Bathroom()),
                CategoryId::HOUSEHOLD_BEDROOM => new Category(CategoryId::household_Bedroom()),
                CategoryId::HOUSEHOLD_HOME_DECOR => new Category(CategoryId::household_HomeDecor()),
                CategoryId::HOUSEHOLD_KITCHEN => new Category(CategoryId::household_Kitchen()),
                CategoryId::HOUSEHOLD_LIVING_ROOM => new Category(CategoryId::household_LivingRoom()),
                CategoryId::HOUSEHOLD_OFFICE => new Category(CategoryId::household_Office()),
                CategoryId::HOUSEHOLD_OUTDOOR_GARDEN => new Category(CategoryId::household_OutdoorGarden()),
                CategoryId::HOUSEHOLD_OTHER => new Category(CategoryId::household_Other()),
                CategoryId::HOUSEHOLD_PETS => new Category(CategoryId::household_Pets()),
            ]),
            CategoryId::LEARNING => new Category(CategoryId::learning(), [
                CategoryId::LEARNING_CHEMISTRY => new Category(CategoryId::learning_Chemistry()),
                CategoryId::LEARNING_BIOLOGY => new Category(CategoryId::learning_Biology()),
                CategoryId::LEARNING_PHYSICS => new Category(CategoryId::learning_Physics()),
                CategoryId::LEARNING_ASTRONOMY => new Category(CategoryId::learning_Astronomy()),
                CategoryId::LEARNING_MATHS => new Category(CategoryId::learning_Maths()),
                CategoryId::LEARNING_ENGINEERING => new Category(CategoryId::learning_Engineering()),
                CategoryId::LEARNING_HAPTIC_MODELS => new Category(CategoryId::learning_HapticModels()),
                CategoryId::LEARNING_OTHER => new Category(CategoryId::learning_Other()),
            ]),
            CategoryId::MODELS => new Category(CategoryId::models(), [
                CategoryId::MODELS_ANIMALS => new Category(CategoryId::models_Animals()),
                CategoryId::MODELS_BUILDINGS_STRUCTURES => new Category(CategoryId::models_BuildingStructures()),
                CategoryId::MODELS_CREATURES => new Category(CategoryId::models_Creatures()),
                CategoryId::MODELS_FOOD_DRINK => new Category(CategoryId::models_FoodDrink()),
                CategoryId::MODELS_FURNITURE => new Category(CategoryId::models_Furniture()),
                CategoryId::MODELS_ROBOTS => new Category(CategoryId::models_Robots()),
                CategoryId::MODELS_PEOPLE => new Category(CategoryId::models_People()),
                CategoryId::MODELS_PROPS => new Category(CategoryId::models_Props()),
                CategoryId::MODELS_VEHICLES => new Category(CategoryId::models_Vehicles()),
            ]),
            CategoryId::SEASONAL => new Category(CategoryId::seasonal(), [
                CategoryId::SEASONAL_AUTUMN_HALLOWEEN => new Category(CategoryId::seasonal_AutumnHalloween()),
                CategoryId::SEASONAL_SPRING => new Category(CategoryId::seasonal_Spring()),
                CategoryId::SEASONAL_SUMMER => new Category(CategoryId::seasonal_Summer()),
                CategoryId::SEASONAL_WINTER_CHRISTMAS => new Category(CategoryId::seasonal_WinterChristmas()),
            ]),
            CategoryId::SPORTS_OUTDOOR => new Category(CategoryId::sportsOutdoor(), [
                CategoryId::SPORTS_OUTDOOR_CAMPING => new Category(CategoryId::sportsOutdoor_Camping()),
                CategoryId::SPORTS_OUTDOOR_INDOOR_SPORTS => new Category(CategoryId::sportsOutdoor_IndoorSports()),
                CategoryId::SPORTS_OUTDOOR_OUTDOOR_SPORTS => new Category(CategoryId::sportsOutdoor_OutdoorSports()),
                CategoryId::SPORTS_OUTDOOR_OTHER => new Category(CategoryId::sportsOutdoor_Other()),
                CategoryId::SPORTS_OUTDOOR_WINTER => new Category(CategoryId::sportsOutdoor_Winter()),
            ]),
            CategoryId::TOOLS => new Category(CategoryId::tools(), [
                CategoryId::TOOLS_HAND => new Category(CategoryId::tools_Hand()),
                CategoryId::TOOLS_MACHINE => new Category(CategoryId::tools_Machine()),
                CategoryId::TOOLS_HOLDERS_BOXES => new Category(CategoryId::tools_HoldersBoxes()),
            ]),
            CategoryId::TOYS_GAMES => new Category(CategoryId::toysGames(), [
                CategoryId::TOYS_GAMES_ACTION_FIGURES => new Category(CategoryId::toysGames_ActionFigures()),
                CategoryId::TOYS_GAMES_STATUES => new Category(CategoryId::toysGames_Statues()),
                CategoryId::TOYS_GAMES_BOARD_GAMES => new Category(CategoryId::toysGames_BoardGames()),
                CategoryId::TOYS_GAMES_BUILDING_TOYS => new Category(CategoryId::toysGames_BuildingToys()),
                CategoryId::TOYS_GAMES_OUTDOOR_TOYS => new Category(CategoryId::toysGames_OutdoorToys()),
                CategoryId::TOYS_GAMES_PUZZLES => new Category(CategoryId::toysGames_Puzzles()),
                CategoryId::TOYS_GAMES_RPG_FIGURES => new Category(CategoryId::toysGames_RgpFigures()),
                CategoryId::TOYS_GAMES_VEHICLES => new Category(CategoryId::toysGames_Vehicles()),
                CategoryId::TOYS_GAMES_OTHER => new Category(CategoryId::toysGames_Other()),
            ]),
            CategoryId::WORLD_SCANS => new Category(CategoryId::worldScans(), [
                CategoryId::WORLD_SCANS_ARCHITECTURE => new Category(CategoryId::worldScans_Architecture()),
                CategoryId::WORLD_SCANS_HISTORICAL => new Category(CategoryId::worldScans_Historical()),
                CategoryId::WORLD_SCANS_PEOPLE => new Category(CategoryId::worldScans_People()),
            ]),
        ];
    }

    /**
     * @return array<int, Category>
     */
    public function getAll(): array
    {
        return array_values($this->categories);
    }

    public function getByCategoryId(CategoryId $categoryId): Category
    {
        foreach ($this->categories as $key => $category) {
            if ($key === $categoryId->toString()) {
                return $category;
            }
            if ($category->hasChild($categoryId->toString())) {
                $child = $category->getChild($categoryId);
                if (!$child instanceof Category) {
                    throw new CategoryByIdNotFound($categoryId->toString());
                }
                return $child;
            }
        }
        throw new CategoryByIdNotFound($categoryId->toString());
    }

    /**
     * @param array<int, CategoryId> $categoryIds
     * @return array<int, string>
     */
    public function getCategoryIdsWithAllParentIds(array $categoryIds): array
    {
        // collect for the current categoryId all parent ids so the piece
        $allCategoryIds = [];
        foreach ($categoryIds as $categoryId) {
            $category = $this->getByCategoryId($categoryId);

            /** @var array<int, string> $parentIds */
            $parentIds = $category->allParentIdsAsString();
            $allCategoryIds = [...$allCategoryIds, ...$parentIds];
        }

        $uniqueIds = array_unique($allCategoryIds);
        sort($uniqueIds);
        return $uniqueIds;
    }
}
