<?php declare(strict_types=1);

namespace App\Domain\Market\Piece\Event;

use App\Domain\Account\UserId;
use App\Domain\DateTimeHelper;
use App\Domain\Market\Piece\ArticleNumber;
use App\Domain\Market\Piece\PieceId;
use App\Domain\Market\Piece\PieceStatus;
use DateTimeImmutable;
use Patchlevel\EventSourcing\Aggregate\AggregateChanged;

class PieceCreated extends AggregateChanged
{
    public static function raise(
        UserId $userId,
        PieceId $id,
        ArticleNumber $articleNumber,
        string $name,
        PieceStatus $status
    ): AggregateChanged {
        return self::occur(
            $id->toString(),
            [
                'userId' => $userId->toString(),
                'articleNumber' => $articleNumber->toString(),
                'name' => $name,
                'status' => $status->toString(),
                'createdAt' => DateTimeHelper::create()->format(DateTimeImmutable::ATOM),
            ]
        );
    }

    public function userId(): UserId
    {
        return UserId::fromString($this->payload['userId']);
    }

    public function pieceId(): PieceId
    {
        return PieceId::fromString($this->aggregateId);
    }

    public function articleNumber(): ArticleNumber
    {
        return ArticleNumber::fromString($this->payload['articleNumber']);
    }

    public function name(): string
    {
        return $this->payload['name'];
    }

    public function status(): PieceStatus
    {
        return PieceStatus::fromString($this->payload['status']);
    }

    public function createdAt(): DateTimeImmutable
    {
        return DateTimeHelper::createFromString($this->payload['createdAt']);
    }
}
