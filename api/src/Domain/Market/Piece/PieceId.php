<?php

declare(strict_types=1);

namespace App\Domain\Market\Piece;

use App\Domain\UuidBehaviour;

class PieceId
{
    use UuidBehaviour;
}
