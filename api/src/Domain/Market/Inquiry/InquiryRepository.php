<?php

declare(strict_types=1);

namespace App\Domain\Market\Inquiry;

interface InquiryRepository
{
    public function get(InquiryId $inquiryId): Inquiry;
    public function save(Inquiry $inquiry): void;
}
