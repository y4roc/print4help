<?php

declare(strict_types=1);

namespace App\Domain\Market\Inquiry;

use Patchlevel\Enum\ExtendedEnum;

/**
 * @psalm-immutable
 */
final class InquiryStatus extends ExtendedEnum
{
    private const OPEN = 'open';
    private const IN_EXECUTION = 'in-execution';
    private const COMPLETED = 'completed';
    private const CLOSED = 'closed';
    private const PRODUCTION_STARTED = 'production_started';
    private const PRODUCTION_ABORTED = 'production_aborted';
    private const PRODUCTION_FINISHED = 'production_finished';
    private const SHIPPED = 'shipped';

    public static function open(): self
    {
        return self::get(self::OPEN);
    }

    public static function inExecution(): self
    {
        return self::get(self::IN_EXECUTION);
    }

    public static function completed(): self
    {
        return self::get(self::COMPLETED);
    }

    public static function closed(): self
    {
        return self::get(self::CLOSED);
    }

    public static function productionStarted(): self
    {
        return self::get(self::PRODUCTION_STARTED);
    }

    public static function productionAborted(): self
    {
        return self::get(self::PRODUCTION_ABORTED);
    }

    public static function productionFinished(): self
    {
        return self::get(self::PRODUCTION_FINISHED);
    }

    public static function shipped(): self
    {
        return self::get(self::SHIPPED);
    }
}
