<?php

declare(strict_types=1);

namespace App\Domain\Market\Inquiry;

use App\Domain\Account\UserId;
use App\Domain\Market\Inquiry\Event\Closed;
use App\Domain\Market\Inquiry\Event\InquiryCreated;
use App\Domain\Market\Inquiry\Event\Locked;
use App\Domain\Market\Inquiry\Event\MetadataUpdated;
use App\Domain\Market\Piece\PieceId;
use DateTimeImmutable;
use Patchlevel\EventSourcing\Aggregate\AggregateRoot;
use Webmozart\Assert\Assert;

class Inquiry extends AggregateRoot
{
    private InquiryId $id;
    private UserId $userId;
    private PieceId $pieceId;
    private InquiryStatus $status;
    private int $quantity;
    private ?string $purpose;
    /**
     * @var array<int, string>
     */
    private array $requirements;
    private ?DateTimeImmutable $createdAt;
    private ?DateTimeImmutable $updatedAt;
    private ?DateTimeImmutable $inExecutionAt;
    private ?DateTimeImmutable $completedAt;

    public function aggregateRootId(): string
    {
        return $this->id->toString();
    }

    public function status(): InquiryStatus
    {
        return $this->status;
    }

    /**
     * @param array<int, string> $requirements
     */
    public static function create(
        UserId $userId,
        InquiryId $inquiryId,
        PieceId $pieceId,
        int $quantity,
        ?string $purpose,
        array $requirements = []
    ): self {
        Assert::greaterThan($quantity, 0);
        Assert::lessThan($quantity, 100);
        Assert::allMaxLength($requirements, 1000);
        Assert::nullOrString($purpose);
        if (is_string($purpose)) {
            Assert::maxLength($purpose, 1000);
        }

        $self = new self();
        $self->apply(InquiryCreated::raise(
            $userId,
            $inquiryId,
            $pieceId,
            InquiryStatus::open(),
            $quantity,
            $purpose,
            $requirements
        ));

        return $self;
    }

    public function applyInquiryCreated(InquiryCreated $event): void
    {
        $this->id = $event->inquiryId();
        $this->status = $event->status();
        $this->userId = $event->userId();
        $this->pieceId = $event->pieceId();
        $this->quantity = $event->quantity();
        $this->purpose = $event->purpose();
        $this->requirements = $event->requirements();
        $this->createdAt = $event->createdAt();
    }

    /**
     * @param array<int, string> $requirements
     */
    public function updateMetadata(
        int $quantity,
        ?string $purpose,
        array $requirements = []
    ): void {
        Assert::greaterThan($quantity, 0);
        Assert::lessThan($quantity, 100);
        Assert::allMaxLength($requirements, 1000);
        Assert::nullOrString($purpose);
        if (is_string($purpose)) {
            Assert::maxLength($purpose, 1000);
        }

        $this->apply(MetadataUpdated::raise(
            $this->id,
            $quantity,
            $purpose,
            $requirements
        ));
    }

    public function applyMetadataUpdated(MetadataUpdated $event): void
    {
        $this->quantity = $event->quantity();
        $this->purpose = $event->purpose();
        $this->requirements = $event->requirements();
    }

    public function lock(): void
    {
        $this->apply(
            Locked::raise($this->id, InquiryStatus::inExecution())
        );
    }

    public function applyLocked(Locked $event): void
    {
        $this->status = $event->status();
        $this->inExecutionAt = $event->inExecutionAt();
    }

    public function close(): void
    {
        $this->apply(
            Closed::raise($this->id, InquiryStatus::closed())
        );
    }

    public function applyClosed(Closed $event): void
    {
        $this->status = $event->status();
        $this->updatedAt = $event->updatedAt();
    }
}
