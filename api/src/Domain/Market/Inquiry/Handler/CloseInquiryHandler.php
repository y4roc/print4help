<?php

declare(strict_types=1);

namespace App\Domain\Market\Inquiry\Handler;

use App\Domain\CommandHandlerInterface;
use App\Domain\Market\Inquiry\Command\CloseInquiry;
use App\Domain\Market\Inquiry\Exception\InquiryNotCloseable;
use App\Domain\Market\Inquiry\InquiryRepository;
use App\Domain\Market\Inquiry\InquiryStatus;

class CloseInquiryHandler implements CommandHandlerInterface
{
    public function __construct(private InquiryRepository $inquiryRepository)
    {
    }

    public function __invoke(CloseInquiry $command): void
    {
        $inquiry = $this->inquiryRepository->get($command->inquiryId());

        if ($inquiry->status() !== InquiryStatus::open()) {
            throw new InquiryNotCloseable($command->inquiryId(), $inquiry->status());
        }

        $inquiry->close();
        $this->inquiryRepository->save($inquiry);
    }
}
