<?php

declare(strict_types=1);

namespace App\Domain\Market\Inquiry\Handler;

use App\Domain\CommandHandlerInterface;
use App\Domain\Market\Inquiry\Command\UpdateInquiryMetadata;
use App\Domain\Market\Inquiry\InquiryRepository;

class UpdateInquiryMetadataHandler implements CommandHandlerInterface
{
    public function __construct(private InquiryRepository $inquiryRepository)
    {
    }

    public function __invoke(UpdateInquiryMetadata $command): void
    {
        $inquiry = $this->inquiryRepository->get($command->inquiryId());

        $inquiry->updateMetadata(
            $command->quantity(),
            $command->purpose(),
            $command->requirements()
        );

        $this->inquiryRepository->save($inquiry);
    }
}
