<?php declare(strict_types=1);

namespace App\Domain\Market\Inquiry\Event;

use App\Domain\DateTimeHelper;
use App\Domain\Market\Inquiry\InquiryId;
use DateTimeImmutable;
use Patchlevel\EventSourcing\Aggregate\AggregateChanged;

class MetadataUpdated extends AggregateChanged
{
    /**
     * @param array<int, string> $requirements
     */
    public static function raise(
        InquiryId $id,
        int $quantity,
        ?string $purpose = null,
        array $requirements = []
    ): AggregateChanged {
        return self::occur(
            $id->toString(),
            [
                'quantity' => $quantity,
                'purpose' => $purpose,
                'requirements' => $requirements,
                'updatedAt' => DateTimeHelper::create()->format(DateTimeImmutable::ATOM),
            ]
        );
    }

    public function inquiryId(): InquiryId
    {
        return InquiryId::fromString($this->aggregateId);
    }

    public function quantity(): int
    {
        return $this->payload['quantity'];
    }

    public function purpose(): ?string
    {
        return $this->payload['purpose'];
    }

    /**
     * @return array<int, string>
     */
    public function requirements(): array
    {
        return $this->payload['requirements'];
    }

    public function updatedAt(): DateTimeImmutable
    {
        return DateTimeHelper::createFromString($this->payload['updatedAt']);
    }
}
