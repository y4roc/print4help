<?php

declare(strict_types=1);

namespace App\Domain\Market\Inquiry;

use App\Domain\UuidBehaviour;

class InquiryId
{
    use UuidBehaviour;
}
