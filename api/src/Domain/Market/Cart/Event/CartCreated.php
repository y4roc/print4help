<?php

declare(strict_types=1);

namespace App\Domain\Market\Cart\Event;

use App\Domain\Account\UserId;
use App\Domain\DateTimeHelper;
use App\Domain\Market\Cart\CartId;
use DateTimeImmutable;
use Patchlevel\EventSourcing\Aggregate\AggregateChanged;

class CartCreated extends AggregateChanged
{
    public static function raise(
        UserId $userId,
        CartId $id
    ): AggregateChanged {
        return self::occur(
            $id->toString(),
            [
                'userId' => $userId->toString(),
                'createdAt' => DateTimeHelper::create()->format(DateTimeImmutable::ATOM),
            ]
        );
    }

    public function userId(): UserId
    {
        return UserId::fromString($this->payload['userId']);
    }

    public function cartId(): CartId
    {
        return CartId::fromString($this->aggregateId);
    }

    public function createdAt(): DateTimeImmutable
    {
        return DateTimeHelper::createFromString($this->payload['createdAt']);
    }
}
