<?php declare(strict_types=1);

namespace App\Domain\Market\Cart\Event;

use App\Domain\DateTimeHelper;
use App\Domain\Market\Cart\CartId;
use App\Domain\Market\Piece\PieceId;
use DateTimeImmutable;
use Patchlevel\EventSourcing\Aggregate\AggregateChanged;

class CartItemMetadataUpdated extends AggregateChanged
{
    /**
     * @param array<int, string> $requirements
     */
    public static function raise(
        CartId $id,
        PieceId $pieceId,
        string $purpose,
        array $requirements,
    ): AggregateChanged {
        return self::occur(
            $id->toString(),
            [
                'pieceId' => $pieceId->toString(),
                'purpose' => $purpose,
                'requirements' => $requirements,
                'updatedAt' => DateTimeHelper::create()->format(DateTimeImmutable::ATOM),
            ]
        );
    }

    public function cartId(): CartId
    {
        return CartId::fromString($this->aggregateId);
    }

    public function pieceId(): PieceId
    {
        return PieceId::fromString($this->payload['pieceId']);
    }

    public function purpose(): string
    {
        return $this->payload['purpose'];
    }

    /**
     * @return array<int, string>
     */
    public function requirements(): array
    {
        return $this->payload['requirements'];
    }

    public function updatedAt(): DateTimeImmutable
    {
        return DateTimeHelper::createFromString($this->payload['updatedAt']);
    }
}
