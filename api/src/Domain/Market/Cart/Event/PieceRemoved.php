<?php

declare(strict_types=1);

namespace App\Domain\Market\Cart\Event;

use App\Domain\DateTimeHelper;
use App\Domain\Market\Cart\CartId;
use App\Domain\Market\Piece\PieceId;
use DateTimeImmutable;
use Patchlevel\EventSourcing\Aggregate\AggregateChanged;

class PieceRemoved extends AggregateChanged
{
    public static function raise(
        CartId $id,
        PieceId $pieceId
    ): AggregateChanged {
        return self::occur(
            $id->toString(),
            [
                'pieceId' => $pieceId->toString(),
                'removedAt' => DateTimeHelper::create()->format(DateTimeImmutable::ATOM),
            ]
        );
    }

    public function pieceId(): PieceId
    {
        return PieceId::fromString($this->payload['pieceId']);
    }

    public function cartId(): CartId
    {
        return CartId::fromString($this->aggregateId);
    }

    public function removedAt(): DateTimeImmutable
    {
        return DateTimeHelper::createFromString($this->payload['removedAt']);
    }
}
