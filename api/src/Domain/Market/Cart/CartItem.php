<?php

declare(strict_types=1);

namespace App\Domain\Market\Cart;

use App\Domain\Market\Piece\PieceId;
use DateTimeImmutable;

class CartItem
{
    /**
     * @param array<int, string> $requirements
     */
    public function __construct(
        private PieceId $pieceId,
        private int $quantity,
        private DateTimeImmutable $addedAt,
        private ?string $purpose = null,
        private array $requirements = []
    ) {
    }

    public function pieceId(): PieceId
    {
        return $this->pieceId;
    }

    public function changeQuantity(int $quantity): void
    {
        $this->quantity = $quantity;
    }

    public function quantity(): int
    {
        return $this->quantity;
    }

    public function addedAt(): DateTimeImmutable
    {
        return $this->addedAt;
    }

    public function purpose(): ?string
    {
        return $this->purpose;
    }

    /**
     * @return array<int, string>
     */
    public function requirements(): array
    {
        return $this->requirements;
    }

    /**
     * @param array<int, string> $requirements
     */
    public function updateMetadata(string $purpose, array $requirements): void
    {
        $this->purpose = $purpose;
        $this->requirements = $requirements;
    }
}
