<?php

declare(strict_types=1);

namespace App\Domain\Market\Cart\Handler;

use App\Domain\Account\Repository\UserRepository;
use App\Domain\CommandHandlerInterface;
use App\Domain\Market\Cart\CartId;
use App\Domain\Market\Cart\CartRepository;
use App\Domain\Market\Cart\Command\UpdateCartItemMetadata;
use App\Domain\Market\Cart\Exception\CartNotFoundInUser;

class UpdateCartItemMetadataHandler implements CommandHandlerInterface
{
    public function __construct(
        private CartRepository $cartRepository,
        private UserRepository $userRepository
    ) {
    }

    public function __invoke(UpdateCartItemMetadata $command): void
    {
        $user = $this->userRepository->get($command->userId());
        $cartId = $user->getCartId();

        if (!$cartId instanceof CartId) {
            throw new CartNotFoundInUser($command->userId());
        }

        $cart = $this->cartRepository->get($cartId);

        $cart->updateCartItemMetadata(
            $command->pieceId(),
            $command->purpose(),
            $command->requirements()
        );
        $this->cartRepository->save($cart);
    }
}
