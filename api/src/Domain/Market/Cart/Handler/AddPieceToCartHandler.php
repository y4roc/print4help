<?php

declare(strict_types=1);

namespace App\Domain\Market\Cart\Handler;

use App\Domain\Account\Repository\UserRepository;
use App\Domain\CommandHandlerInterface;
use App\Domain\Market\Cart\Cart;
use App\Domain\Market\Cart\CartId;
use App\Domain\Market\Cart\CartRepository;
use App\Domain\Market\Cart\Command\AddPieceToCart;

class AddPieceToCartHandler implements CommandHandlerInterface
{
    public function __construct(
        private CartRepository $cartRepository,
        private UserRepository $userRepository
    ) {
    }

    public function __invoke(AddPieceToCart $command): void
    {
        $user = $this->userRepository->get($command->userId());
        $cartId = $user->getCartId();

        if ($cartId instanceof CartId) {
            $cart = $this->cartRepository->get($cartId);
        } else {
            $cart = Cart::create($command->userId(), CartId::create());
            $this->cartRepository->save($cart);
        }

        $cart->addPiece($command->pieceId(), $command->quantity());
        $this->cartRepository->save($cart);
    }
}
