<?php

declare(strict_types=1);

namespace App\Domain\Market\Cart\Handler;

use App\Domain\Account\Repository\UserRepository;
use App\Domain\CommandHandlerInterface;
use App\Domain\Market\Cart\CartId;
use App\Domain\Market\Cart\CartRepository;
use App\Domain\Market\Cart\Command\ChangePieceQuantity;
use App\Domain\Market\Cart\Exception\CartNotFoundInUser;

class ChangePieceQuantityHandler implements CommandHandlerInterface
{
    public function __construct(
        private CartRepository $cartRepository,
        private UserRepository $userRepository
    ) {
    }

    public function __invoke(ChangePieceQuantity $command): void
    {
        $user = $this->userRepository->get($command->userId());
        $cartId = $user->getCartId();

        if (!$cartId instanceof CartId) {
            throw new CartNotFoundInUser($command->userId());
        }

        $cart = $this->cartRepository->get($cartId);
        $cart->changePieceQuantity($command->pieceId(), $command->quantity());
        $this->cartRepository->save($cart);
    }
}
