<?php

declare(strict_types=1);

namespace App\Domain\Market\Cart\Command;

use App\Domain\Account\UserId;
use App\Domain\Market\Piece\PieceId;

class UpdateCartItemMetadata
{
    private string $userId;
    private string $pieceId;
    private string $purpose;
    /**
     * @var array<array-key, mixed>
     */
    private array $requirements;

    /**
     * @param array<array-key, mixed> $requirements
     */
    public function __construct(UserId $userId, PieceId $pieceId, string $purpose, array $requirements)
    {
        $this->userId = $userId->toString();
        $this->pieceId = $pieceId->toString();
        $this->purpose = $purpose;
        $this->requirements = $requirements;
    }

    public function userId(): UserId
    {
        return UserId::fromString($this->userId);
    }

    public function pieceId(): PieceId
    {
        return PieceId::fromString($this->pieceId);
    }

    public function purpose(): string
    {
        return $this->purpose;
    }

    /**
     * @return array<array-key, mixed>
     */
    public function requirements(): array
    {
        return $this->requirements;
    }
}
