<?php

declare(strict_types=1);

namespace App\Domain\Market\Cart\Command;

use App\Domain\Account\UserId;
use App\Domain\Market\Piece\PieceId;

class RemovePieceFromCart
{
    private string $userId;
    private string $pieceId;

    public function __construct(UserId $userId, PieceId $pieceId)
    {
        $this->userId = $userId->toString();
        $this->pieceId = $pieceId->toString();
    }

    public function userId(): UserId
    {
        return UserId::fromString($this->userId);
    }

    public function pieceId(): PieceId
    {
        return PieceId::fromString($this->pieceId);
    }
}
