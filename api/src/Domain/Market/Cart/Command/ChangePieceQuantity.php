<?php

declare(strict_types=1);

namespace App\Domain\Market\Cart\Command;

use App\Domain\Account\UserId;
use App\Domain\Market\Piece\PieceId;

class ChangePieceQuantity
{
    private string $userId;
    private string $pieceId;
    private int $quantity;

    public function __construct(UserId $userId, PieceId $pieceId, int $quantity)
    {
        $this->userId = $userId->toString();
        $this->pieceId = $pieceId->toString();
        $this->quantity = $quantity;
    }

    public function userId(): UserId
    {
        return UserId::fromString($this->userId);
    }

    public function pieceId(): PieceId
    {
        return PieceId::fromString($this->pieceId);
    }

    public function quantity(): int
    {
        return $this->quantity;
    }
}
