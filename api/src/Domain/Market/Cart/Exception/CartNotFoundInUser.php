<?php

declare(strict_types=1);

namespace App\Domain\Market\Cart\Exception;

use App\Domain\Account\UserId;
use App\Domain\DomainException;
use Throwable;

final class CartNotFoundInUser extends DomainException
{
    public function __construct(UserId $userId, Throwable $previous = null)
    {
        parent::__construct(sprintf('User [%s] does not have a Cart', $userId->toString()), 0, $previous);
    }
}
