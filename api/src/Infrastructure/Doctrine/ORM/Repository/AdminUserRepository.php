<?php

declare(strict_types=1);

namespace App\Infrastructure\Doctrine\ORM\Repository;

use App\Domain\Admin\AdminUserId;
use App\Domain\Admin\Exception\AdminUserByAdminUserIdDoesNotExist;
use App\Domain\Admin\Exception\AdminUserByEmailDoesNotExist;
use App\Domain\Admin\Model\AdminUser;
use App\Domain\Admin\Repository\AdminUserRepository as AdminUserRepositoryInterface;
use App\Domain\Email;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use RuntimeException;

final class AdminUserRepository implements AdminUserRepositoryInterface
{
    public function __construct(private ManagerRegistry $registry)
    {
    }

    public function get(AdminUserId $adminUserId): AdminUser
    {
        $queryBuilder = $this->getRepository()->createQueryBuilder('a');
        $queryBuilder->where('a.id = :id');
        $queryBuilder->setParameter('id', $adminUserId->toString());

        $admin = $queryBuilder->getQuery()->getOneOrNullResult();

        if (!$admin instanceof AdminUser) {
            throw new AdminUserByAdminUserIdDoesNotExist($adminUserId);
        }

        return $admin;
    }

    public function getByEmail(Email $email): AdminUser
    {
        $queryBuilder = $this->getRepository()->createQueryBuilder('a');
        $queryBuilder->where('a.email = :email');
        $queryBuilder->setParameter('email', $email->toString());

        $admin = $queryBuilder->getQuery()->getOneOrNullResult();

        if (!$admin instanceof AdminUser) {
            throw new AdminUserByEmailDoesNotExist($email);
        }

        return $admin;
    }

    public function save(AdminUser $adminUser): void
    {
        $entityManager = $this->getManager();

        $entityManager->persist($adminUser);
        $entityManager->flush();
    }

    public function remove(AdminUser $adminUser): void
    {
        $entityManager = $this->getManager();

        $entityManager->remove($adminUser);
        $entityManager->flush();
    }

    private function getManager(): EntityManager
    {
        $manager = $this->registry->getManagerForClass(AdminUser::class);

        if (!$manager instanceof EntityManager) {
            throw new RuntimeException();
        }

        return $manager;
    }

    /**
     * @return EntityRepository<AdminUser>
     */
    private function getRepository(): EntityRepository
    {
        $repository = $this->registry->getRepository(AdminUser::class);

        if (!$repository instanceof EntityRepository) {
            throw new RuntimeException();
        }

        return $repository;
    }
}
