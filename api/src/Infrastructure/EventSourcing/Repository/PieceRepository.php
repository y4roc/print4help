<?php

declare(strict_types=1);

namespace App\Infrastructure\EventSourcing\Repository;

use App\Domain\Market\Piece\Piece;
use App\Domain\Market\Piece\PieceId;
use App\Domain\Market\Piece\PieceRepository as BasePieceRepository;
use Patchlevel\EventSourcing\Repository\Repository;
use RuntimeException;

final class PieceRepository implements BasePieceRepository
{
    public function __construct(private Repository $repository)
    {
    }

    public function get(PieceId $pieceId): Piece
    {
        $aggregate = $this->repository->load($pieceId->toString());

        if (!$aggregate instanceof Piece) {
            throw new RuntimeException();
        }

        return $aggregate;
    }

    public function save(Piece $piece): void
    {
        $this->repository->save($piece);
    }
}
