<?php

declare(strict_types=1);

namespace App\Infrastructure\EventSourcing\Repository;

use App\Domain\Market\Cart\Cart;
use App\Domain\Market\Cart\CartId;
use App\Domain\Market\Cart\CartRepository as BaseCartRepository;
use App\Domain\Market\Cart\Exception\CartNotFound;
use Patchlevel\EventSourcing\Repository\Repository;

final class CartRepository implements BaseCartRepository
{
    public function __construct(private Repository $repository)
    {
    }

    public function get(CartId $cartId): Cart
    {
        $aggregate = $this->repository->load($cartId->toString());

        if (!$aggregate instanceof Cart) {
            throw new CartNotFound($cartId);
        }

        return $aggregate;
    }

    public function save(Cart $cart): void
    {
        $this->repository->save($cart);
    }
}
