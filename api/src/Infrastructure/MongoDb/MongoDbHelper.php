<?php declare(strict_types=1);

namespace App\Infrastructure\MongoDb;

use MongoDB\Driver\Cursor;
use Webmozart\Assert\Assert;

final class MongoDbHelper
{
    /**
     * @return array<int, non-empty-array<array-key, mixed>>
     */
    public static function cursorToArray(Cursor $cursor): array
    {
        $result = [];

        foreach ($cursor->toArray() as $row) {
            Assert::isArray($row);
            /** @var non-empty-array<array-key, mixed> $row */
            $result[] = self::normalize($row);
        }

        return $result;
    }

    /**
     * @param non-empty-array<array-key, mixed> $data
     * @return non-empty-array<array-key, mixed>
     */
    public static function normalize(array $data): array
    {
        if (array_key_exists('_id', $data)) {
            $data['id'] = (string)$data['_id'];
            unset($data['_id']);
        }

        return $data;
    }
}
