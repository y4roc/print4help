<?php

declare(strict_types=1);

namespace App\Infrastructure\MongoDb\Repository;

use App\Domain\InputHelper;
use App\Domain\Market\Piece\CategoryId;
use App\Domain\Market\Piece\CategoryRepository;
use App\Domain\Market\Piece\PieceStatus;
use App\Infrastructure\MongoDb\Connection;
use App\Infrastructure\MongoDb\MongoDbHelper;
use App\Infrastructure\ReadModel\Exception\PieceByArticleNumberNotFoundException;
use App\Infrastructure\ReadModel\Exception\PieceByPieceIdNotFoundException;
use App\Infrastructure\ReadModel\Piece;
use App\Infrastructure\ReadModel\Projection\PieceProjection;
use App\Infrastructure\ReadModel\Repository\PieceRepository as PieceRepositoryInterface;
use Webmozart\Assert\Assert;

class PieceRepository implements PieceRepositoryInterface
{
    public function __construct(
        private Connection $db,
        private CategoryRepository $categoryRepository
    ) {
    }

    public function get(string $pieceId): Piece
    {
        $filter = ['_id' => $pieceId];
        $options = [];
        $data = $this->db->collection(PieceProjection::DOCUMENT_NAME)->findOne($filter, $options);

        if (empty($data)) {
            throw new PieceByPieceIdNotFoundException($pieceId);
        }

        Assert::isArray($data);
        return $this->hydrate(MongoDbHelper::normalize($data));
    }

    public function getByArticleNumber(string $articleNumber): Piece
    {
        $filter = ['articleNumber' => $articleNumber];
        $options = [];
        $data = $this->db->collection(PieceProjection::DOCUMENT_NAME)->findOne($filter, $options);

        if (empty($data)) {
            throw new PieceByArticleNumberNotFoundException($articleNumber);
        }

        Assert::isArray($data);
        return $this->hydrate(MongoDbHelper::normalize($data));
    }

    public function getAll(): array
    {
        $filter = [];
        $options = [];
        $data = $this->db->collection(PieceProjection::DOCUMENT_NAME)->find($filter, $options);

        return array_map(
            fn (array $data): Piece => $this->hydrate($data),
            MongoDbHelper::cursorToArray($data)
        );
    }

    /**
     * @param array<int, string> $categoryIds
     * @return Piece[]
     */
    public function getAllApproved(array $categoryIds): array
    {
        $filter = [
            'status' => PieceStatus::approved()->toString(),
        ];

        if (count($categoryIds) > 0) {
            $filter['categoryIds'] = ['$all' => $categoryIds];
        }

        $options = [];
        $data = $this->db->collection(PieceProjection::DOCUMENT_NAME)->find($filter, $options);

        return array_map(
            fn (array $data): Piece => $this->hydrate($data),
            MongoDbHelper::cursorToArray($data)
        );
    }

    /**
     * @param non-empty-array<array-key, mixed> $data
     */
    private function hydrate(array $data): Piece
    {
        $piece = new Piece();
        $piece->id = InputHelper::string($data['id']);
        $piece->userId = InputHelper::string($data['userId']);
        $piece->articleNumber = InputHelper::string($data['articleNumber']);
        $piece->status = InputHelper::string($data['status']);
        $piece->name = InputHelper::string($data['name']);
        $piece->summary = InputHelper::nullableString($data['summary']);
        $piece->description = InputHelper::nullableString($data['description']);
        $piece->manufacturingNotes = InputHelper::nullableString($data['manufacturingNotes']);
        $piece->licence = InputHelper::nullableString($data['licence']);
        $piece->sourceUrl = InputHelper::nullableString($data['sourceUrl']);
        $piece->createdAt = InputHelper::string($data['createdAt']);
        $piece->updatedAt = InputHelper::nullableString($data['updatedAt']);
        $piece->categories = array_map(
            fn (string $categoryId) => $this->categoryRepository->getByCategoryId(CategoryId::fromString($categoryId)),
            InputHelper::array($data['categoryIds'])
        );

        return $piece;
    }
}
