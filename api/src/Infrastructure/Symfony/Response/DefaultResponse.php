<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Response;

use OpenApi\Annotations as OA;

class DefaultResponse
{
    /**
     * @OA\Property(type="integer")
     */
    public int $code;

    /**
     * @OA\Property(type="string")
     */
    public string $message;

    /**
     * @var array<mixed, mixed>
     * @OA\Property(
     *     type="array",
     *     @OA\Items(type="string")
     * )
     */
    public array $data;

    /**
     * @param array<mixed, mixed> $data
     */
    public function __construct(int $code = 200, string $message = 'OK.', array $data = [])
    {
        $this->code = $code;
        $this->message = $message;
        $this->data = $data;
    }
}
