<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Translation\Extractor;

use App\Domain\InputHelper;
use PhpParser\Node;
use PhpParser\NodeVisitor;
use Translation\Extractor\Visitor\Php\BasePHPVisitor;

class TranslatableMessageExtractor extends BasePHPVisitor implements NodeVisitor
{
    public function beforeTraverse(array $nodes): ?Node
    {
        return null;
    }

    public function enterNode(Node $node): ?Node
    {
        if (!$node instanceof Node\Expr\New_) {
            return null;
        }

        $parts = $node->class->parts;
        if (!array_key_exists(0, $parts)) {
            return null;
        }

        if ($parts[0] !== 'TranslatableMessage') {
            return null;
        }

        if (count($node->args) === 0) {
            return null;
        }

        $arg0 = $node->args[0];

        if ($arg0->value instanceof Node\Scalar\String_) {
            $this->addLocation(
                $arg0->value->value,
                InputHelper::int($node->getAttribute('startLine')),
                $node
            );
        }

        return null;
    }

    public function leaveNode(Node $node): ?Node
    {
        return null;
    }

    public function afterTraverse(array $nodes): ?Node
    {
        return null;
    }
}
