<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Translation\Extractor;

use App\Domain\Market\Piece\CategoryId;
use App\Infrastructure\Symfony\Translation\CategoryTranslator;
use PhpParser\Node;
use PhpParser\NodeVisitor;
use Translation\Extractor\Visitor\Php\BasePHPVisitor;

class CategoryIdExtractor extends BasePHPVisitor implements NodeVisitor
{
    private bool $addedCategories = false;

    public function beforeTraverse(array $nodes)
    {
        if ($this->addedCategories) {
            return null;
        }

        foreach (CategoryId::values() as $categoryId) {
            $this->addLocation(
                CategoryTranslator::buildName($categoryId),
                0,
                null,
                ['domain' => 'category']
            );
            $this->addLocation(
                CategoryTranslator::buildDescription($categoryId),
                0,
                null,
                ['domain' => 'category']
            );
        }

        $this->addedCategories = true;

        return null;
    }

    public function enterNode(Node $node)
    {
        return null;
    }

    public function leaveNode(Node $node)
    {
        return null;
    }

    public function afterTraverse(array $nodes)
    {
        return null;
    }

    public function supportsExtension(string $extension): bool
    {
        return $extension === 'php';
    }

    public function addVisitor(NodeVisitor $visitor): void
    {
    }
}
