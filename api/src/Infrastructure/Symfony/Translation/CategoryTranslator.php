<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Translation;

use App\Domain\Market\Piece\CategoryId;

class CategoryTranslator
{
    public static function buildName(CategoryId $categoryId): string
    {
        return sprintf('category.%s._name', $categoryId->toString());
    }

    public static function buildDescription(CategoryId $categoryId): string
    {
        return sprintf('category.%s._description', $categoryId->toString());
    }
}
