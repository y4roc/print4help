<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Security;

use App\Infrastructure\Symfony\Response\DefaultResponse;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Exception\JWTDecodeFailureException;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Http\Authenticator\AbstractAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\PassportInterface;
use Symfony\Component\Security\Http\Authenticator\Passport\SelfValidatingPassport;
use Webmozart\Assert\Assert;
use function sprintf;
use function str_replace;

final class TokenAuthenticator extends AbstractAuthenticator
{
    public function __construct(
        private JWTEncoderInterface $jwtEncoder,
        private LoggerInterface $logger
    ) {
    }

    public function authenticate(Request $request): PassportInterface
    {
        $token = $this->getTokenFromRequest($request);
        $user = $this->getUser($token);

        $userBadge = new UserBadge($user->getUserIdentifier(), function () use ($user): UserInterface {
            return $user;
        });

        return new SelfValidatingPassport($userBadge);
    }

    public function supports(Request $request): bool
    {
        $this->logger->debug('TokenAuthenticator supports hitted!');

        if ($request->getPathInfo() === '/login') {
            $this->logger->debug('TokenAuthenticator supports hitted but login in path');

            return false;
        }

        $this->logger->debug('TokenAuthenticator supports header', $request->headers->all());

        return $request->headers->has('X-AUTH-TOKEN');
    }

    private function getTokenFromRequest(Request $request): string
    {
        /** @var string $rawToken */
        $rawToken = $request->headers->get('X-AUTH-TOKEN');

        $this->logger->debug(sprintf('Extracted token [%s] from header', $rawToken));

        return str_replace('Bearer ', '', $rawToken);
    }

    private function getUser(string $token): UserInterface
    {
        if ($token === '') {
            $this->logger->error(sprintf('Token was empty.'));
            throw new AuthenticationException();
        }

        try {
            $data = $this->jwtEncoder->decode($token);
        } catch (JWTDecodeFailureException $exception) {
            throw new AuthenticationException();
        }

        Assert::keyExists($data, 'id');
        Assert::keyExists($data, 'username');
        Assert::keyExists($data, 'roles');
        Assert::string($data['id']);
        Assert::string($data['username']);
        Assert::isArray($data['roles']);
        Assert::allString($data['roles']);

        return User::fromJWT($data);
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): ?Response
    {
        $this->logger->error('Authentication failed');

        return new JsonResponse(
            new DefaultResponse(
                Response::HTTP_FORBIDDEN,
                'Authentication failed.',
                []
            ),
            Response::HTTP_FORBIDDEN
        );
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
    {
        return null;
    }
}
