<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Security;

use Webmozart\Assert\Assert;

final class LoginRequest
{
    public function __construct(
        private string $username,
        private string $password
    ) {
    }

    /**
     * @param array<mixed, mixed> $data
     */
    public static function fromArray(array $data): self
    {
        Assert::keyExists($data, 'username');
        Assert::keyExists($data, 'password');

        $username = $data['username'];
        $password = $data['password'];

        Assert::string($username);
        Assert::string($password);

        return new self($username, $password);
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function getPassword(): string
    {
        return $this->password;
    }
}
