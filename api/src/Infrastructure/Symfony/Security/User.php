<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Security;

use App\Domain\Account\Model\User as Account;
use App\Domain\Account\UserId;
use App\Domain\Admin\Model\AdminUser;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

final class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    private UserId $userId;
    /** @var array<string> */
    private array $roles;
    private string $username;
    private ?string $password;

    /**
     * @param array<string> $roles
     */
    public function __construct(UuidInterface $userId, string $username, array $roles, ?string $password = null)
    {
        $this->userId = UserId::fromString($userId->toString());
        $this->roles = $roles;
        $this->username = $username;
        $this->password = $password;
    }

    public function getId(): UserId
    {
        return $this->userId;
    }

    public function getRoles(): array
    {
        return $this->roles;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function getUserIdentifier(): string
    {
        return $this->getId()->toString();
    }

    public function isSuperAdmin(): bool
    {
        return in_array('ROLE_SUPER_ADMIN', $this->roles, true);
    }

    public function isAdmin(): bool
    {
        return in_array('ROLE_ADMIN', $this->roles, true);
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function getSalt(): ?string
    {
        return '';
    }

    public function eraseCredentials(): void
    {
        // TODO: Implement eraseCredentials() method.
    }

    public static function forPasswordHashing(): self
    {
        return new self(Uuid::uuid4(), 'temp', []);
    }

    /**
     * @param array{id: string, username: string, roles: array<string>} $data
     */
    public static function fromJWT(array $data): self
    {
        return new self(
            Uuid::fromString($data['id']),
            $data['username'],
            $data['roles']
        );
    }

    public static function fromAccount(Account $account): self
    {
        return new self(
            Uuid::fromString($account->getId()->toString()),
            $account->getEmail()->toString(),
            ['ROLE_USER'],
            $account->getPassword()
        );
    }

    public static function fromAdmin(AdminUser $admin): self
    {
        return new self(
            Uuid::fromString($admin->getId()->toString()),
            $admin->getEmail()->toString(),
            ['ROLE_ADMIN'],
            $admin->getPassword()
        );
    }
}
