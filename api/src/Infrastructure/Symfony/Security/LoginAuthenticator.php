<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Security;

use App\Domain\Account\Exception\UserByEmailDoesNotExist;
use App\Domain\Account\Repository\UserRepository;
use App\Domain\Admin\Exception\AdminUserByEmailDoesNotExist;
use App\Domain\Admin\Repository\AdminUserRepository;
use App\Domain\Email;
use App\Infrastructure\Symfony\Response\Security\LoginResponse;
use InvalidArgumentException;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Http\Authenticator\AbstractAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Credentials\PasswordCredentials;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Http\Authenticator\Passport\PassportInterface;
use function array_key_exists;
use function sprintf;

final class LoginAuthenticator extends AbstractAuthenticator
{
    public function __construct(
        private UserRepository $userRepository,
        private AdminUserRepository $adminRepository,
        private UserPasswordHasherInterface $hasher,
        private JWTEncoderInterface $jwtEncoder,
        private LoggerInterface $logger
    ) {
    }

    public function supports(Request $request): bool
    {
        if ($request->getPathInfo() !== '/login') {
            return false;
        }

        $content = $this->getBody($request);

        return array_key_exists('username', $content) && array_key_exists('password', $content);
    }

    public function authenticate(Request $request): PassportInterface
    {
        $loginRequest = LoginRequest::fromArray($this->getBody($request));
        $user = $this->getUser($loginRequest);

        if (!$this->checkCredentials($loginRequest, $user)) {
            throw new AuthenticationException();
        }

        $userBadge = new UserBadge(
            $user->getUserIdentifier(),
            function () use ($user) {
                return $user;
            }
        );

        return new Passport(
            $userBadge,
            new PasswordCredentials($loginRequest->getPassword()),
            [] // csrf token?
        );
    }

    private function getUser(LoginRequest $credentials): User
    {
        if ($credentials->getUsername() === '') {
            $this->logger->error('Username was empty.');
            throw new AuthenticationException();
        }

        try {
            $email = new Email($credentials->getUsername());
        } catch (InvalidArgumentException $exception) {
            $this->logger->error(sprintf('Username [%s] was not a valid email.', $credentials->getUsername()));
            throw new AuthenticationException();
        }

        try {
            return User::fromAdmin($this->adminRepository->getByEmail($email));
        } catch (AdminUserByEmailDoesNotExist $exception) {
            try {
                $account = $this->userRepository->getByEmail($email);
            } catch (UserByEmailDoesNotExist $exception) {
                throw new AuthenticationException();
            }
        }

        if ($account->isActive() === false) {
            throw new AuthenticationException();
        }

        return User::fromAccount($account);
    }

    private function checkCredentials(LoginRequest $credentials, PasswordAuthenticatedUserInterface $user): bool
    {
        if ($credentials->getPassword() === '') {
            $this->logger->error('Password was empty.');
            throw new AuthenticationException();
        }

        $hashedPassword = $user->getPassword();
        if ($hashedPassword === null) {
            $this->logger->error('User does not have a password');
            throw new AuthenticationException();
        }

        if ($this->hasher->isPasswordValid($user, $credentials->getPassword())) {
            return true;
        }

        $this->logger->error('Wrong password given.');

        return false;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): ?Response
    {
        $this->logger->error('Authentication failed');

        return new JsonResponse(
            [
                'code' => Response::HTTP_UNAUTHORIZED,
                'message' => 'Authentication failed.',
                'data' => null,
            ],
            Response::HTTP_UNAUTHORIZED
        );
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
    {
        $this->logger->debug('Authentication successful');

        /** @var User $user */
        $user = $token->getUser();

        $jwt = $this->jwtEncoder->encode(
            [
                'id' => $user->getId()->toString(),
                'roles' => $user->getRoles(),
                'username' => $user->getUsername(),
            ]
        );

        return new JsonResponse(
            new LoginResponse(
                Response::HTTP_OK,
                'Authentication successful.',
                $jwt
            ),
            Response::HTTP_OK
        );
    }

    /**
     * @return array<mixed, mixed>
     */
    private function getBody(Request $request): array
    {
        /** @var array<mixed, mixed> $body */
        $body = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);

        return $body;
    }
}
