<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Validator\Piece;

use App\Domain\Market\Piece\PieceStatus;
use App\Infrastructure\ReadModel\Exception\PieceByPieceIdNotFoundException;
use App\Infrastructure\ReadModel\Repository\PieceRepository;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use function is_string;

final class PieceIsApprovedValidator extends ConstraintValidator
{
    public function __construct(private PieceRepository $pieceRepository)
    {
    }

    public function validate($value, Constraint $constraint): void
    {
        if (!$constraint instanceof PieceIsApproved) {
            throw new UnexpectedTypeException($constraint, PieceIsApproved::class);
        }

        if ($value === null || is_string($value) === false || $value === '') {
            return;
        }

        try {
            $piece = $this->pieceRepository->get($value);
        } catch (PieceByPieceIdNotFoundException $exception) {
            return;
        }

        if ($piece->status === PieceStatus::approved()->toString()) {
            return;
        }

        $this->context->buildViolation($constraint->message)
            ->setInvalidValue($value)
            ->setTranslationDomain($constraint->translationDomain)
            ->addViolation();
    }
}
