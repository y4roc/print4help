<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Validator\Piece;

use Attribute;
use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
#[Attribute]
final class PieceBelongsToCurrentUser extends Constraint
{
    public string $message = 'piece.access_denied';
    public string $translationDomain = 'piece.validator';

    /**
     * @param mixed $options
     * @param string[] $groups
     * @param mixed $payload
     */
    public function __construct(
        string $message,
        $options = null,
        array $groups = null,
        $payload = null
    ) {
        parent::__construct($options, $groups, $payload);
        $this->message = $message;
    }

    public function getTargets(): string
    {
        return self::PROPERTY_CONSTRAINT;
    }
}
