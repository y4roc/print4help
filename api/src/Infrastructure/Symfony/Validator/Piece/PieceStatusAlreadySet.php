<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Validator\Piece;

use Attribute;
use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
#[Attribute]
final class PieceStatusAlreadySet extends Constraint
{
    public string $message = 'piece.status.already_set';
    public string $targetStatus;
    public string $translationDomain = 'piece.validator';

    /**
     * @param mixed $options
     * @param string[] $groups
     * @param mixed $payload
     */
    public function __construct(
        string $message,
        string $targetStatus,
        $options = null,
        array $groups = null,
        $payload = null
    ) {
        $this->message = $message;
        $this->targetStatus = $targetStatus;
        parent::__construct($options, $groups, $payload);
    }

    public function getTargets(): string
    {
        return self::PROPERTY_CONSTRAINT;
    }
}
