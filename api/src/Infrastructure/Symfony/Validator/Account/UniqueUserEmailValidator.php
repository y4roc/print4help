<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Validator\Account;

use App\Domain\Account\Exception\UserByEmailDoesNotExist;
use App\Domain\Account\Repository\UserRepository;
use App\Domain\Email;
use InvalidArgumentException;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use function is_string;

final class UniqueUserEmailValidator extends ConstraintValidator
{
    public function __construct(private UserRepository $userRepository)
    {
    }

    public function validate($value, Constraint $constraint): void
    {
        if (!$constraint instanceof UniqueUserEmail) {
            throw new UnexpectedTypeException($constraint, UniqueUserEmail::class);
        }

        if ($value === null) {
            return;
        }

        if (is_string($value) === false) {
            return;
        }

        try {
            $email = Email::fromString($value);
        } catch (InvalidArgumentException $exception) {
            return;
        }

        try {
            $this->userRepository->getByEmail($email);
        } catch (UserByEmailDoesNotExist $exception) {
            return;
        }

        $this->context->buildViolation($constraint->message)
            ->setInvalidValue($value)
            ->setTranslationDomain($constraint->translationDomain)
            ->addViolation();
    }
}
