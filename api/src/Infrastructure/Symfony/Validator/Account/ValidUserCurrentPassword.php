<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Validator\Account;

use Attribute;
use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 * @Target({"CLASS", "ANNOTATION"})
 */
#[Attribute]
final class ValidUserCurrentPassword extends Constraint
{
    public string $message = 'valid_user_current_password.invalid';
    public string $translationDomain = 'account.validator';

    /**
     * @param mixed $options
     * @param string[] $groups
     * @param mixed $payload
     */
    public function __construct(
        string $message,
        $options = null,
        array $groups = null,
        $payload = null
    ) {
        parent::__construct($options, $groups, $payload);
        $this->message = $message;
    }

    public function getTargets(): string
    {
        return self::CLASS_CONSTRAINT;
    }
}
