<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Validator\Account;

use App\Domain\Account\Exception\UserPasswordRecoveryByPasswordRecoveryTokenDoesNotExist;
use App\Domain\Account\Repository\UserRepository;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use function is_string;

final class UserPasswordRecoveryExistsForPasswordRecoveryTokenValidator extends ConstraintValidator
{
    public function __construct(private UserRepository $userRepository)
    {
    }

    public function validate($value, Constraint $constraint): void
    {
        if (!$constraint instanceof UserPasswordRecoveryExistsForPasswordRecoveryToken) {
            throw new UnexpectedTypeException($constraint, UserPasswordRecoveryExistsForPasswordRecoveryToken::class);
        }

        if ($value === null) {
            return;
        }

        if (is_string($value) === false) {
            return;
        }

        try {
            $this->userRepository->getPasswordRecoveryByToken($value);
        } catch (UserPasswordRecoveryByPasswordRecoveryTokenDoesNotExist $exception) {
            $this->context->buildViolation($constraint->message)
                ->setInvalidValue($value)
                ->setTranslationDomain($constraint->translationDomain)
                ->addViolation();
        }
    }
}
