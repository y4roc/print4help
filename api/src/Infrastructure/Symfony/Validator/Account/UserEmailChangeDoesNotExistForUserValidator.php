<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Validator\Account;

use App\Domain\Account\Exception\UserEmailChangeByUserIdDoesNotExist;
use App\Domain\Account\Repository\UserRepository;
use App\Domain\Account\UserId;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use function is_string;

final class UserEmailChangeDoesNotExistForUserValidator extends ConstraintValidator
{
    public function __construct(private UserRepository $userRepository)
    {
    }

    public function validate($value, Constraint $constraint): void
    {
        if (!$constraint instanceof UserEmailChangeDoesNotExistForUser) {
            throw new UnexpectedTypeException($constraint, UserEmailChangeDoesNotExistForUser::class);
        }

        if ($value === null) {
            return;
        }

        if (is_string($value) === false) {
            return;
        }

        try {
            $this->userRepository->getEmailChangeByUserId(UserId::fromString($value));
        } catch (UserEmailChangeByUserIdDoesNotExist $exception) {
            return;
        }

        $this->context->buildViolation($constraint->message)
            ->setTranslationDomain($constraint->translationDomain)
            ->setInvalidValue($value)
            ->addViolation();
    }
}
