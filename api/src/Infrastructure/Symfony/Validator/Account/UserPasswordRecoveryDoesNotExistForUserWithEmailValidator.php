<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Validator\Account;

use App\Domain\Account\Exception\UserByEmailDoesNotExist;
use App\Domain\Account\Repository\UserRepository;
use App\Domain\Email;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use function is_string;

final class UserPasswordRecoveryDoesNotExistForUserWithEmailValidator extends ConstraintValidator
{
    public function __construct(private UserRepository $userRepository)
    {
    }

    public function validate($value, Constraint $constraint): void
    {
        if (!$constraint instanceof UserPasswordRecoveryDoesNotExistForUserWithEmail) {
            throw new UnexpectedTypeException($constraint, UserPasswordRecoveryDoesNotExistForUserWithEmail::class);
        }

        if ($value === null) {
            return;
        }

        if (is_string($value) === false) {
            return;
        }

        try {
            $user = $this->userRepository->getByEmail(Email::fromString($value));
        } catch (UserByEmailDoesNotExist $exception) {
            return;
        }

        if ($user->getPasswordRecovery() === null) {
            return;
        }

        $this->context->buildViolation($constraint->message)
            ->setInvalidValue($value)
            ->setTranslationDomain($constraint->translationDomain)
            ->addViolation();
    }
}
