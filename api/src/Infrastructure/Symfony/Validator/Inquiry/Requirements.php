<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Validator\Inquiry;

use Attribute;
use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
#[Attribute]
final class Requirements extends Constraint
{
    public string $message = 'inquiry.requirements.invalid';
    public string $isNotIntMessage = 'inquiry.requirements.key_is_not_int';
    public string $valueIsNotStringMessage = 'inquiry.requirements.value_is_not_string';
    public string $valueMaxLengthMessage = 'inquiry.requirements.value_is_too_long';
    public int $maxLength = 1000;
    public string $translationDomain = 'inquiry.validator';

    /**
     * @param mixed $options
     * @param string[] $groups
     * @param mixed $payload
     */
    public function __construct(
        string $message,
        string $messageKeyIsNotInt,
        string $messageValueIsNotString,
        string $messageValueMaxLength,
        int $maxLength = 1000,
        $options = null,
        array $groups = null,
        $payload = null
    ) {
        parent::__construct($options, $groups, $payload);
        $this->message = $message;
        $this->isNotIntMessage = $messageKeyIsNotInt;
        $this->valueIsNotStringMessage = $messageValueIsNotString;
        $this->valueMaxLengthMessage = $messageValueMaxLength;
        $this->maxLength = $maxLength;
    }

    public function getTargets(): string
    {
        return self::PROPERTY_CONSTRAINT;
    }
}
