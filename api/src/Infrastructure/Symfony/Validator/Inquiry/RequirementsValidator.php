<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Validator\Inquiry;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use function is_string;

final class RequirementsValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint): void
    {
        if (!$constraint instanceof Requirements) {
            throw new UnexpectedTypeException($constraint, Requirements::class);
        }

        if (!is_array($value)) {
            $this->context->buildViolation($constraint->message)
                ->setInvalidValue($value)
                ->setTranslationDomain($constraint->translationDomain)
                ->addViolation();
            return;
        }

        foreach ($value as $key => $item) {
            if (!is_int($key)) {
                $this->context->buildViolation($constraint->isNotIntMessage)
                    ->setInvalidValue(['invalidType' => $key])
                    ->setTranslationDomain($constraint->translationDomain)
                    ->addViolation();
            }
            if (!is_string($item)) {
                $this->context->buildViolation($constraint->valueIsNotStringMessage)
                    ->setInvalidValue(['invalidType' => $item])
                    ->setTranslationDomain($constraint->translationDomain)
                    ->addViolation();
                continue;
            }
            if (strlen($item) > $constraint->maxLength) {
                $this->context->buildViolation($constraint->valueMaxLengthMessage)
                    ->setInvalidValue($item)
                    ->setTranslationDomain($constraint->translationDomain)
                    ->addViolation();
            }
        }
    }
}
