<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Validator\Inquiry;

use App\Domain\Market\Inquiry\InquiryStatus;
use App\Infrastructure\ReadModel\Exception\InquiryByInquiryIdNotFoundException;
use App\Infrastructure\ReadModel\Repository\InquiryRepository;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Exception\UnexpectedValueException;
use function is_string;

final class InquiryIsStatusValidator extends ConstraintValidator
{
    public function __construct(private InquiryRepository $inquiryRepository)
    {
    }

    public function validate($value, Constraint $constraint): void
    {
        if (!$constraint instanceof InquiryIsStatus) {
            throw new UnexpectedTypeException($constraint, InquiryIsStatus::class);
        }

        if (InquiryStatus::isValid($constraint->status) === false) {
            throw new UnexpectedValueException(
                $constraint->status,
                implode('|', InquiryStatus::values())
            );
        }

        if ($value === null || is_string($value) === false || $value === '') {
            return;
        }

        try {
            $inquiry = $this->inquiryRepository->get($value);
        } catch (InquiryByInquiryIdNotFoundException $exception) {
            return;
        }

        if ($inquiry->status === $constraint->status) {
            return;
        }

        $this->context->buildViolation($constraint->message)
            ->setInvalidValue($inquiry->status)
            ->setTranslationDomain($constraint->translationDomain)
            ->addViolation();
    }
}
