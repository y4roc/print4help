<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Validator\Cart;

use App\Domain\Account\Repository\UserRepository;
use App\Domain\Email;
use App\Infrastructure\MongoDb\Repository\CartRepository;
use App\Infrastructure\ReadModel\Exception\CartByCartIdNotFoundException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

final class UserHasCartValidator extends ConstraintValidator
{
    public function __construct(
        private UserRepository $userRepository,
        private CartRepository $cartRepository,
        private TokenStorageInterface $tokenStorage
    ) {
    }

    public function validate($value, Constraint $constraint): void
    {
        if (!$constraint instanceof UserHasCart) {
            throw new UnexpectedTypeException($constraint, UserHasCart::class);
        }

        $tokenStorageUser = $this->getUser();

        $user = $this->userRepository->getByEmail(
            Email::fromString($tokenStorageUser->getUsername())
        );

        $cartId = $user->getCartId();
        if ($cartId === null) {
            $this->context->buildViolation($constraint->message)
                ->setTranslationDomain($constraint->translationDomain)
                ->addViolation();
            return;
        }

        try {
            $this->cartRepository->get($cartId->toString());
        } catch (CartByCartIdNotFoundException $exception) {
            $this->context->buildViolation($constraint->message)
                ->setTranslationDomain($constraint->translationDomain)
                ->addViolation();
        }
    }

    private function getUser(): UserInterface
    {
        $token = $this->tokenStorage->getToken();

        if (!$token) {
            throw new AccessDeniedHttpException();
        }

        $user = $token->getUser();

        if (!$user instanceof UserInterface) {
            throw new AccessDeniedHttpException();
        }

        return $user;
    }
}
