<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

final class PingAction
{
    #[Route('/ping')]
    public function __invoke(): JsonResponse
    {
        return new JsonResponse(['message' => 'pong']);
    }
}
