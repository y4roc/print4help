<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Controller;

use Doctrine\DBAL\Connection;
use Doctrine\Persistence\ManagerRegistry;
use InvalidArgumentException;
use RuntimeException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Throwable;

final class HealthAction
{
    public function __construct(private ManagerRegistry $registry)
    {
    }

    #[Route('/health')]
    public function __invoke(): JsonResponse
    {
        try {
            $this->checkDatabase();
        } catch (Throwable $exception) {
            return new JsonResponse('', 500);
        }

        return new JsonResponse();
    }

    private function checkDatabase(): void
    {
        $connection = $this->registry->getConnection();

        if (!$connection instanceof Connection) {
            throw new InvalidArgumentException('No connection available');
        }

        $connect = $connection->connect();
        $connection->close();

        if ($connect === false) {
            throw new RuntimeException('Database is not available!');
        }
    }
}
