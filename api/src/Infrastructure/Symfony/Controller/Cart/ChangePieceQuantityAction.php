<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Controller\Cart;

use App\Domain\InputHelper;
use App\Domain\Market\Cart\Command\ChangePieceQuantity;
use App\Domain\Market\Piece\PieceId;
use App\Infrastructure\ReadModel\Repository\CartRepository;
use App\Infrastructure\Symfony\Request\Cart\ChangePieceQuantityRequest;
use App\Infrastructure\Symfony\Request\RequestHandler;
use App\Infrastructure\Symfony\UserTrait;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class ChangePieceQuantityAction
{
    use UserTrait;

    /**
     * @Security(name="Bearer")
     * @OA\Tag(name="Cart")
     * @OA\Response(
     *     response=201,
     *     description="The updated Cart",
     *     @OA\JsonContent(
     *          type="object",
     *          @OA\Property(
     *              property="data",
     *              ref=@Model(type=App\Infrastructure\ReadModel\Cart::class)
     *          )
     *     )
     * )
     * @OA\RequestBody(
     *     required=true,
     *     @Model(type=App\Infrastructure\Symfony\Request\Cart\ChangePieceQuantityRequest::class)
     * )
     */
    #[Route('/user/cart/change-piece-quantity', methods: ["POST"])]
    public function __invoke(
        Request $request,
        RequestHandler $requestHandler,
        MessageBusInterface $bus,
        SerializerInterface $serializer,
        CartRepository $cartRepository
    ): JsonResponse {
        $changePieceQuantityRequest = $requestHandler->handle($request, ChangePieceQuantityRequest::class);
        $changePieceQuantity = new ChangePieceQuantity(
            $this->userId(),
            PieceId::fromString(
                InputHelper::string($changePieceQuantityRequest->pieceId)
            ),
            InputHelper::int($changePieceQuantityRequest->quantity)
        );

        $bus->dispatch($changePieceQuantity);

        $cart = $cartRepository->getByUserId($this->userId()->toString());
        $serialized = $serializer->serialize(['data' => $cart,], 'json', ['skip_null_values' => true]);
        return new JsonResponse($serialized, Response::HTTP_OK, [], true);
    }
}
