<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Controller\Account;

use App\Domain\Account\Command\FinishUserRegistration;
use App\Domain\InputHelper;
use App\Infrastructure\Symfony\Request\Account\UserRegistrationFinishRequest;
use App\Infrastructure\Symfony\Request\RequestHandler;
use App\Infrastructure\Symfony\Response\DefaultResponse;
use App\Infrastructure\Symfony\Security\User;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;

final class UserRegistrationFinishAction
{
    /**
     * @OA\Tag(name="Account")
     * @OA\Response(
     *     response=200,
     *     description="Finish user registration with token",
     *     @Model(type=DefaultResponse::class)
     * )
     * @OA\Response(
     *     response=422,
     *     description="Validation Errors",
     *     @Model(type=DefaultResponse::class)
     * )
     * @OA\RequestBody(
     *     required=true,
     *     @Model(type=UserRegistrationFinishRequest::class)
     * )
     */
    #[Route('/registration/finish', methods: ["POST"])]
    public function __invoke(
        Request $request,
        RequestHandler $requestHandler,
        MessageBusInterface $bus,
        UserPasswordHasherInterface $hasher
    ): Response {
        $userRegistrationFinishRequest = $requestHandler->handle($request, UserRegistrationFinishRequest::class);
        $hashedPassword = $hasher->hashPassword(
            User::forPasswordHashing(),
            InputHelper::string($userRegistrationFinishRequest->password)
        );

        $command = new FinishUserRegistration(
            InputHelper::string($userRegistrationFinishRequest->registrationToken),
            $hashedPassword
        );
        $bus->dispatch($command);

        return new JsonResponse(new DefaultResponse(), Response::HTTP_OK);
    }
}
