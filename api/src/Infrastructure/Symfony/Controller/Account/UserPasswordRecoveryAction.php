<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Controller\Account;

use App\Domain\Account\Command\CreateUserPasswordRecovery;
use App\Domain\Email;
use App\Domain\InputHelper;
use App\Infrastructure\Symfony\Request\Account\UserPasswordRecoveryRequest;
use App\Infrastructure\Symfony\Request\RequestHandler;
use App\Infrastructure\Symfony\Response\DefaultResponse;
use App\Infrastructure\Symfony\ValidationException;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;

final class UserPasswordRecoveryAction
{
    /**
     * @OA\Tag(name="Account")
     * @OA\Response(
     *     response=200,
     *     description="Request an E-Mail to change the User's password",
     *     @Model(type=DefaultResponse::class)
     * )
     * @OA\RequestBody(
     *     required=true,
     *     @Model(type=UserPasswordRecoveryRequest::class)
     * )
     */
    #[Route('/password/recovery', methods: ['POST'])]
    public function __invoke(
        Request $request,
        RequestHandler $requestHandler,
        MessageBusInterface $bus
    ): JsonResponse {
        try {
            $userPasswordRecoveryRequest = $requestHandler->handle($request, UserPasswordRecoveryRequest::class);
        } catch (ValidationException $exception) {
            // don't tell user if the email exists or not so we always say it's successful
            return new JsonResponse(new DefaultResponse(), Response::HTTP_OK);
        }

        $passwordRecovery = new CreateUserPasswordRecovery(
            Email::fromString(
                InputHelper::string($userPasswordRecoveryRequest->email)
            ),
        );

        $bus->dispatch($passwordRecovery);

        return new JsonResponse(new DefaultResponse(), Response::HTTP_OK);
    }
}
