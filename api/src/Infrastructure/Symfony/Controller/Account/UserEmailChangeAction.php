<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Controller\Account;

use App\Domain\Account\Command\CreateUserEmailChange;
use App\Domain\Account\UserId;
use App\Domain\Email;
use App\Domain\InputHelper;
use App\Infrastructure\Symfony\Request\Account\UserEmailChangeRequest;
use App\Infrastructure\Symfony\Request\RequestHandler;
use App\Infrastructure\Symfony\Response\DefaultResponse;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;

final class UserEmailChangeAction
{
    /**
     * @Security(name="Bearer")
     * @OA\Tag(name="Account")
     * @OA\Response(
     *     response=200,
     *     description="Request an E-Mail with a token to change a User's E-Mail Address",
     *     @Model(type=DefaultResponse::class)
     * )
     * @OA\RequestBody(
     *     required=true,
     *     @Model(type=UserEmailChangeRequest::class)
     * )
     */
    #[Route('/user/settings/change-email', methods: ['POST'])]
    public function __invoke(
        Request $request,
        RequestHandler $requestHandler,
        MessageBusInterface $bus
    ): JsonResponse {
        $userEmailChangeRequest = $requestHandler->handle($request, UserEmailChangeRequest::class);
        $passwordRecovery = new CreateUserEmailChange(
            UserId::fromString(
                InputHelper::string($userEmailChangeRequest->userId)
            ),
            Email::fromString(
                InputHelper::string($userEmailChangeRequest->email)
            ),
        );

        $bus->dispatch($passwordRecovery);

        return new JsonResponse(new DefaultResponse(), Response::HTTP_OK);
    }
}
