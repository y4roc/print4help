<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Controller\Account;

use App\Domain\Account\Command\FinishUserPasswordRecovery;
use App\Domain\InputHelper;
use App\Infrastructure\Symfony\Request\Account\UserPasswordRecoveryFinishRequest;
use App\Infrastructure\Symfony\Request\RequestHandler;
use App\Infrastructure\Symfony\Response\DefaultResponse;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;

final class UserPasswordRecoveryFinishAction
{
    /**
     * @OA\Tag(name="Account")
     * @OA\Response(
     *     response=200,
     *     description="Finish user password recovery with token",
     *     @Model(type=DefaultResponse::class)
     * )
     * @OA\Response(
     *     response=422,
     *     description="Validation Errors",
     *     @Model(type=DefaultResponse::class)
     * )
     * @OA\RequestBody(
     *     required=true,
     *     @Model(type=UserPasswordRecoveryFinishRequest::class)
     * )
     */
    #[Route('/password/reset', methods: ["POST"])]
    public function __invoke(
        Request $request,
        RequestHandler $requestHandler,
        MessageBusInterface $bus
    ): JsonResponse {
        $userPasswordRecoveryFinishRequest = $requestHandler->handle(
            $request,
            UserPasswordRecoveryFinishRequest::class
        );
        $passwordRecovery = new FinishUserPasswordRecovery(
            InputHelper::string($userPasswordRecoveryFinishRequest->token),
            InputHelper::string($userPasswordRecoveryFinishRequest->newPassword)
        );

        $bus->dispatch($passwordRecovery);

        return new JsonResponse(new DefaultResponse(), Response::HTTP_OK);
    }
}
