<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Controller\Piece;

use App\Domain\IdsDoNotMatchException;
use App\Domain\InputHelper;
use App\Domain\Market\Piece\Command\BlockPiece;
use App\Domain\Market\Piece\PieceId;
use App\Infrastructure\ReadModel\Repository\PieceRepository;
use App\Infrastructure\Symfony\Request\Piece\BlockPieceRequest;
use App\Infrastructure\Symfony\Request\RequestHandler;
use App\Infrastructure\Symfony\Response\DefaultResponse;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class BlockPieceAction
{
    /**
     * @Security(name="Bearer")
     * @OA\Tag(name="Piece")
     * @OA\Response(
     *     response=200,
     *     description="Block a Piece and exclude it from everywhere.",
     *     @OA\JsonContent(
     *          type="object",
     *          @OA\Property(
     *              property="data",
     *              ref=@Model(type=App\Infrastructure\Symfony\Response\DefaultResponse::class)
     *          )
     *     )
     * )
     * @OA\RequestBody(
     *     required=true,
     *     @Model(type=App\Infrastructure\Symfony\Request\Piece\BlockPieceRequest::class)
     * )
     */
    #[Route('/piece/{uuid}/block', methods: ["POST"])]
    public function __invoke(
        Request $request,
        RequestHandler $requestHandler,
        MessageBusInterface $bus,
        SerializerInterface $serializer,
        PieceRepository $pieceRepository
    ): JsonResponse {
        $blockPieceRequest = $requestHandler->handle(
            $request,
            BlockPieceRequest::class,
            function (BlockPieceRequest $blockPieceRequest, Request $request): void {
                if ($request->get('uuid') !== $blockPieceRequest->id) {
                    throw new IdsDoNotMatchException($request->get('uuid'), $blockPieceRequest->id);
                }
            }
        );

        $requestReview = new BlockPiece(
            PieceId::fromString(InputHelper::string($blockPieceRequest->id)),
            InputHelper::nullableString($blockPieceRequest->message)
        );
        $bus->dispatch($requestReview);

        return new JsonResponse(['data' => new DefaultResponse()], Response::HTTP_OK);
    }
}
