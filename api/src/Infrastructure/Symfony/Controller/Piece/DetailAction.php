<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Controller\Piece;

use App\Domain\InputHelper;
use App\Domain\Market\Piece\PieceStatus;
use App\Infrastructure\ReadModel\Repository\PieceRepository;
use App\Infrastructure\Symfony\UserTrait;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class DetailAction
{
    use UserTrait;

    /**
     * @OA\Tag(name="Piece")
     * @OA\Response(
     *     response=200,
     *     description="List of all Pieces",
     *     @OA\JsonContent(
     *          type="object",
     *          @OA\Property(
     *              property="data",
     *              ref=@Model(type=App\Infrastructure\ReadModel\Piece::class)
     *          )
     *     )
     * )
     */
    #[Route('/piece/{articleNumber}/detail', methods: ['GET'])]
    public function __invoke(
        Request $request,
        PieceRepository $pieceRepository
    ): JsonResponse {
        $articleNumber = InputHelper::string($request->get('articleNumber'));
        $piece = $pieceRepository->getByArticleNumber($articleNumber);

        if ($piece->status === PieceStatus::approved()->toString()) {
            return new JsonResponse([
                'data' => $piece,
            ], Response::HTTP_OK);
        }

        if (!$this->authenticated()) {
            throw new NotFoundHttpException(
                sprintf(
                    'Piece with article number [%s] not found',
                    $articleNumber
                )
            );
        }

        // author of a piece can show every status except blocked
        if (
            $piece->status !== PieceStatus::blocked()->toString() &&
            $piece->userId === $this->userId()->toString()
        ) {
            return new JsonResponse([
                'data' => $piece,
            ], Response::HTTP_OK);
        }

        if (!$this->isAdmin()) {
            throw new NotFoundHttpException(
                sprintf(
                    'Piece with article number [%s] not found',
                    $articleNumber
                )
            );
        }

        return new JsonResponse(['data' => $piece], Response::HTTP_OK);
    }
}
