<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Controller\Piece;

use App\Domain\InputHelper;
use App\Infrastructure\ReadModel\Repository\PieceRepository;
use App\Infrastructure\Symfony\Request\Piece\ListRequest;
use App\Infrastructure\Symfony\ValidationException;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ListAction
{
    /**
     * @OA\Tag(name="Piece")
     * @OA\Parameter(
     *     name="categoryIds[]",
     *     in="query",
     *     @OA\Schema(
     *       type="array",
     *       @OA\Items(
     *         type="string"
     *       )
     *     ),
     *     description="Query all pieces that are stored in the given categories",
     *     example="[household.kitchen]"
     * )
     * @OA\Response(
     *     response=200,
     *     description="List of all Pieces",
     *     @OA\JsonContent(
     *          type="object",
     *          @OA\Property(
     *              property="data",
     *              ref=@Model(type=App\Infrastructure\ReadModel\Piece::class)
     *          )
     *     )
     * )
     */
    #[Route('/piece/list', methods: ['GET'])]
    public function __invoke(
        Request $request,
        ValidatorInterface $validator,
        SerializerInterface $serializer,
        PieceRepository $pieceRepository
    ): JsonResponse {
        $listRequest = ListRequest::createFromRequest($request);

        $errors = $validator->validate($listRequest);
        if (count($errors) > 0) {
            throw new ValidationException($errors);
        }

        $pieces = $pieceRepository->getAllApproved(
            InputHelper::array($listRequest->categoryIds)
        );

        return new JsonResponse(
            $serializer->serialize(['data' => $pieces], 'json'),
            Response::HTTP_OK,
            [],
            true
        );
    }
}
