<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Controller\Inquiry;

use App\Infrastructure\ReadModel\Repository\InquiryRepository;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ListAction
{
    /**
     * @OA\Tag(name="Inquiry")
     * @OA\Response(
     *     response=200,
     *     description="List of all Inquiries",
     *     @OA\JsonContent(
     *          type="object",
     *          @OA\Property(
     *              property="data",
     *              ref=@Model(type=App\Infrastructure\ReadModel\Inquiry::class)
     *          )
     *     )
     * )
     */
    #[Route('/inquiry/list', methods: ['GET'])]
    public function __invoke(
        Request $request,
        ValidatorInterface $validator,
        SerializerInterface $serializer,
        InquiryRepository $inquiryRepository
    ): JsonResponse {
        $inquiries = $inquiryRepository->getAllOpen();

        return new JsonResponse(
            $serializer->serialize(['data' => $inquiries], 'json'),
            Response::HTTP_OK,
            [],
            true
        );
    }
}
