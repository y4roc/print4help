<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Controller\Inquiry;

use App\Domain\IdsDoNotMatchException;
use App\Domain\InputHelper;
use App\Domain\Market\Inquiry\Command\UpdateInquiryMetadata;
use App\Domain\Market\Inquiry\InquiryId;
use App\Infrastructure\ReadModel\Repository\InquiryRepository;
use App\Infrastructure\Symfony\Request\Inquiry\UpdateInquiryMetadataRequest;
use App\Infrastructure\Symfony\Request\RequestHandler;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class UpdateInquiryMetadataAction
{
    /**
     * @Security(name="Bearer")
     * @OA\Tag(name="Inquiry")
     * @OA\Response(
     *     response=200,
     *     description="The updated Inquiry with its new metadata",
     *     @OA\JsonContent(
     *          type="object",
     *          @OA\Property(
     *              property="data",
     *              ref=@Model(type=App\Infrastructure\ReadModel\Inquiry::class)
     *          )
     *     )
     * )
     * @OA\RequestBody(
     *     required=true,
     *     @Model(type=App\Infrastructure\Symfony\Request\Inquiry\UpdateInquiryMetadataRequest::class)
     * )
     */
    #[Route('/inquiry/{uuid}/update', methods: ['POST'])]
    public function __invoke(
        Request $request,
        RequestHandler $requestHandler,
        MessageBusInterface $bus,
        SerializerInterface $serializer,
        InquiryRepository $inquiryRepository
    ): JsonResponse {
        $updateInquiryMetadataRequest = $requestHandler->handle(
            $request,
            UpdateInquiryMetadataRequest::class,
            function (UpdateInquiryMetadataRequest $updateInquiryMetadataRequest, Request $request): void {
                if ($request->get('uuid') !== $updateInquiryMetadataRequest->inquiryId) {
                    throw new IdsDoNotMatchException($request->get('uuid'), $updateInquiryMetadataRequest->inquiryId);
                }
            }
        );

        $inquiryId = InquiryId::fromString(InputHelper::string($updateInquiryMetadataRequest->inquiryId));
        $createPiece = new UpdateInquiryMetadata(
            $inquiryId,
            InputHelper::int($updateInquiryMetadataRequest->quantity),
            InputHelper::nullableString($updateInquiryMetadataRequest->purpose),
            InputHelper::array($updateInquiryMetadataRequest->requirements)
        );

        $bus->dispatch($createPiece);

        $updatedInquiry = $inquiryRepository->get($inquiryId->toString());
        return new JsonResponse(['data' => $updatedInquiry], Response::HTTP_OK);
    }
}
