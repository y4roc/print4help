<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Controller\Inquiry;

use App\Infrastructure\ReadModel\Repository\InquiryRepository;
use App\Infrastructure\Symfony\UserTrait;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ListMyInquriesAction
{
    use UserTrait;
    /**
     * @Security(name="Bearer")
     * @OA\Tag(name="Inquiry")
     * @OA\Response(
     *     response=200,
     *     description="List of all my Inquiries",
     *     @OA\JsonContent(
     *          type="object",
     *          @OA\Property(
     *              property="data",
     *              ref=@Model(type=App\Infrastructure\ReadModel\Inquiry::class)
     *          )
     *     )
     * )
     */
    #[Route('/inquiry/list-my', methods: ['GET'])]
    public function __invoke(
        Request $request,
        ValidatorInterface $validator,
        SerializerInterface $serializer,
        InquiryRepository $inquiryRepository
    ): JsonResponse {
        $user = $this->getUser();

        if ($user === null) {
            throw new AccessDeniedException();
        }

        $inquiries = $inquiryRepository->getByUser($user->getId()->toString());

        return new JsonResponse(
            $serializer->serialize(['data' => $inquiries], 'json'),
            Response::HTTP_OK,
            [],
            true
        );
    }
}
