<?php declare(strict_types=1);

namespace App\Infrastructure\Symfony\Request;

use App\Infrastructure\Symfony\ValidationException;
use RuntimeException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class RequestHandler
{
    private SerializerInterface $serializer;
    private ValidatorInterface $validator;

    public function __construct(SerializerInterface $serializer, ValidatorInterface $validator)
    {
        $this->serializer = $serializer;
        $this->validator = $validator;
    }

    /**
     * @template T of object
     * @param class-string<T> $class
     * @param (callable(T, Request): void)|null $finalizer
     * @return T
     */
    public function handle(Request $request, string $class, ?callable $finalizer = null): object
    {
        $object = $this->serializer->deserialize(
            $request->getContent(),
            $class,
            'json'
        );

        if (!$object instanceof $class) {
            throw new RuntimeException();
        }

        if ($finalizer) {
            $finalizer($object, $request);
        }

        $violations = $this->validator->validate($object);

        if (count($violations) > 0) {
            throw new ValidationException($violations);
        }

        return $object;
    }
}
