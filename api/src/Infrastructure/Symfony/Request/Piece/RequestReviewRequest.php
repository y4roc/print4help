<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Request\Piece;

use App\Infrastructure\Symfony\Validator\Piece\PieceBelongsToCurrentUser;
use App\Infrastructure\Symfony\Validator\Piece\PieceExists;
use App\Infrastructure\Symfony\Validator\Piece\PieceStatusAlreadySet;
use OpenApi\Annotations as OA;
use Symfony\Component\Validator\Constraints as Assert;

class RequestReviewRequest
{
    /**
     * @OA\Property(type="string", format="uuid")
     */
    #[Assert\NotBlank(message: "piece.request_review.id.not_blank")]
    #[Assert\Type(type: "string", message: "piece.request_review.id.type")]
    #[Assert\Uuid(message: "piece.request_review.id.invalid_uuid", strict: false)]
    #[PieceExists(message: "piece.request_review.id.not_found")]
    #[PieceBelongsToCurrentUser(message: "piece.request_review.id.access_denied")]
    #[PieceStatusAlreadySet(
        message: "piece.request_review.id.status_already_set",
        targetStatus: "in-review"
    )]
    public mixed $id;

    /**
     * @OA\Property(type="string", maxLength=2000)
     */
    #[Assert\Type(type: "string", message: "piece.request_review.message.type")]
    #[Assert\Length(
        max: 2000,
        maxMessage: "piece.request_review.message.max_length"
    )]
    public mixed $message = null;
}
