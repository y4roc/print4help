<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Request\Piece;

use OpenApi\Annotations as OA;
use Symfony\Component\Validator\Constraints as Assert;

class CreatePieceRequest
{
    /**
     * @OA\Property(type="string", minLength=3, maxLength=255)
     */
    #[Assert\NotBlank(message: "piece.create_piece.name.not_blank")]
    #[Assert\Type(type: "string", message: "piece.create_piece.name.type")]
    #[Assert\Length(
        min: 3,
        max: 255,
        minMessage: "piece.create_piece.name.min_length",
        maxMessage: "piece.create_piece.name.max_length"
    )]
    public mixed $name;
}
