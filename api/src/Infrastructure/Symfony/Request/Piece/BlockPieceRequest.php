<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Request\Piece;

use App\Infrastructure\Symfony\Validator\Piece\PieceExists;
use App\Infrastructure\Symfony\Validator\Piece\PieceStatusAlreadySet;
use OpenApi\Annotations as OA;
use Symfony\Component\Validator\Constraints as Assert;

class BlockPieceRequest
{
    /**
     * @OA\Property(type="string", format="uuid")
     */
    #[Assert\NotBlank(message: "piece.block_piece.id.not_blank")]
    #[Assert\Type(type: "string", message: "piece.block_piece.id.type")]
    #[Assert\Uuid(message: "piece.block_piece.id.invalid_uuid", strict: false)]
    #[PieceExists(message: "piece.block_piece.id.not_found")]
    #[PieceStatusAlreadySet(
        message: "piece.block_piece.id.status_already_set",
        targetStatus: "blocked"
    )]
    public mixed $id;

    /**
     * @OA\Property(type="string", maxLength=2000)
     */
    #[Assert\Type(type: "string", message: "piece.block_piece.message.type")]
    #[Assert\Length(
        max: 2000,
        maxMessage: "piece.block_piece.message.max_length"
    )]
    public mixed $message = null;
}
