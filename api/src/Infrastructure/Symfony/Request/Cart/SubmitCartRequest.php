<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Request\Cart;

use App\Infrastructure\Symfony\Validator\Cart\UserHasCart;

#[UserHasCart(message: "cart.update_cart_item_metadata.user_has_no_cart")]
class SubmitCartRequest
{
}
