<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Request\Cart;

use App\Infrastructure\Symfony\Validator\Cart\PieceExistsInCart;
use App\Infrastructure\Symfony\Validator\Cart\UserHasCart;
use App\Infrastructure\Symfony\Validator\Inquiry\Requirements;
use App\Infrastructure\Symfony\Validator\Piece\PieceExists;
use App\Infrastructure\Symfony\Validator\Piece\PieceIsApproved;
use OpenApi\Annotations as OA;
use Symfony\Component\Validator\Constraints as Assert;

#[UserHasCart(message: "cart.update_cart_item_metadata.user_has_no_cart")]
class UpdateCartItemMetadataRequest
{
    /**
     * @OA\Property(type="string", format="uuid")
     */
    #[Assert\NotBlank(message: "cart.update_cart_item_metadata.piece_id.not_blank")]
    #[Assert\Type(type: "string", message: "cart.update_cart_item_metadata.piece_id.type")]
    #[Assert\Uuid(message: "cart.update_cart_item_metadata.piece_id.invalid_uuid", strict: false)]
    #[PieceExists(message: "cart.update_cart_item_metadata.piece_id.not_found")]
    #[PieceIsApproved(message: "cart.update_cart_item_metadata.piece_id.not_approved")]
    #[PieceExistsInCart(message: "cart.update_cart_item_metadata.piece_id.not_found_in_cart")]
    public mixed $pieceId;

    /**
     * @OA\Property(type="string", example="My old ones broke")
     */
    #[Assert\Type(type: "string", message: "cart.update_cart_item_metadata.purpose.type")]
    #[Assert\Length(max: 1000, maxMessage: "cart.update_cart_item_metadata.purpose.max_length")]
    public mixed $purpose;

    /**
     * @OA\Property(
     *     type="array",
     *     @OA\Items(type="string", maxLength=1000),
     *     example="['Color: Red', 'Material: any']"
     * )
     */
    #[Requirements(
        message: "cart.update_cart_item_metadata.requirements.type",
        messageKeyIsNotInt: "cart.update_cart_item_metadata.requirements.key.type",
        messageValueIsNotString: "cart.update_cart_item_metadata.requirements.value.type",
        messageValueMaxLength: "cart.update_cart_item_metadata.requirements.value.max_length",
        maxLength: 1000
    )]
    public mixed $requirements;
}
