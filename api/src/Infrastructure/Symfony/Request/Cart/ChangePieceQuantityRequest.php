<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Request\Cart;

use App\Infrastructure\Symfony\Validator\Cart\PieceExistsInCart;
use App\Infrastructure\Symfony\Validator\Cart\UserHasCart;
use App\Infrastructure\Symfony\Validator\Piece\PieceExists;
use App\Infrastructure\Symfony\Validator\Piece\PieceIsApproved;
use OpenApi\Annotations as OA;
use Symfony\Component\Validator\Constraints as Assert;

#[UserHasCart(message:"cart.change_piece_quantity.user_has_no_cart")]
class ChangePieceQuantityRequest
{
    /**
     * @OA\Property(type="string", format="uuid")
     */
    #[Assert\NotBlank(message: "cart.change_piece_quantity.id.not_blank")]
    #[Assert\Type(type: "string", message: "cart.change_piece_quantity.id.type")]
    #[Assert\Uuid(message: "cart.change_piece_quantity.id.invalid_uuid", strict: false)]
    #[PieceExists(message: "cart.change_piece_quantity.id.not_found")]
    #[PieceIsApproved(message: "cart.change_piece_quantity.id.not_approved")]
    #[PieceExistsInCart(message: "cart.change_piece_quantity.id.not_found_in_cart")]
    public mixed $pieceId;

    /**
     * @OA\Property(type="int", example="5")
     */
    #[Assert\NotBlank(message: "cart.change_piece_quantity.quantity.not_blank")]
    #[Assert\Type(type: "integer", message: "cart.change_piece_quantity.quantity.type")]
    #[Assert\GreaterThanOrEqual(value: 0, message: "cart.change_piece_quantity.quantity.min")]
    #[Assert\LessThanOrEqual(value:100, message:"cart.change_piece_quantity.quantity.max")]
    public mixed $quantity;
}
