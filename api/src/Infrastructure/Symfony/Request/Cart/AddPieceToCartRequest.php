<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Request\Cart;

use App\Infrastructure\Symfony\Validator\Piece\PieceExists;
use App\Infrastructure\Symfony\Validator\Piece\PieceIsApproved;
use OpenApi\Annotations as OA;
use Symfony\Component\Validator\Constraints as Assert;

class AddPieceToCartRequest
{
    /**
     * @OA\Property(type="string", format="uuid")
     */
    #[Assert\NotBlank(message:"cart.add_piece.id.not_blank")]
     #[Assert\Type(type:"string", message:"cart.add_piece.id.type")]
     #[Assert\Uuid(message: "cart.add_piece.id.invalid_uuid", strict: false)]
     #[PieceExists(message:"cart.add_piece.id.not_found")]
     #[PieceIsApproved(message:"cart.add_piece.id.not_approved")]
    public mixed $pieceId;

    /**
     * @OA\Property(type="int", example="5")
     */
    #[Assert\NotBlank(message:"cart.add_piece.quantity.not_blank")]
    #[Assert\Type(type:"integer", message:"cart.add_piece.quantity.type")]
    #[Assert\GreaterThanOrEqual(value:1, message:"cart.add_piece.quantity.min")]
    #[Assert\LessThanOrEqual(value:100, message:"cart.add_piece.quantity.max")]
    public mixed $quantity;
}
