<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Request\Cart;

use App\Infrastructure\Symfony\Validator\Cart\PieceExistsInCart;
use App\Infrastructure\Symfony\Validator\Cart\UserHasCart;
use App\Infrastructure\Symfony\Validator\Piece\PieceExists;
use OpenApi\Annotations as OA;
use Symfony\Component\Validator\Constraints as Assert;

#[UserHasCart(message: "cart.remove_piece.user_has_no_cart")]
class RemovePieceFromCartRequest
{
    /**
     * @OA\Property(type="string", format="uuid")
     */
    #[Assert\NotBlank(message: "cart.remove_piece.id.not_blank")]
    #[Assert\Type(type: "string", message: "cart.remove_piece.id.type")]
    #[Assert\Uuid(message: "cart.remove_piece.id.invalid_uuid", strict: false)]
    #[PieceExists(message: "cart.remove_piece.id.not_found")]
    #[PieceExistsInCart(message: "cart.remove_piece.id.not_found_in_cart")]
    public mixed $pieceId;
}
