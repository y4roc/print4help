<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Request\Inquiry;

use App\Infrastructure\Symfony\Validator\Inquiry\InquiryBelongsToCurrentUser;
use App\Infrastructure\Symfony\Validator\Inquiry\InquiryExists;
use App\Infrastructure\Symfony\Validator\Inquiry\InquiryIsStatus;
use OpenApi\Annotations as OA;
use Symfony\Component\Validator\Constraints as Assert;

class CloseInquiryRequest
{
    /**
     * @OA\Property(type="string", format="uuid")
     */
    #[Assert\NotBlank(message: "inquiry.close.id.not_blank")]
    #[Assert\Type(type: "string", message: "inquiry.close.id.type")]
    #[Assert\Uuid(message: "inquiry.close.id.invalid_uuid", strict: false)]
    #[InquiryExists(message: "inquiry.close.id.not_found")]
    #[InquiryBelongsToCurrentUser(message: "inquiry.close.id.access_denied")]
    #[InquiryIsStatus(
        message: "inquiry.close.id.status_not_open",
        status: "open"
    )]
    public mixed $inquiryId;
}
