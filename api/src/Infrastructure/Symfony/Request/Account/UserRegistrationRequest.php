<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Request\Account;

use App\Infrastructure\Symfony\Validator\Account\UniqueUserEmail;
use OpenApi\Annotations as OA;
use Symfony\Component\Validator\Constraints as Assert;

class UserRegistrationRequest
{
    /**
     * @var mixed
     * @OA\Property(type="string", maxLength=255, example="John")
     */
    #[Assert\NotBlank(message: "user_registration.firstname.not_blank")]
    #[Assert\Type(type: "string", message: "user_registration.firstname.type")]
    #[Assert\Length(
        min: 2,
        max: 255,
        minMessage: "user_registration.firstname.length.min",
        maxMessage: "user_registration.firstname.length.max"
    )]
    public mixed $firstName;

    /**
     * @OA\Property(type="string", maxLength=255, example="Doe")
     */
    #[Assert\NotBlank(message: "user_registration.lastname.not_blank")]
    #[Assert\Type(type: "string", message: "user_registration.lastname.type")]
    #[Assert\Length(
        min: 2,
        max: 255,
        minMessage: "user_registration.lastname.length.min",
        maxMessage: "user_registration.lastname.length.max"
    )]
    public mixed $lastName;
    /**
     * @OA\Property(type="string", maxLength=255, example="john.doe@example.com")
     */
    #[Assert\NotBlank(message: "user_registration.email.not_blank")]
    #[Assert\Type(type: "string", message: "user_registration.email.type")]
    #[Assert\Email(message: "user_registration.email.email", mode: "strict")]
    #[UniqueUserEmail(message: "user_registration.email.email_already_exists")]
    public mixed $email;
}
