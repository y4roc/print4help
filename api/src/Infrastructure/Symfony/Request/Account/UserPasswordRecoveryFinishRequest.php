<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Request\Account;

use App\Infrastructure\Symfony\Validator\Account\UserPasswordRecoveryExistsForPasswordRecoveryToken;
use App\Infrastructure\Symfony\Validator\Account\ValidUserPasswordRecoveryToken;
use OpenApi\Annotations as OA;
use Symfony\Component\Validator\Constraints as Assert;

class UserPasswordRecoveryFinishRequest
{
    /**
     * @OA\Property(type="string", maxLength=255, example="32cd11ba8c")
     */
    #[Assert\NotBlank(message: "user_password_recovery_finish.token.not_blank")]
    #[Assert\Type(type: "string", message: "user_password_recovery_finish.token.type")]
    #[UserPasswordRecoveryExistsForPasswordRecoveryToken(message: "user_password_recovery_finish.token.does_not_exist")]
    #[ValidUserPasswordRecoveryToken(message: "user_password_recovery_finish.token.invalid")]
    public mixed $token;

    /**
     * @OA\Property(
     *     type="string",
     *     maxLength=255,
     *     example="Abcdef1!",
     *     description="The Password has to be 8 character, at least 1 Uppercase, 1 Number and 1 special-char"
     * )
     */
    #[Assert\NotBlank(message: "user_password_recovery_finish.new_password.not_blank")]
    #[Assert\Type(type: "string", message: "user_password_recovery_finish.new_password.type")]
    #[Assert\Regex(
        pattern: "/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/",
        message: "user_password_recovery_finish.new_password.regex"
    )]
    #[Assert\Length(
        min: 8,
        max: 4096,
        minMessage: "user_password_recovery_finish.new_password.min_length",
        maxMessage: "user_password_recovery_finish.new_password.max_length"
    )]
    public mixed $newPassword;

    /**
     * @OA\Property(
     *     type="string",
     *     maxLength=255,
     *     example="Abcdef1!",
     *     description="The exact same value as for [newPassword]"
     * )
     */
    #[Assert\NotBlank(message: "user_password_recovery_finish.new_password_repeat.not_blank")]
    #[Assert\Type(type: "string", message: "user_password_recovery_finish.new_password_repeat.type")]
    #[Assert\EqualTo(propertyPath: "newPassword", message: "user_password_recovery_finish.new_password_repeat.do_not_match")]
    public mixed $newPasswordRepeat;
}
