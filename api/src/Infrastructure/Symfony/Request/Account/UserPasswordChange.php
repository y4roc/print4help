<?php

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Request\Account;

use App\Infrastructure\Symfony\Validator\Account\ValidUserCurrentPassword;
use OpenApi\Annotations as OA;
use Symfony\Component\Validator\Constraints as Assert;

#[ValidUserCurrentPassword(message:"user_password_change.current_password.invalid")]
final class UserPasswordChange
{
    /**
     * @OA\Property(type="string", maxLength=255, example="d882da02-76e1-11eb-9439-0242ac130002")
     */
    #[Assert\NotBlank(message: "user_password_change.user_id.not_blank")]
    #[Assert\Type(type: "string", message: "user_password_change.user_id.type")]
    public mixed $userId;

    /**
     * @OA\Property(type="string", maxLength=255, example="Abcdef1!")
     */
    #[Assert\NotBlank(message: "user_password_change.current_password.not_blank")]
    #[Assert\Type(type: "string", message: "user_password_change.current.type")]
    public mixed $currentPassword;

    /**
     * @OA\Property(
     *     type="string",
     *     maxLength=255,
     *     example="Ghijkl1!",
     *     description="The Password has to be 8 character, at least 1 Uppercase, 1 Number and 1 special-char"
     * )
     */
    #[Assert\NotBlank(message: "user_password_change.new_password.not_blank")]
    #[Assert\Type(type: "string", message: "user_password_change.new_password.type")]
    #[Assert\Regex(
        pattern: "/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/",
        message: "user_password_change.new_password.regex"
    )]
    #[Assert\Length(
        min: 8,
        max: 4096,
        minMessage: "user_password_change.new_password.min_length",
        maxMessage: "user_password_change.new_password.max_length"
    )]
    public mixed $newPassword;

    /**
     * @OA\Property(
     *     type="string",
     *     maxLength=255,
     *     example="Abcdef1!",
     *     description="The exact same value as for [newPassword]"
     * )
     */
    #[Assert\NotBlank(message: "user_password_change.new_password_repeat.not_blank")]
    #[Assert\Type(type: "string", message: "user_password_change.new_password_repeat.type")]
    #[Assert\EqualTo(propertyPath: "newPassword", message: "user_password_change.new_password_repeat.do_not_match")]
    public mixed $newPasswordRepeat;
}
