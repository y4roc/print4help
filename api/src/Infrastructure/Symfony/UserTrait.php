<?php declare(strict_types=1);

namespace App\Infrastructure\Symfony;

use App\Domain\Account\UserId;
use App\Infrastructure\Symfony\Security\User;
use RuntimeException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\User\UserInterface;

trait UserTrait
{
    private ?TokenStorageInterface $tokenStorage = null;

    /**
     * @required
     */
    public function setTokenStorage(TokenStorageInterface $tokenStorage): void
    {
        $this->tokenStorage = $tokenStorage;
    }

    private function getToken(): TokenInterface
    {
        $tokenStorage = $this->tokenStorage;
        if (!$tokenStorage instanceof TokenStorageInterface) {
            throw new RuntimeException('TokenStorage not set!');
        }

        $token = $tokenStorage->getToken();

        if (!$token) {
            throw new AccessDeniedHttpException();
        }

        return $token;
    }

    private function getUser(): ?User
    {
        $user = $this->getToken()->getUser();

        if (!$user instanceof UserInterface) {
            return null;
        }

        if (!$user instanceof User) {
            throw new RuntimeException(sprintf('Different kind of User in Token than [%s]', User::class));
        }

        return $user;
    }

    private function userId(): UserId
    {
        $user = $this->getUser();

        if (!$user instanceof User) {
            throw new AccessDeniedHttpException();
        }

        return UserId::fromString($user->getId()->toString());
    }

    private function authenticated(): bool
    {
        try {
            $user = $this->getToken()->getUser();
            return $user instanceof User;
        } catch (AccessDeniedHttpException $e) {
            return false;
        }
    }

    private function isAdmin(): bool
    {
        $user = $this->getUser();
        if (!$user instanceof User) {
            return false;
        }

        return $user->isAdmin();
    }
}
