<?php

declare(strict_types=1);

namespace App\Infrastructure\ReadModel;

use App\Domain\Market\Piece\Category;
use OpenApi\Annotations as OA;

class Piece
{
    public string $id;

    public string $userId;

    public string $articleNumber;

    public string $status;

    public string $name;

    public ?string $summary;

    public ?string $description;

    public ?string $manufacturingNotes;

    public ?string $licence;

    public ?string $sourceUrl;

    /**
     * @var array<array-key, Category>
     * @OA\Property(
     *      property="categories",
     *      type="array",
     *      @OA\Items(
     *          @OA\Property(property="id", type="string", example="art-design"),
     *          @OA\Property(property="name", type="string", example="Art & Design"),
     *          @OA\Property(property="description", type="string", example="A nice description of the category"),
     *          @OA\Property(
     *              property="children",
     *              type="array",
     *                  @OA\Items(
     *                      @OA\Property(property="id", type="string", example="art-designn.sculptures"),
     *                      @OA\Property(property="name", type="string", example="Scultptures"),
     *                      @OA\Property(property="description", type="string", example="A nice description of the category")
     *                  )
     *              )
     *          )
     *      )
     * )
     */
    public array $categories;

    /**
     * @OA\Property(type="string", format="date-time")
     */
    public string $createdAt;

    /**
     * @OA\Property(type="string", format="date-time")
     */
    public ?string $updatedAt;
}
