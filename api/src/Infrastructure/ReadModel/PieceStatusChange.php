<?php

declare(strict_types=1);

namespace App\Infrastructure\ReadModel;

use OpenApi\Annotations as OA;

class PieceStatusChange
{
    public string $status;

    /**
     * @OA\Property(type="string", format="date-time")
     */
    public ?string $updatedAt;

    public ?string $message;

    public function __construct(string $status, ?string $updatedAt, ?string $message)
    {
        $this->status = $status;
        $this->updatedAt = $updatedAt;
        $this->message = $message;
    }
}
