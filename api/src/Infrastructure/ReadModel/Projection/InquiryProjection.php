<?php

declare(strict_types=1);

namespace App\Infrastructure\ReadModel\Projection;

use App\Domain\Market\Inquiry\Event\Closed;
use App\Domain\Market\Inquiry\Event\InquiryCreated;
use App\Domain\Market\Inquiry\Event\MetadataUpdated;
use App\Infrastructure\MongoDb\Connection;
use App\Infrastructure\ReadModel\Repository\PieceRepository;
use DateTimeImmutable;
use Patchlevel\EventSourcing\Projection\Projection;

class InquiryProjection implements Projection
{
    public const DOCUMENT_NAME = 'inquiry';

    public function __construct(
        private Connection $db,
        private PieceRepository $pieceRepository
    ) {
    }

    public function handledEvents(): iterable
    {
        yield InquiryCreated::class => 'handleInquiryCreated';
        yield MetadataUpdated::class => 'handleMetadataUpdated';
        yield Closed::class => 'handleClosed';
    }

    public function handleInquiryCreated(InquiryCreated $event): void
    {
        $piece = $this->pieceRepository->get($event->pieceId()->toString());

        $this->db->collection(self::DOCUMENT_NAME)->insertOne([
            '_id' => $event->inquiryId()->toString(),
            'userId' => $event->userId()->toString(),
            'pieceId' => $event->pieceId()->toString(),
            'articleNumber' => $piece->articleNumber,
            'status' => $event->status()->toString(),
            'quantity' => $event->quantity(),
            'purpose' => $event->purpose(),
            'requirements' => $event->requirements(),
            'createdAt' => $event->createdAt()->format(DateTimeImmutable::ATOM),
            'updatedAt' => null,
            'inExecutionAt' => null,
            'completedAt' => null,
        ]);
    }

    public function handleMetadataUpdated(MetadataUpdated $event): void
    {
        $filter = ['_id' => $event->inquiryId()->toString()];

        $this->db->collection(self::DOCUMENT_NAME)->updateOne(
            $filter,
            [
                '$set' => [
                    'quantity' => $event->quantity(),
                    'purpose' => $event->purpose(),
                    'requirements' => $event->requirements(),
                    'updatedAt' => $event->updatedAt()->format(DateTimeImmutable::ATOM),
                ],
            ]
        );
    }

    public function handleClosed(Closed $event): void
    {
        $filter = ['_id' => $event->inquiryId()->toString()];

        $this->db->collection(self::DOCUMENT_NAME)->updateOne(
            $filter,
            [
                '$set' => [
                    'status' => $event->status()->toString(),
                    'updatedAt' => $event->updatedAt()->format(DateTimeImmutable::ATOM),
                ],
            ]
        );
    }

    public function create(): void
    {
        // not needed for mongodb
    }

    public function drop(): void
    {
        $this->db->collection(self::DOCUMENT_NAME)->drop();
    }
}
