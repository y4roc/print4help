<?php

declare(strict_types=1);

namespace App\Infrastructure\ReadModel\Projection;

use App\Domain\Market\Cart\Event\CartCreated;
use App\Domain\Market\Cart\Event\CartItemMetadataUpdated;
use App\Domain\Market\Cart\Event\PieceAdded;
use App\Domain\Market\Cart\Event\PieceQuantityChanged;
use App\Domain\Market\Cart\Event\PieceRemoved;
use App\Domain\Market\Piece\PieceId;
use App\Infrastructure\MongoDb\Connection;
use DateTimeImmutable;
use Patchlevel\EventSourcing\Projection\Projection;

class CartProjection implements Projection
{
    public const DOCUMENT_NAME = 'cart';

    public function __construct(private Connection $db)
    {
    }

    public function handledEvents(): iterable
    {
        yield CartCreated::class => 'handleCartCreated';
        yield PieceAdded::class => 'handlePieceAdded';
        yield PieceQuantityChanged::class => 'handlePieceQuantityChanged';
        yield PieceRemoved::class => 'handlePieceRemoved';
        yield CartItemMetadataUpdated::class => 'handleCartItemMetadataUpdated';
    }

    public function handleCartCreated(CartCreated $event): void
    {
        $this->db->collection(self::DOCUMENT_NAME)->insertOne([
            '_id' => $event->cartId()->toString(),
            'userId' => $event->userId()->toString(),
            'createdAt' => $event->createdAt()->format(DateTimeImmutable::ATOM),
            'cartItems' => [],
        ]);
    }

    public function handlePieceAdded(PieceAdded $event): void
    {
        $filter = ['_id' => $event->cartId()->toString()];

        $this->db->collection(self::DOCUMENT_NAME)->updateOne(
            $filter,
            [
                '$push' => [
                    'cartItems' => [
                        'pieceId' => $event->pieceId()->toString(),
                        'quantity' => $event->quantity(),
                        'addedAt' => $event->addedAt()->format(DateTimeImmutable::ATOM),
                    ],
                ],
            ]
        );
    }

    public function handlePieceQuantityChanged(PieceQuantityChanged $event): void
    {
        $filter = [
            '_id' => $event->cartId()->toString(),
            'cartItems.pieceId' => $event->pieceId()->toString(),
        ];

        $this->db->collection(self::DOCUMENT_NAME)->updateOne(
            $filter,
            [
                '$set' => [
                    'cartItems.$.quantity' => $event->quantity(),
                ],
            ]
        );
    }

    public function handlePieceRemoved(PieceRemoved $event): void
    {
        $filter = ['_id' => $event->cartId()->toString()];

        $this->db->collection(self::DOCUMENT_NAME)->updateOne(
            $filter,
            [
                '$pull' => [
                    'cartItems' => [
                        'pieceId' => $event->pieceId()->toString(),
                    ],
                ],
            ]
        );
    }

    public function handleCartItemMetadataUpdated(CartItemMetadataUpdated $event): void
    {
        $filter = [
            '_id' => $event->cartId()->toString(),
            'cartItems.pieceId' => $event->pieceId()->toString(),
        ];

        $this->db->collection(self::DOCUMENT_NAME)->updateOne(
            $filter,
            [
                '$set' => [
                    'cartItems.$.purpose' => $event->purpose(),
                    'cartItems.$.requirements' => $event->requirements(),
                ],
            ]
        );
    }

    private static function getCartItemKey(PieceId $pieceId): string
    {
        return 'cartItems.' . $pieceId->toString();
    }

    public function create(): void
    {
        // not needed for mongodb
    }

    public function drop(): void
    {
        $this->db->collection(self::DOCUMENT_NAME)->drop();
    }
}
