<?php

declare(strict_types=1);

namespace App\Infrastructure\ReadModel\Projection;

use App\Domain\Market\Piece\Event\PieceApproved;
use App\Domain\Market\Piece\Event\PieceBlocked;
use App\Domain\Market\Piece\Event\PieceCreated;
use App\Domain\Market\Piece\Event\PieceRejected;
use App\Domain\Market\Piece\Event\PieceReviewRequested;
use App\Infrastructure\MongoDb\Connection;
use DateTimeImmutable;
use Patchlevel\EventSourcing\Projection\Projection;

class PieceStatusHistoryProjection implements Projection
{
    public const DOCUMENT_NAME = 'piece_status_history';

    public function __construct(private Connection $db)
    {
    }

    public function handledEvents(): iterable
    {
        yield PieceCreated::class => 'handlePieceCreated';
        yield PieceReviewRequested::class => 'handleReviewRequested';
        yield PieceApproved::class => 'handlePieceApproved';
        yield PieceRejected::class => 'handlePieceRejected';
        yield PieceBlocked::class => 'handlePieceBlocked';
    }

    public function handlePieceCreated(PieceCreated $event): void
    {
        $this->db->collection(self::DOCUMENT_NAME)->insertOne([
            '_id' => $event->pieceId()->toString(),
            'userId' => $event->userId()->toString(),
            'articleNumber' => $event->articleNumber()->toString(),
            'statusChanges' => [
                [
                    'status' => $event->status()->toString(),
                    'updatedAt' => $event->createdAt()->format(DateTimeImmutable::ATOM),
                    'message' => null,
                ],
            ],
        ]);
    }

    public function handleReviewRequested(PieceReviewRequested $event): void
    {
        $filter = ['_id' => $event->pieceId()->toString()];
        $this->db->collection(self::DOCUMENT_NAME)->updateOne(
            $filter,
            [
                '$push' => [
                    'statusChanges' => [
                        'status' => $event->status()->toString(),
                        'updatedAt' => $event->updatedAt()->format(DateTimeImmutable::ATOM),
                        'message' => $event->statusMessage(),
                    ],
                ],
            ]
        );
    }

    public function handlePieceApproved(PieceApproved $event): void
    {
        $filter = ['_id' => $event->pieceId()->toString()];
        $this->db->collection(self::DOCUMENT_NAME)->updateOne(
            $filter,
            [
                '$push' => [
                    'statusChanges' => [
                        'status' => $event->status()->toString(),
                        'updatedAt' => $event->updatedAt()->format(DateTimeImmutable::ATOM),
                        'message' => $event->statusMessage(),
                    ],
                ],
            ]
        );
    }

    public function handlePieceRejected(PieceRejected $event): void
    {
        $filter = ['_id' => $event->pieceId()->toString()];
        $this->db->collection(self::DOCUMENT_NAME)->updateOne(
            $filter,
            [
                '$push' => [
                    'statusChanges' => [
                        'status' => $event->status()->toString(),
                        'updatedAt' => $event->updatedAt()->format(DateTimeImmutable::ATOM),
                        'message' => $event->statusMessage(),
                    ],
                ],
            ]
        );
    }

    public function handlePieceBlocked(PieceBlocked $event): void
    {
        $filter = ['_id' => $event->pieceId()->toString()];
        $this->db->collection(self::DOCUMENT_NAME)->updateOne(
            $filter,
            [
                '$push' => [
                    'statusChanges' => [
                        'status' => $event->status()->toString(),
                        'updatedAt' => $event->updatedAt()->format(DateTimeImmutable::ATOM),
                        'message' => $event->statusMessage(),
                    ],
                ],
            ]
        );
    }

    public function create(): void
    {
        // not needed for mongodb
    }

    public function drop(): void
    {
        $this->db->collection(self::DOCUMENT_NAME)->drop();
    }
}
