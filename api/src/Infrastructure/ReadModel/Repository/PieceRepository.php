<?php
declare(strict_types=1);

namespace App\Infrastructure\ReadModel\Repository;

use App\Infrastructure\ReadModel\Exception\PieceByArticleNumberNotFoundException;
use App\Infrastructure\ReadModel\Exception\PieceByPieceIdNotFoundException;
use App\Infrastructure\ReadModel\Piece;

interface PieceRepository
{
    /**
     * @throws PieceByPieceIdNotFoundException
     */
    public function get(string $pieceId): Piece;

    /**
     * @throws PieceByArticleNumberNotFoundException
     */
    public function getByArticleNumber(string $articleNumber): Piece;

    /**
     * @return Piece[]
     */
    public function getAll(): array;

    /**
     * @param array<int, string> $categoryIds
     * @return Piece[]
     */
    public function getAllApproved(array $categoryIds): array;
}
