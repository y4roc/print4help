<?php
declare(strict_types=1);

namespace App\Infrastructure\ReadModel\Repository;

use App\Infrastructure\ReadModel\Exception\PieceStatusHistoryByArticleNumberNotFoundException;
use App\Infrastructure\ReadModel\Exception\PieceStatusHistoryByPieceIdNotFoundException;
use App\Infrastructure\ReadModel\PieceStatusHistory;

interface PieceStatusHistoryRepository
{
    /**
     * @throws PieceStatusHistoryByPieceIdNotFoundException
     */
    public function get(string $pieceId): PieceStatusHistory;

    /**
     * @throws PieceStatusHistoryByArticleNumberNotFoundException
     */
    public function getByArticleNumber(string $articleNumber): PieceStatusHistory;

    /**
     * @return PieceStatusHistory[]
     */
    public function getAll(): array;
}
