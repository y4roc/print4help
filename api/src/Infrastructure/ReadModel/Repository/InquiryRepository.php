<?php
declare(strict_types=1);

namespace App\Infrastructure\ReadModel\Repository;

use App\Infrastructure\ReadModel\Exception\InquiryByInquiryIdNotFoundException;
use App\Infrastructure\ReadModel\Inquiry;

interface InquiryRepository
{
    /**
     * @throws InquiryByInquiryIdNotFoundException
     */
    public function get(string $inquiryId): Inquiry;

    /**
     * @return Inquiry[]
     */
    public function getAllOpen(): array;

    /**
     * @return Inquiry[]
     */
    public function getByUser(string $userId): array;
}
