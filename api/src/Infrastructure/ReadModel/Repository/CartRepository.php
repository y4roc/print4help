<?php
declare(strict_types=1);

namespace App\Infrastructure\ReadModel\Repository;

use App\Infrastructure\ReadModel\Cart;
use App\Infrastructure\ReadModel\Exception\CartByCartIdNotFoundException;
use App\Infrastructure\ReadModel\Exception\CartByUserIdIdNotFoundException;

interface CartRepository
{
    /**
     * @throws CartByCartIdNotFoundException
     */
    public function get(string $cartId): Cart;

    /**
     * @throws CartByUserIdIdNotFoundException
     */
    public function getByUserId(string $userId): Cart;

    /**
     * @return Cart[]
     */
    public function getAll(): array;
}
