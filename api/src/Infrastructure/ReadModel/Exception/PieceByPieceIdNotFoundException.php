<?php

declare(strict_types=1);

namespace App\Infrastructure\ReadModel\Exception;

use App\Infrastructure\ReadModel\ReadModelException;

class PieceByPieceIdNotFoundException extends ReadModelException
{
    public function __construct(string $id)
    {
        parent::__construct(sprintf('Piece with id [%s] was not found', $id));
    }
}
