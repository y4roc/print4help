<?php

declare(strict_types=1);

namespace App\Infrastructure\ReadModel\Exception;

use App\Infrastructure\ReadModel\ReadModelException;

class PieceByArticleNumberNotFoundException extends ReadModelException
{
    public function __construct(string $articleNumber)
    {
        parent::__construct(sprintf('Piece with articleNumber [%s] was not found', $articleNumber));
    }
}
