<?php

declare(strict_types=1);

namespace App\Infrastructure\ReadModel\Exception;

use App\Infrastructure\ReadModel\ReadModelException;

class CartByUserIdIdNotFoundException extends ReadModelException
{
    public function __construct(string $userId)
    {
        parent::__construct(sprintf('Cart for User id [%s] was not found', $userId));
    }
}
