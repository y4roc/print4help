<?php

declare(strict_types=1);

namespace App\Infrastructure\ReadModel;

use OpenApi\Annotations as OA;

class Inquiry
{
    public string $id;

    public string $userId;

    public string $pieceId;

    public string $articleNumber;

    public string $status;

    public int $quantity;

    public ?string $purpose;

    /**
     * @var array<array-key, string>
     * @OA\Property(
     *      property="requirements",
     *      type="array",
     *      @OA\Items(
     *          type="string", example="Color: red"
     *      )
     * )
     */
    public array $requirements;

    /**
     * @OA\Property(type="string", format="date-time")
     */
    public string $createdAt;

    /**
     * @OA\Property(type="string", format="date-time")
     */
    public ?string $updatedAt;

    /**
     * @OA\Property(type="string", format="date-time")
     */
    public ?string $inExecutionAt;

    /**
     * @OA\Property(type="string", format="date-time")
     */
    public ?string $completedAt;
}
