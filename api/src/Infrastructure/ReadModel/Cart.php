<?php

declare(strict_types=1);

namespace App\Infrastructure\ReadModel;

class Cart
{
    public string $id;

    public string $userId;

    /**
     * @var CartItem[]
     */
    public array $cartItems;

    /**
     * @param CartItem[] $cartItems
     */
    public function __construct(
        string $id,
        string $userId,
        array $cartItems
    ) {
        $this->id = $id;
        $this->userId = $userId;
        $this->cartItems = $cartItems;
    }
}
