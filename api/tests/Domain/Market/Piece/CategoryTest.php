<?php

declare(strict_types=1);

namespace App\Tests\Domain\Market\Piece;

use App\Domain\Market\Piece\CategoryId;
use App\Domain\Market\Piece\CategoryRepository;
use PHPUnit\Framework\TestCase;

class CategoryTest extends TestCase
{

    /**
     * @covers \App\Domain\Market\Piece\Category::allParentIds
     * @covers \App\Domain\Market\Piece\Category::allParentIdsAsString
     */
    public function testAllParentIds(): void
    {
        $categoryRepository = new CategoryRepository();

        $category = $categoryRepository->getByCategoryId(CategoryId::toysGames_ActionFigures());
        $allParentIds = $category->allParentIds();

        self::assertContainsOnlyInstancesOf(CategoryId::class, $allParentIds);

        $allParentIdStrings = array_map(fn (CategoryId $categoryId) => $categoryId->toString(), $allParentIds);
        self::assertEquals(['toys-games', 'toys-games.action-figures'], $allParentIdStrings);
        self::assertEquals($allParentIdStrings, $category->allParentIdsAsString());
    }

    /**
     * @covers \App\Domain\Market\Piece\Category::allChildrenIds
     * @covers \App\Domain\Market\Piece\Category::allChildrenIdsAsString
     */
    public function testAllChildrenIds(): void
    {
        $categoryRepository = new CategoryRepository();
        $category = $categoryRepository->getByCategoryId(CategoryId::toysGames());

        $allChildrenIds = $category->allChildrenIds();

        self::assertContainsOnlyInstancesOf(CategoryId::class, $allChildrenIds);

        $allChildrenIdsAsString = array_map(fn (CategoryId $categoryId) => $categoryId->toString(), $allChildrenIds);

        self::assertEquals([
            'toys-games.action-figures',
            'toys-games.board-games',
            'toys-games.building-toys',
            'toys-games.other',
            'toys-games.outdoor-toys',
            'toys-games.puzzles',
            'toys-games.rpg-figures',
            'toys-games.statues',
            'toys-games.vehicles',
        ], $allChildrenIdsAsString);
        self::assertEquals($allChildrenIdsAsString, $category->allChildrenIdsAsString());
    }
}
