<?php

declare(strict_types=1);

namespace App\Tests\Infrastructure\Symfony\Validator\Inquiry;

use App\Infrastructure\MongoDb\Repository\InquiryRepository;
use App\Infrastructure\Symfony\Validator\Inquiry\InquiryIsStatus;
use App\Infrastructure\Symfony\Validator\Inquiry\InquiryIsStatusValidator;
use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;
use Symfony\Component\Validator\Exception\UnexpectedValueException;

class InquiryIsStatusValidatorTest extends TestCase
{
    use ProphecyTrait;

    public function getInvalidStatus(): iterable
    {
        yield [''];
        yield ['invalid'];
    }

    /**
     * @covers       \App\Infrastructure\Symfony\Validator\Inquiry\InquiryIsStatusValidator
     * @dataProvider getInvalidStatus
     */
    public function testStatusBlankInConstraint(string $status): void
    {
        $pieceRepository = $this->prophesize(InquiryRepository::class);

        $constraint = new InquiryIsStatus('Test', $status);
        $validator = new InquiryIsStatusValidator($pieceRepository->reveal());

        $this->expectException(UnexpectedValueException::class);
        $validator->validate('', $constraint);
    }
}
