<?php

declare(strict_types=1);

namespace App\Tests\Integration\Account;

use App\Domain\Account\UserId;
use App\Domain\Email;
use App\Tests\Integration\BaseIntegrationTest;

final class UserEmailChangeTest extends BaseIntegrationTest
{
    /**
     * @covers \App\Infrastructure\Symfony\Controller\Account\UserEmailChangeAction
     */
    public function testSuccessful(): void
    {
        $user = self::createUser(
            UserId::create(),
            Email::fromString('test@test.com'),
        );
        self::login('test@test.com');

        $response = self::post('/user/settings/change-email', [
            'userId' => $user->getId()->toString(),
            'email' => 'new-email@test.com',
        ]);

        self::assertSuccessful();
        self::assertStatusCode(200);
        $this->assertMatchesSnapshot($response);
    }

    /**
     * @covers \App\Infrastructure\Symfony\Validator\Account\UserEmailChangeDoesNotExistForUserValidator
     */
    public function testEmailChangeTokenAlreadyExists(): void
    {
        $user = self::createUser(
            UserId::create(),
            Email::fromString('test@test.com'),
        );
        self::login('test@test.com');

        $response = self::post('/user/settings/change-email', [
            'userId' => $user->getId()->toString(),
            'email' => 'new-email@test.com',
        ]);

        self::assertSuccessful();
        self::assertStatusCode(200);

        $response = self::post('/user/settings/change-email', [
            'userId' => $user->getId()->toString(),
            'email' => 'new-email-2@test.com',
        ]);
        self::assertClientError(422);
        $this->assertMatchesSnapshot($response);
    }

    /**
     * @covers \App\Infrastructure\Symfony\Validator\Account\UniqueUserEmailValidator
     */
    public function testEmailAlreadyExists(): void
    {
        $userA = self::createUser(
            UserId::create(),
            Email::fromString('test@test.com'),
        );
        $userB = self::createUser(
            UserId::create(),
            Email::fromString('user-b@test.com'),
        );
        // login as UserA
        self::login('test@test.com');

        $response = self::post('/user/settings/change-email', [
            'userId' => $userA->getId()->toString(),
            'email' => 'user-b@test.com',
        ]);

        self::assertClientError(422);
        $this->assertMatchesSnapshot($response);
    }
}
