<?php

declare(strict_types=1);

namespace App\Tests\Integration\Account;

use App\Tests\Integration\BaseIntegrationTest;

final class UserRegistrationTest extends BaseIntegrationTest
{
    /**
     * @covers \App\Infrastructure\Symfony\Controller\Account\UserRegistrationAction
     */
    public function testSuccessful(): void
    {
        $response = self::post('/registration', [
            'email' => 'test@test.com',
            'firstName' => 'John',
            'lastName' => 'Doe',
        ]);

        self::assertSuccessful();
        self::assertStatusCode(201);
        $this->assertMatchesSnapshot($response);
    }

    /**
     * @covers \App\Infrastructure\Symfony\Validator\Account\UniqueUserEmailValidator
     */
    public function testUserAlreadyExists(): void
    {
        $response = self::post('/registration', [
            'email' => 'test@test.com',
            'firstName' => 'John',
            'lastName' => 'Doe',
        ]);

        self::assertStatusCode(201);
        self::assertSuccessful();

        $response = self::post('/registration', [
            'email' => 'test@test.com',
            'firstName' => 'John',
            'lastName' => 'Doe',
        ]);

        self::assertClientError(422);
        $this->assertMatchesSnapshot($response);
    }

    /**
     * @covers \App\Infrastructure\Symfony\Request\Account\UserRegistrationRequest
     */
    public function testValidation(): void
    {
        $response = self::post('/registration', [
            'email' => 'invalid-email',
            'firstName' => '',
        ]);

        self::assertClientError(422);
        $this->assertMatchesSnapshot($response);
    }
}
