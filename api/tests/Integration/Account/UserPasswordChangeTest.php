<?php

declare(strict_types=1);

namespace App\Tests\Integration\Account;

use App\Domain\Account\UserId;
use App\Domain\Email;
use App\Tests\Integration\BaseIntegrationTest;

final class UserPasswordChangeTest extends BaseIntegrationTest
{
    /**
     * @covers \App\Infrastructure\Symfony\Controller\Account\UserPasswordChangeAction
     */
    public function testSuccessful(): void
    {
        $user = self::createUser(
            UserId::create(),
            Email::fromString('test@test.com'),
        );
        self::login('test@test.com');

        $newPassword = 'Ghijkl1!';
        $response = self::post('/user/settings/change-password', [
            'userId' => $user->getId()->toString(),
            'currentPassword' => self::PASSWORD_DEFAULT,
            'newPassword' => $newPassword,
            'newPasswordRepeat' => $newPassword,
        ]);

        self::assertSuccessful();
        self::assertStatusCode(200);
        $this->assertMatchesSnapshot($response);

        // try logging in with new password
        $response = self::post(
            '/login',
            [
                'username' => 'test@test.com',
                'password' => $newPassword,
            ]
        );

        self::assertSuccessful();
        self::assertArrayHasKey('data', $response);
        self::assertArrayHasKey('token', $response['data']);
    }

    /**
     * @covers \App\Infrastructure\Symfony\Validator\Account\ValidUserCurrentPassword
     */
    public function testInvalidCurrentPassword(): void
    {
        $user = self::createUser(
            UserId::create(),
            Email::fromString('test@test.com'),
        );
        self::login('test@test.com');

        $newPassword = 'Ghijkl1!';
        $response = self::post('/user/settings/change-password', [
            'userId' => $user->getId()->toString(),
            'currentPassword' => 'invalid',
            'newPassword' => $newPassword,
            'newPasswordRepeat' => $newPassword,
        ]);

        self::assertClientError(422);
        $this->assertMatchesSnapshot($response);
    }

    /**
     * @covers \App\Infrastructure\Symfony\Controller\Account\UserPasswordChangeAction
     */
    public function testFailsWithOldPassword(): void
    {
        $user = self::createUser(
            UserId::create(),
            Email::fromString('test@test.com'),
        );
        self::login('test@test.com');

        $newPassword = 'Ghijkl1!';
        $response = self::post('/user/settings/change-password', [
            'userId' => $user->getId()->toString(),
            'currentPassword' => self::PASSWORD_DEFAULT,
            'newPassword' => $newPassword,
            'newPasswordRepeat' => $newPassword,
        ]);

        self::assertSuccessful();
        self::assertStatusCode(200);
        $this->assertMatchesSnapshot($response);

        // try logging in with new password
        $response = self::post(
            '/login',
            [
                'username' => 'test@test.com',
                'password' => self::PASSWORD_DEFAULT,
            ]
        );

        self::assertClientError(401);
        $this->assertMatchesSnapshot($response);
    }
}
