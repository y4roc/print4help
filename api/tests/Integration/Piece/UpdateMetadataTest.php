<?php

declare(strict_types=1);

namespace App\Tests\Integration\Piece;

use App\Domain\Account\UserId;
use App\Domain\Admin\AdminUserId;
use App\Domain\Email;
use App\Domain\Market\Piece\ArticleNumber;
use App\Domain\Market\Piece\PieceId;
use App\Domain\UuidGenerator;
use App\Tests\Integration\BaseIntegrationTest;

class UpdateMetadataTest extends BaseIntegrationTest
{
    /**
     * @covers \App\Infrastructure\Symfony\Controller\Piece\UpdateMetadataAction
     */
    public function testSuccessful(): void
    {
        $user = self::createUser(
            UserId::create(),
            Email::fromString('test@test.com'),
        );
        $piece = self::createPiece($user->getId(), PieceId::create(), ArticleNumber::create(), 'Example Piece');

        self::login('test@test.com');

        $response = self::post(sprintf('/piece/%s/update', $piece->id), [
            'id' => $piece->id,
            'name' => 'Renamed Piece',
            'summary' => 'This is a sample Piece that is just updated',
            'description' => 'Here, we can describe the piece in all its detail and some use cases or whatever comes to mind',
            'manufacturingNotes' => 'Sometimes, a piece needs to be manufactured or you need some glue or screws, so this is the place for this info.',
            'licence' => 'MIT',
            'sourceUrl' => 'https://www.thingiverse.com/thing:4249113',
        ]);

        self::assertSuccessful();
        self::assertStatusCode(200);
        $this->assertMatchesSnapshot($response);
    }

    /**
     * @covers \App\Infrastructure\Symfony\Controller\Piece\UpdateMetadataAction
     */
    public function testValidationError(): void
    {
        $user = self::createUser(
            UserId::create(),
            Email::fromString('test@test.com'),
        );
        $piece = self::createPiece($user->getId(), PieceId::create(), ArticleNumber::create(), 'Example Piece');
        self::login('test@test.com');

        $response = self::post(sprintf('/piece/%s/update', $piece->id), [
            'id' => $piece->id,
            'name' => 'AB',
            'summary' => '',
        ]);

        self::assertClientError(422);
        $this->assertMatchesSnapshot($response);
    }

    /**
     * @covers \App\Infrastructure\Symfony\Controller\Piece\UpdateMetadataAction
     */
    public function testPieceNotFoundValidationError(): void
    {
        $user = self::createUser(
            UserId::create(),
            Email::fromString('test@test.com'),
        );
        self::login('test@test.com');

        $pieceId = UuidGenerator::generate();
        $response = self::post(sprintf('/piece/%s/update', $pieceId), [
            'id' => $pieceId,
            'name' => 'Renamed Piece',
            'summary' => 'This is a sample Piece that is just updated',
        ]);

        self::assertClientError(422);
        $this->assertMatchesSnapshot($response);
    }

    /**
     * @covers \App\Infrastructure\Symfony\Controller\Piece\UpdateMetadataAction
     */
    public function testInvalidUserValidationError(): void
    {
        $author = self::createUser(
            UserId::create(),
            Email::fromString('test@test.com'),
        );
        $piece = self::createPiece($author->getId(), PieceId::create(), ArticleNumber::create(), 'Example Piece');

        $user = self::createUser(
            UserId::create(),
            Email::fromString('test2@test.com'),
        );
        self::login('test2@test.com');

        $response = self::post(sprintf('/piece/%s/update', $piece->id), [
            'id' => $piece->id,
            'name' => 'Renamed Piece',
            'summary' => 'This is a sample Piece that is just updated',
            'description' => 'Here, we can describe the piece in all its detail and some use cases or whatever comes to mind',
            'manufacturingNotes' => 'Sometimes, a piece needs to be manufactured or you need some glue or screws, so this is the place for this info.',
            'licence' => 'MIT',
            'sourceUrl' => 'https://www.thingiverse.com/thing:4249113',
        ]);

        self::assertClientError(422);
        $this->assertMatchesSnapshot($response);
    }

    /**
     * @covers \App\Infrastructure\Symfony\Controller\Piece\UpdateMetadataAction
     */
    public function testIdsDoNotMatch(): void
    {
        $user = self::createUser(
            UserId::create(),
            Email::fromString('test@test.com'),
        );
        $piece = self::createPiece($user->getId(), PieceId::create(), ArticleNumber::create(), 'Example Piece');
        self::login('test@test.com');

        $response = self::post(sprintf('/piece/%s/update', $piece->id), [
            'id' => '12345',
            'name' => 'AB',
            'summary' => '',
        ]);

        self::assertClientError(400);
        $this->assertMatchesSnapshot($response);
    }

    /**
     * @covers \App\Infrastructure\Symfony\Controller\Piece\UpdateMetadataAction
     */
    public function testSuccessfulForAdmin(): void
    {
        $admin = self::createAdminUser(
            AdminUserId::create(),
            Email::fromString('admin@test.com')
        );

        $author = self::createUser(
            UserId::create(),
            Email::fromString('test@test.com'),
        );
        $piece = self::createPiece($author->getId(), PieceId::create(), ArticleNumber::create(), 'Example Piece');

        self::login('admin@test.com');

        $response = self::post(sprintf('/piece/%s/update', $piece->id), [
            'id' => $piece->id,
            'name' => 'Renamed Piece',
            'summary' => 'This is a sample Piece that is just updated',
            'description' => 'Here, we can describe the piece in all its detail and some use cases or whatever comes to mind',
            'manufacturingNotes' => 'Sometimes, a piece needs to be manufactured or you need some glue or screws, so this is the place for this info.',
            'licence' => 'MIT',
            'sourceUrl' => 'https://www.thingiverse.com/thing:4249113',
        ]);

        self::assertSuccessful();
        $this->assertMatchesSnapshot($response);
    }
}
