<?php

declare(strict_types=1);

namespace App\Tests\Integration\Piece;

use App\Domain\Account\UserId;
use App\Domain\Email;
use App\Tests\Integration\BaseIntegrationTest;

class CreatePieceTest extends BaseIntegrationTest
{
    /**
     * @covers \App\Infrastructure\Symfony\Controller\Piece\CreatePieceAction
     */
    public function testSuccessful(): void
    {
        $user = self::createUser(
            UserId::create(),
            Email::fromString('test@test.com'),
        );
        self::login('test@test.com');

        $response = self::post('/piece/create', [
           'name' => 'Example Piece',
        ]);

        self::assertSuccessful();
        self::assertStatusCode(201);
        $this->assertMatchesSnapshot($response);
    }

    /**
     * @covers \App\Infrastructure\Symfony\Controller\Piece\CreatePieceAction
     */
    public function testValidationErrors(): void
    {
        $user = self::createUser(
            UserId::create(),
            Email::fromString('test@test.com'),
        );
        self::login('test@test.com');

        $response = self::post('/piece/create', [
        ]);

        self::assertClientError(422);
        $this->assertMatchesSnapshot($response);
    }
}
