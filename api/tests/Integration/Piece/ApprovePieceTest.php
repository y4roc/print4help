<?php

declare(strict_types=1);

namespace App\Tests\Integration\Piece;

use App\Domain\Account\UserId;
use App\Domain\Admin\AdminUserId;
use App\Domain\Email;
use App\Domain\Market\Piece\ArticleNumber;
use App\Domain\Market\Piece\PieceId;
use App\Tests\Integration\BaseIntegrationTest;

class ApprovePieceTest extends BaseIntegrationTest
{
    /**
     * @covers \App\Infrastructure\Symfony\Controller\Piece\ApprovePieceAction
     */
    public function testSuccessful(): void
    {
        $user = self::createUser(
            UserId::create(),
            Email::fromString('test@test.com'),
        );
        $piece = self::createPiece($user->getId(), PieceId::create(), ArticleNumber::create(), 'Example Piece');

        $admin = self::createAdminUser(
            AdminUserId::create(),
            Email::fromString('admin@test.com')
        );
        self::login('admin@test.com');

        $response = self::post(sprintf('/piece/%s/approve', $piece->id), [
            'id' => $piece->id,
            'message' => 'Really nice piece!',
        ]);

        self::assertSuccessful();
        self::assertStatusCode(200);
        $this->assertMatchesSnapshot($response);
    }

    /**
     * @covers \App\Infrastructure\Symfony\Controller\Piece\RequestReviewAction
     * @covers \App\Infrastructure\Symfony\Validator\Piece\PieceStatusAlreadySetValidator
     */
    public function testSameStatusValidationError(): void
    {
        $user = self::createUser(
            UserId::create(),
            Email::fromString('test@test.com'),
        );
        $piece = self::createPiece($user->getId(), PieceId::create(), ArticleNumber::create(), 'Example Piece');

        $admin = self::createAdminUser(
            AdminUserId::create(),
            Email::fromString('admin@test.com')
        );
        self::login('admin@test.com');

        self::post(sprintf('/piece/%s/approve', $piece->id), [
            'id' => $piece->id,
        ]);

        self::assertSuccessful();
        self::assertStatusCode(200);

        $response = self::post(sprintf('/piece/%s/approve', $piece->id), [
            'id' => $piece->id,
        ]);

        self::assertClientError(422);
        $this->assertMatchesSnapshot($response);
    }

    /**
     * @covers \App\Infrastructure\Symfony\Controller\Piece\RequestReviewAction
     * @covers \App\Infrastructure\Symfony\Validator\Piece\PieceStatusAlreadySetValidator
     */
    public function testAccessDenied(): void
    {
        $user = self::createUser(
            UserId::create(),
            Email::fromString('test@test.com'),
        );
        $piece = self::createPiece($user->getId(), PieceId::create(), ArticleNumber::create(), 'Example Piece');

        self::login('test@test.com');

        self::post(sprintf('/piece/%s/approve', $piece->id), [
            'id' => $piece->id,
        ]);

        self::assertClientError(403);
    }

    /**
     * @covers \App\Infrastructure\Symfony\Controller\Piece\ApprovePieceAction
     */
    public function testIdsDoNotMatch(): void
    {
        $user = self::createUser(
            UserId::create(),
            Email::fromString('test@test.com'),
        );
        $piece = self::createPiece($user->getId(), PieceId::create(), ArticleNumber::create(), 'Example Piece');

        $admin = self::createAdminUser(
            AdminUserId::create(),
            Email::fromString('admin@test.com')
        );
        self::login('admin@test.com');

        $response = self::post(sprintf('/piece/%s/approve', $piece->id), [
            'id' => '12345',
            'name' => 'AB',
            'summary' => '',
        ]);

        self::assertClientError(400);
        $this->assertMatchesSnapshot($response);
    }
}
