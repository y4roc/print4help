<?php

declare(strict_types=1);

namespace App\Tests\Integration\Piece;

use App\Domain\Account\UserId;
use App\Domain\Admin\AdminUserId;
use App\Domain\Email;
use App\Domain\Market\Piece\ArticleNumber;
use App\Domain\Market\Piece\Command\ApprovePiece;
use App\Domain\Market\Piece\Command\BlockPiece;
use App\Domain\Market\Piece\Command\RejectPiece;
use App\Domain\Market\Piece\Command\RequestReview;
use App\Domain\Market\Piece\PieceId;
use App\Tests\Integration\BaseIntegrationTest;

class DetailPieceTest extends BaseIntegrationTest
{

    /**
     * @covers \App\Infrastructure\Symfony\Controller\Piece\DetailAction
     */
    public function testDraftSuccessAuthor(): void
    {
        $user = self::createUser(UserId::create(), Email::fromString('test@test.com'));
        $piece = self::createPiece($user->getId(), PieceId::create(), ArticleNumber::create(), 'Example Piece');

        self::login('test@test.com');
        $response = self::get(sprintf('/piece/%s/detail', $piece->articleNumber));

        self::assertSuccessful();
        $this->assertMatchesSnapshot($response);
    }

    /**
     * @covers \App\Infrastructure\Symfony\Controller\Piece\DetailAction
     */
    public function testDraftSuccessAdmin(): void
    {
        $admin = self::createAdminUser(AdminUserId::create(), Email::fromString('admin@test.com'));
        $user = self::createUser(UserId::create(), Email::fromString('test@test.com'));
        $piece = self::createPiece($user->getId(), PieceId::create(), ArticleNumber::create(), 'Example Piece');

        self::login('admin@test.com');
        $response = self::get(sprintf('/piece/%s/detail', $piece->articleNumber));

        self::assertSuccessful();
        $this->assertMatchesSnapshot($response);
    }

    /**
     * @covers \App\Infrastructure\Symfony\Controller\Piece\DetailAction
     */
    public function testDraftFailureAnonymous(): void
    {
        $user = self::createUser(UserId::create(), Email::fromString('test@test.com'));
        $piece = self::createPiece($user->getId(), PieceId::create(), ArticleNumber::create(), 'Example Piece');

        $response = self::get(sprintf('/piece/%s/detail', $piece->articleNumber));

        self::assertClientError(404);
        $this->assertMatchesSnapshot($response);
    }

    /**
     * @covers \App\Infrastructure\Symfony\Controller\Piece\DetailAction
     */
    public function testApprovedSuccessAuthor(): void
    {
        $user = self::createUser(UserId::create(), Email::fromString('test@test.com'));
        $piece = self::createPiece($user->getId(), PieceId::create(), ArticleNumber::create(), 'Example Piece');
        self::dispatchCommand(new ApprovePiece(PieceId::fromString($piece->id), ''));

        self::login('test@test.com');
        $response = self::get(sprintf('/piece/%s/detail', $piece->articleNumber));

        self::assertSuccessful();
        $this->assertMatchesSnapshot($response);
    }

    /**
     * @covers \App\Infrastructure\Symfony\Controller\Piece\DetailAction
     */
    public function testApprovedSuccessAdmin(): void
    {
        $admin = self::createAdminUser(AdminUserId::create(), Email::fromString('admin@test.com'));
        $user = self::createUser(UserId::create(), Email::fromString('test@test.com'));
        $piece = self::createPiece($user->getId(), PieceId::create(), ArticleNumber::create(), 'Example Piece');
        self::dispatchCommand(new ApprovePiece(PieceId::fromString($piece->id), ''));

        self::login('admin@test.com');
        $response = self::get(sprintf('/piece/%s/detail', $piece->articleNumber));

        self::assertSuccessful();
        $this->assertMatchesSnapshot($response);
    }

    /**
     * @covers \App\Infrastructure\Symfony\Controller\Piece\DetailAction
     */
    public function testApprovedSuccessAnonymous(): void
    {
        $user = self::createUser(UserId::create(), Email::fromString('test@test.com'));
        $piece = self::createPiece($user->getId(), PieceId::create(), ArticleNumber::create(), 'Example Piece');
        self::dispatchCommand(new ApprovePiece(PieceId::fromString($piece->id), ''));

        $response = self::get(sprintf('/piece/%s/detail', $piece->articleNumber));

        self::assertSuccessful();
        $this->assertMatchesSnapshot($response);
    }

    /**
     * @covers \App\Infrastructure\Symfony\Controller\Piece\DetailAction
     */
    public function testInReviewSuccessAuthor(): void
    {
        $user = self::createUser(UserId::create(), Email::fromString('test@test.com'));
        $piece = self::createPiece($user->getId(), PieceId::create(), ArticleNumber::create(), 'Example Piece');
        self::dispatchCommand(new RequestReview(PieceId::fromString($piece->id), ''));

        self::login('test@test.com');
        $response = self::get(sprintf('/piece/%s/detail', $piece->articleNumber));

        self::assertSuccessful();
        $this->assertMatchesSnapshot($response);
    }

    /**
     * @covers \App\Infrastructure\Symfony\Controller\Piece\DetailAction
     */
    public function testInReviewSuccessAdmin(): void
    {
        $admin = self::createAdminUser(AdminUserId::create(), Email::fromString('admin@test.com'));
        $user = self::createUser(UserId::create(), Email::fromString('test@test.com'));
        $piece = self::createPiece($user->getId(), PieceId::create(), ArticleNumber::create(), 'Example Piece');
        self::dispatchCommand(new RequestReview(PieceId::fromString($piece->id), ''));

        self::login('admin@test.com');
        $response = self::get(sprintf('/piece/%s/detail', $piece->articleNumber));

        self::assertSuccessful();
        $this->assertMatchesSnapshot($response);
    }

    /**
     * @covers \App\Infrastructure\Symfony\Controller\Piece\DetailAction
     */
    public function testInReviewFailureAnonymous(): void
    {
        $user = self::createUser(UserId::create(), Email::fromString('test@test.com'));
        $piece = self::createPiece($user->getId(), PieceId::create(), ArticleNumber::create(), 'Example Piece');
        self::dispatchCommand(new RequestReview(PieceId::fromString($piece->id), ''));

        $response = self::get(sprintf('/piece/%s/detail', $piece->articleNumber));

        self::assertClientError(404);
        $this->assertMatchesSnapshot($response);
    }

    /**
     * @covers \App\Infrastructure\Symfony\Controller\Piece\DetailAction
     */
    public function testRejectedSuccessAuthor(): void
    {
        $user = self::createUser(UserId::create(), Email::fromString('test@test.com'));
        $piece = self::createPiece($user->getId(), PieceId::create(), ArticleNumber::create(), 'Example Piece');
        self::dispatchCommand(new RejectPiece(PieceId::fromString($piece->id), ''));

        self::login('test@test.com');
        $response = self::get(sprintf('/piece/%s/detail', $piece->articleNumber));

        self::assertSuccessful();
        $this->assertMatchesSnapshot($response);
    }

    /**
     * @covers \App\Infrastructure\Symfony\Controller\Piece\DetailAction
     */
    public function testRejectedSuccessAdmin(): void
    {
        $admin = self::createAdminUser(AdminUserId::create(), Email::fromString('admin@test.com'));
        $user = self::createUser(UserId::create(), Email::fromString('test@test.com'));
        $piece = self::createPiece($user->getId(), PieceId::create(), ArticleNumber::create(), 'Example Piece');
        self::dispatchCommand(new RejectPiece(PieceId::fromString($piece->id), ''));

        self::login('admin@test.com');
        $response = self::get(sprintf('/piece/%s/detail', $piece->articleNumber));

        self::assertSuccessful();
        $this->assertMatchesSnapshot($response);
    }

    /**
     * @covers \App\Infrastructure\Symfony\Controller\Piece\DetailAction
     */
    public function testRejectedFailureAnonymous(): void
    {
        $user = self::createUser(UserId::create(), Email::fromString('test@test.com'));
        $piece = self::createPiece($user->getId(), PieceId::create(), ArticleNumber::create(), 'Example Piece');
        self::dispatchCommand(new RejectPiece(PieceId::fromString($piece->id), ''));

        $response = self::get(sprintf('/piece/%s/detail', $piece->articleNumber));

        self::assertClientError(404);
        $this->assertMatchesSnapshot($response);
    }

    /**
     * @covers \App\Infrastructure\Symfony\Controller\Piece\DetailAction
     */
    public function testBlockedFailureAuthor(): void
    {
        $user = self::createUser(UserId::create(), Email::fromString('test@test.com'));
        $piece = self::createPiece($user->getId(), PieceId::create(), ArticleNumber::create(), 'Example Piece');
        self::dispatchCommand(new BlockPiece(PieceId::fromString($piece->id), ''));

        self::login('test@test.com');
        $response = self::get(sprintf('/piece/%s/detail', $piece->articleNumber));

        self::assertClientError(404);
        $this->assertMatchesSnapshot($response);
    }

    /**
     * @covers \App\Infrastructure\Symfony\Controller\Piece\DetailAction
     */
    public function testBlockedSuccessAdmin(): void
    {
        $admin = self::createAdminUser(AdminUserId::create(), Email::fromString('admin@test.com'));
        $user = self::createUser(UserId::create(), Email::fromString('test@test.com'));
        $piece = self::createPiece($user->getId(), PieceId::create(), ArticleNumber::create(), 'Example Piece');
        self::dispatchCommand(new BlockPiece(PieceId::fromString($piece->id), ''));

        self::login('admin@test.com');
        $response = self::get(sprintf('/piece/%s/detail', $piece->articleNumber));

        self::assertSuccessful();
        $this->assertMatchesSnapshot($response);
    }

    /**
     * @covers \App\Infrastructure\Symfony\Controller\Piece\DetailAction
     */
    public function testBlockFailureAnonymous(): void
    {
        $user = self::createUser(UserId::create(), Email::fromString('test@test.com'));
        $piece = self::createPiece($user->getId(), PieceId::create(), ArticleNumber::create(), 'Example Piece');
        self::dispatchCommand(new BlockPiece(PieceId::fromString($piece->id), ''));

        $response = self::get(sprintf('/piece/%s/detail', $piece->articleNumber));

        self::assertClientError(404);
        $this->assertMatchesSnapshot($response);
    }
}
