<?php

declare(strict_types=1);

namespace App\Tests\Integration\Piece;

use App\Domain\Account\UserId;
use App\Domain\Email;
use App\Domain\Market\Piece\ArticleNumber;
use App\Domain\Market\Piece\CategoryId;
use App\Domain\Market\Piece\Command\ApprovePiece;
use App\Domain\Market\Piece\Command\UpdatePieceMetadata;
use App\Domain\Market\Piece\PieceId;
use App\Infrastructure\ReadModel\Piece;
use App\Tests\Integration\BaseIntegrationTest;

class ListPiecesTest extends BaseIntegrationTest
{
    /**
     * @covers \App\Infrastructure\Symfony\Controller\Piece\ListAction
     */
    public function testSuccessful(): void
    {
        $user = self::createUser(
            UserId::create(),
            Email::fromString('test@test.com'),
        );

        /** @var Piece[] $pieces */
        $pieces = [
            self::createPiece($user->getId(), PieceId::create(), ArticleNumber::create(), 'Example Piece 1'),
            self::createPiece($user->getId(), PieceId::create(), ArticleNumber::create(), 'Example Piece 2'),
            self::createPiece($user->getId(), PieceId::create(), ArticleNumber::create(), 'Example Piece 3'),
            self::createPiece($user->getId(), PieceId::create(), ArticleNumber::create(), 'Example Piece 4'),
            self::createPiece($user->getId(), PieceId::create(), ArticleNumber::create(), 'Example Piece 5'),
        ];

        foreach ($pieces as $piece) {
            self::dispatchCommand(new ApprovePiece(PieceId::fromString($piece->id), ''));
        }

        $response = self::get('/piece/list');
        self::assertSuccessful();
        $this->assertMatchesSnapshot($response);
    }

    public function categoryIdsProvider(): iterable
    {
        yield [
            [CategoryId::toysGames(), CategoryId::household_LivingRoom()],
            [CategoryId::household()],
        ];
        yield [
            [CategoryId::toysGames(), CategoryId::household_LivingRoom()],
            [CategoryId::toysGames()],
        ];
        yield [
            [CategoryId::hobbyMakers_Other(), CategoryId::fashion_Rings()],
            [CategoryId::fashion_Rings()],
        ];
        yield [
            [CategoryId::hobbyMakers_Other(), CategoryId::fashion_Rings()],
            [CategoryId::hobbyMakers()],
        ];
        yield [
            [CategoryId::worldScans(), CategoryId::healthcare()],
            [CategoryId::worldScans_Historical()],
        ];
        yield [
            [CategoryId::worldScans(), CategoryId::healthcare()],
            [CategoryId::healthcare()],
        ];
        yield [
            [CategoryId::worldScans(), CategoryId::healthcare()],
            [CategoryId::hobbyMakers()],
        ];
    }

    /**
     * @covers       \App\Infrastructure\Symfony\Controller\Piece\ListAction
     * @dataProvider categoryIdsProvider
     * @param array<int, CategoryID> $categoryIdsToSave
     * @param array<int, CategoryId> $categoryIdsToFilter
     */
    public function testCategoryFilter(array $categoryIdsToSave, array $categoryIdsToFilter): void
    {
        $user = self::createUser(
            UserId::create(),
            Email::fromString('test@test.com'),
        );

        /** @var Piece[] $pieces */
        $pieces = [
            self::createPiece($user->getId(), PieceId::create(), ArticleNumber::create(), 'Example Piece 1'),
            self::createPiece($user->getId(), PieceId::create(), ArticleNumber::create(), 'Example Piece 2'),
            self::createPiece($user->getId(), PieceId::create(), ArticleNumber::create(), 'Example Piece 3'),
            self::createPiece($user->getId(), PieceId::create(), ArticleNumber::create(), 'Example Piece 4'),
            self::createPiece($user->getId(), PieceId::create(), ArticleNumber::create(), 'Example Piece 5'),
        ];

        $idsToSaveAsString = array_map(
            fn (CategoryId $categoryId): string => $categoryId->toString(),
            $categoryIdsToSave
        );
        $idsToFilterAsString = array_map(
            fn (CategoryId $categoryId): string => $categoryId->toString(),
            $categoryIdsToFilter
        );

        foreach ($pieces as $piece) {
            self::dispatchCommand(new UpdatePieceMetadata(
                PieceId::fromString($piece->id),
                $piece->name,
                'saved-categories: ' . implode(',', $idsToSaveAsString),
                'filter-categories :' . implode(',', $idsToFilterAsString),
                $piece->manufacturingNotes,
                $piece->licence,
                $piece->sourceUrl,
                $categoryIdsToSave
            ));
        }

        foreach ($pieces as $piece) {
            /** @var $piece Piece */
            self::dispatchCommand(new ApprovePiece(PieceId::fromString($piece->id), ''));
        }

        $query = http_build_query(['categoryIds' => $idsToFilterAsString]);

        $response = self::get(sprintf('/piece/list?%s', $query));
        self::assertSuccessful();
        $this->assertMatchesSnapshot($response);
    }
}
