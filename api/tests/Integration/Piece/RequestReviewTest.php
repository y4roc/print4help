<?php

declare(strict_types=1);

namespace App\Tests\Integration\Piece;

use App\Domain\Account\UserId;
use App\Domain\Email;
use App\Domain\Market\Piece\ArticleNumber;
use App\Domain\Market\Piece\PieceId;
use App\Tests\Integration\BaseIntegrationTest;

class RequestReviewTest extends BaseIntegrationTest
{
    /**
     * @covers \App\Infrastructure\Symfony\Controller\Piece\RequestReviewAction
     */
    public function testSuccessful(): void
    {
        $user = self::createUser(
            UserId::create(),
            Email::fromString('test@test.com'),
        );
        $piece = self::createPiece($user->getId(), PieceId::create(), ArticleNumber::create(), 'Example Piece');

        self::login('test@test.com');

        $response = self::post(sprintf('/piece/%s/request-review', $piece->id), [
            'id' => $piece->id,
            'message' => 'Please review my Piece as I think it is really helpful',
        ]);

        self::assertSuccessful();
        self::assertStatusCode(200);
        $this->assertMatchesSnapshot($response);
    }

    /**
     * @covers \App\Infrastructure\Symfony\Controller\Piece\RequestReviewAction
     * @covers \App\Infrastructure\Symfony\Validator\Piece\PieceStatusAlreadySetValidator
     */
    public function testSameStatusValidationError(): void
    {
        $user = self::createUser(
            UserId::create(),
            Email::fromString('test@test.com'),
        );
        $piece = self::createPiece($user->getId(), PieceId::create(), ArticleNumber::create(), 'Example Piece');

        self::login('test@test.com');

        self::post(sprintf('/piece/%s/request-review', $piece->id), [
            'id' => $piece->id,
        ]);

        self::assertSuccessful();
        self::assertStatusCode(200);

        $response = self::post(sprintf('/piece/%s/request-review', $piece->id), [
            'id' => $piece->id,
        ]);

        self::assertClientError(422);
        $this->assertMatchesSnapshot($response);
    }

    /**
     * @covers \App\Infrastructure\Symfony\Controller\Piece\RequestReviewAction
     */
    public function testIdsDoNotMatch(): void
    {
        $user = self::createUser(
            UserId::create(),
            Email::fromString('test@test.com'),
        );
        $piece = self::createPiece($user->getId(), PieceId::create(), ArticleNumber::create(), 'Example Piece');
        self::login('test@test.com');

        $response = self::post(sprintf('/piece/%s/request-review', $piece->id), [
            'id' => '12345',
            'name' => 'AB',
            'summary' => '',
        ]);

        self::assertClientError(400);
        $this->assertMatchesSnapshot($response);
    }
}
