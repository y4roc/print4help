<?php

declare(strict_types=1);

namespace App\Tests\Integration\Piece;

use App\Domain\Account\UserId;
use App\Domain\Admin\AdminUserId;
use App\Domain\Email;
use App\Domain\Market\Piece\ArticleNumber;
use App\Domain\Market\Piece\PieceId;
use App\Tests\Integration\BaseIntegrationTest;

class PieceStatusHistoryActionTest extends BaseIntegrationTest
{
    /**
     * @covers \App\Infrastructure\Symfony\Controller\Piece\PieceStatusHistoryAction
     * @covers \App\Infrastructure\ReadModel\Projection\PieceStatusHistoryProjection
     */
    public function testSuccessful(): void
    {
        $admin = self::createAdminUser(
            AdminUserId::create(),
            Email::fromString('admin@test.com')
        );
        $user = self::createUser(
            UserId::create(),
            Email::fromString('test@test.com'),
        );
        $piece = self::createPiece($user->getId(), PieceId::create(), ArticleNumber::create(), 'Example Piece');
        ////////////////////////////////////////////
        self::login('test@test.com');
        self::post(sprintf('/piece/%s/request-review', $piece->id), [
            'id' => $piece->id,
            'message' => 'Please review my piece!',
        ]);
        self::assertSuccessful();
        self::assertStatusCode(200);
        ////////////////////////////////////////////
        self::login('admin@test.com');
        self::post(sprintf('/piece/%s/reject', $piece->id), [
            'id' => $piece->id,
            'message' => 'Please add a summary, description and a picture.',
        ]);
        self::assertSuccessful();
        self::assertStatusCode(200);
        ////////////////////////////////////////////
        self::login('test@test.com');
        self::post(sprintf('/piece/%s/request-review', $piece->id), [
            'id' => $piece->id,
            'message' => 'Please review my piece, it is perfect the way it is!',
        ]);
        self::assertSuccessful();
        self::assertStatusCode(200);
        ////////////////////////////////////////////
        self::login('admin@test.com');
        self::post(sprintf('/piece/%s/approve', $piece->id), [
            'id' => $piece->id,
            'message' => 'Okay you are right, I made a mistake',
        ]);
        self::assertSuccessful();
        self::assertStatusCode(200);
        ////////////////////////////////////////////
        self::post(sprintf('/piece/%s/block', $piece->id), [
            'id' => $piece->id,
            'message' => 'We do not allow this anymore.',
        ]);
        self::assertSuccessful();
        self::assertStatusCode(200);

        $response = self::get(sprintf('/piece/%s/status-history', $piece->id));

        self::assertSuccessful();
        self::assertStatusCode(200);
        $this->assertMatchesJsonSnapshot($response);
    }

    /**
     * @covers \App\Infrastructure\Symfony\Controller\Piece\PieceStatusHistoryAction
     */
    public function testNotFound(): void
    {
        $admin = self::createAdminUser(
            AdminUserId::create(),
            Email::fromString('admin@test.com')
        );
        self::login('admin@test.com');
        $response = self::get(sprintf('/piece/%s/status-history', '123456'));

        self::assertClientError(404);
        $this->assertMatchesSnapshot($response);
    }
}
