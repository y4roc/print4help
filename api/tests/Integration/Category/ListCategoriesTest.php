<?php

declare(strict_types=1);

namespace App\Tests\Integration\Category;

use App\Tests\Integration\BaseIntegrationTest;

class ListCategoriesTest extends BaseIntegrationTest
{
    /**
     * @covers \App\Infrastructure\Symfony\Controller\Piece\ListAction
     */
    public function testSuccessful(): void
    {
        $response = self::get('/category/list');
        self::assertSuccessful();
        $this->assertMatchesSnapshot($response);
    }
}
