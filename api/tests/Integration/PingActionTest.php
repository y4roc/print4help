<?php

declare(strict_types=1);

namespace App\Tests\Integration;

use Spatie\Snapshots\MatchesSnapshots;

/** @coversNothing  */
final class PingActionTest extends BaseIntegrationTest
{
    use MatchesSnapshots;

    public function testSuccessful(): void
    {
        $response = self::get('/ping');

        self::assertSuccessful();
        $this->assertMatchesSnapshot($response);
    }
}
