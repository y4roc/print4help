<?php

declare(strict_types=1);

namespace App\Tests\Integration;

use App\Domain\Account\Model\User;
use App\Domain\Account\Model\UserAddress;
use App\Domain\Account\Model\UserEmailChange;
use App\Domain\Account\Model\UserPasswordRecovery;
use App\Domain\Account\Repository\UserRepository;
use App\Domain\Account\UserAddressId;
use App\Domain\Account\UserId;
use App\Domain\Admin\AdminUserId;
use App\Domain\Admin\Model\AdminUser;
use App\Domain\Admin\Repository\AdminUserRepository;
use App\Domain\DateTimeHelper;
use App\Domain\Email;
use App\Domain\Market\Cart\Command\AddPieceToCart;
use App\Domain\Market\Piece\ArticleNumber;
use App\Domain\Market\Piece\ArticleNumberGenerator;
use App\Domain\Market\Piece\Command\ApprovePiece;
use App\Domain\Market\Piece\Command\CreatePiece;
use App\Domain\Market\Piece\PieceId;
use App\Domain\UuidGenerator;
use App\Infrastructure\Collector\ExceptionCollector;
use App\Infrastructure\ReadModel\Piece;
use App\Infrastructure\ReadModel\Repository\PieceRepository;
use DateTimeImmutable;
use DateTimeZone;
use RuntimeException;
use Spatie\Snapshots\MatchesSnapshots;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Messenger\Exception\HandlerFailedException;
use Symfony\Component\Messenger\MessageBusInterface;

/** @coversNothing */
abstract class BaseIntegrationTest extends WebTestCase
{
    use MatchesSnapshots;

    public const PASSWORD_DEFAULT = 'test';
    protected static KernelBrowser $client;

    protected function setUp(): void
    {
        parent::setUp();
        static::$client = static::createClient();

        $this->resetProjection();

        DateTimeHelper::override(new DateTimeImmutable(
            '18.09.2020 11:11:00',
            new DateTimeZone('Europe/Berlin')
        ));
        UuidGenerator::dummy();
        ArticleNumberGenerator::dummy();
    }

    protected function resetProjection(): void
    {
        $this->runCommand('event-sourcing:projection:drop');
    }

    protected function runCommand(string $commandName, array $parameters = []): void
    {
        $baseParameters = [
            '--env' => 'test',
            '--quiet' => null,
            'command' => $commandName,
        ];

        $this->getConsoleApplication(static::$kernel)->run(
            new ArrayInput(array_merge($baseParameters, $parameters)),
            new NullOutput()
        );
    }

    private function getConsoleApplication(KernelInterface $kernel): Application
    {
        $app = new Application($kernel);
        $app->setAutoExit(false);
        $app->setCatchExceptions(false);

        return $app;
    }

    protected static function assertSuccessful(): void
    {
        $response = self::$client->getResponse();

        if ($response->isSuccessful() === false) {
            /** @var ExceptionCollector $collector */
            $collector = static::getService(ExceptionCollector::class);
            $exception = $collector->peek();

            if ($exception !== null) {
                while ($exception instanceof HandlerFailedException) {
                    $exception = $exception->getPrevious();
                }

                throw $exception;
            }
        }

        self::assertTrue($response->isSuccessful(), 'got ' . $response->getStatusCode());
    }

    protected static function assertClientError(int $statusCode = 400): void
    {
        $response = static::$client->getResponse();
        $message = $response->isClientError() ? '' : self::extractMessage();

        self::assertEquals($statusCode, $response->getStatusCode(), $message);
    }

    protected static function assertStatusCode(int $statusCode): void
    {
        $response = static::$client->getResponse();
        $message = $response->isSuccessful() ? '' : self::extractMessage();

        self::assertEquals($statusCode, $response->getStatusCode(), $message);
    }

    protected static function assertAdminIsAuthenticated(): void
    {
        $token = self::session()->get('_security_admin');

        self::assertNotNull($token, 'The admin is not authenticated');
    }

    protected static function assertUserIsAuthenticated(): void
    {
        $token = self::session()->get('_security_main');

        self::assertNotNull($token, 'The user is not authenticated');
    }

    protected static function assertIsNotAuthenticated(): void
    {
        $token = self::session()->get('_security_main');

        self::assertNull($token, 'The user is authenticated');
    }

    protected static function post(string $url, array $data = [], $files = [])
    {
        static::$client->request('POST', $url, [], $files, ['CONTENT_TYPE' => 'application/json'], json_encode($data));

        $response = static::$client->getResponse();

        if (strpos($response->headers->get('Content-Type'), 'application/json') !== false) {
            return json_decode($response->getContent(), true);
        }

        return $response->getContent();
    }

    protected static function get(string $url)
    {
        static::$client->request('GET', $url);

        $response = static::$client->getResponse();

        if (strpos($response->headers->get('Content-Type'), 'application/json') !== false) {
            return json_decode($response->getContent(), true, 512, JSON_THROW_ON_ERROR);
        }

        return $response->getContent();
    }

    protected static function dispatchCommand(object $command): void
    {
        $commandBus = static::getService('command.bus');

        if (!$commandBus instanceof MessageBusInterface) {
            throw new RuntimeException();
        }

        $commandBus->dispatch($command);
    }

    protected static function createUser(UserId $userId, Email $email): User
    {
        $user = User::create($userId, $email, 'John', 'Doe');
        self::createRegistration($user);
        $user->updatePassword(self::PASSWORD_DEFAULT);
        $user->activate();
        self::userRepository()->save($user);

        return $user;
    }

    protected static function createRegistration(User $user): void
    {
        $user->createRegistration();
    }

    protected static function createAdminUser(
        AdminUserId $adminUserId,
        Email $email,
        string $password = self::PASSWORD_DEFAULT
    ): AdminUser {
        $adminUser = AdminUser::create($adminUserId, $email);
        $adminUser->updatePassword($password);

        static::adminUserRepository()->save($adminUser);

        return $adminUser;
    }

    protected static function createUserPasswordRecovery(User $user): UserPasswordRecovery
    {
        $user->createPasswordRecovery();

        $userPasswordRecovery = $user->getPasswordRecovery();

        self::userRepository()->save($user);

        return $userPasswordRecovery;
    }

    protected static function createUserEmailChange(User $user, Email $email): UserEmailChange
    {
        $user->createEmailChange($email);

        $userEmailChange = $user->getEmailChange();

        self::userRepository()->save($user);

        return $userEmailChange;
    }

    protected static function changeName(UserId $userId, string $firstName, string $lastName): User
    {
        $user = self::userRepository()->get($userId);
        $user->updateName($firstName, $lastName);
        self::userRepository()->save($user);

        return $user;
    }

    protected static function createAddress(UserAddressId $addressId, User $user): UserAddress
    {
        $address = UserAddress::create(
            $addressId,
            $user,
            'Max Muster',
            'Musterweg',
            '1',
            '12345',
            'Musterhausen',
            'Deutschland',
            'c/o Musterfirma'
        );

        self::userRepository()->save($user);

        return $address;
    }

    protected static function login(string $username, string $password = self::PASSWORD_DEFAULT): void
    {
        static::$client->request(
            'POST',
            '/login',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode(['username' => $username, 'password' => $password])
        );

        $response = static::$client->getResponse();
        $data = $response->getContent();

        if (strpos($response->headers->get('Content-Type'), 'application/json') !== false) {
            $data = json_decode($data, true);
        }

        self::assertSuccessful();
        self::$client->setServerParameter('HTTP_X-AUTH-TOKEN', sprintf('Bearer %s', $data['data']['token']));
    }

    protected static function createPiece(
        UserId $userId,
        PieceId $pieceId,
        ArticleNumber $articleNumber,
        string $name
    ): Piece {
        static::dispatchCommand(new CreatePiece(
            $userId,
            $pieceId,
            $articleNumber,
            $name
        ));

        $pieceRepository = static::getService(PieceRepository::class);
        if (!$pieceRepository instanceof PieceRepository) {
            throw new RuntimeException();
        }

        return $pieceRepository->get($pieceId->toString());
    }

    protected static function createAndApprovePiece(
        UserId $userId,
        PieceId $pieceId,
        ArticleNumber $articleNumber,
        string $name
    ): Piece {
        $piece = self::createPiece($userId, $pieceId, $articleNumber, $name);
        self::dispatchCommand(new ApprovePiece($pieceId, ''));
        return $piece;
    }

    protected static function addPieceToCart(
        UserId $userId,
        PieceId $pieceId,
        int $quantity
    ): void {
        static::dispatchCommand(new AddPieceToCart($userId, $pieceId, $quantity));
    }

    protected static function getService(string $id)
    {
        return static::$kernel->getContainer()->get('test.service_container')->get($id);
    }

    private static function extractMessage(): string
    {
        $response = static::$client->getResponse();
        $message = '';

        if (strpos($response->headers->get('Content-Type'), 'text/html') !== false) {
            $crawler = static::$client->getCrawler();

            if ($crawler && $crawler->filter('h1.exception-message')->count()) {
                $message = $crawler->filter('h1.exception-message')->text();

                foreach ($crawler->filter('.trace-message') as $node) {
                    $message .= "\n" . $node->textContent;
                }
            }
        }

        $exception = static::exceptionCollector()->peek();

        if (!$message && $exception) {
            while ($exception instanceof HandlerFailedException) {
                $exception = current($exception->getNestedExceptions());
            }

            throw $exception;
        }

        return $message ?: $response->getContent();
    }

    protected static function userRepository(): UserRepository
    {
        return static::getService(UserRepository::class);
    }

    protected static function adminUserRepository(): AdminUserRepository
    {
        return static::getService(AdminUserRepository::class);
    }

    private static function session(): SessionInterface
    {
        return static::getService(SessionInterface::class);
    }

    private static function exceptionCollector(): ExceptionCollector
    {
        return static::getService(ExceptionCollector::class);
    }
}
