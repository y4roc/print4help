<?php

declare(strict_types=1);

namespace App\Tests\Integration;

use Spatie\Snapshots\MatchesSnapshots;

/** @coversNothing  */
final class HealthActionTest extends BaseIntegrationTest
{
    use MatchesSnapshots;

    public function testSuccessful(): void
    {
        $response = self::get('/health');

        self::assertSuccessful();
        $this->assertMatchesSnapshot($response);
    }
}
