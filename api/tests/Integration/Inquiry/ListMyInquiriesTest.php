<?php

declare(strict_types=1);

namespace App\Tests\Integration\Inquiry;

use App\Domain\Account\UserId;
use App\Domain\Email;
use App\Domain\Market\Cart\Command\AddPieceToCart;
use App\Domain\Market\Cart\Command\SubmitCart;
use App\Domain\Market\Cart\Command\UpdateCartItemMetadata;
use App\Domain\Market\Piece\ArticleNumber;
use App\Domain\Market\Piece\PieceId;
use App\Infrastructure\ReadModel\Piece;
use App\Tests\Integration\BaseIntegrationTest;

class ListMyInquiriesTest extends BaseIntegrationTest
{
    /**
     * @covers \App\Infrastructure\Symfony\Controller\Inquiry\ListMyInquriesAction
     */
    public function testSuccessfulOneCart(): void
    {
        $maker = self::createUser(
            UserId::create(),
            Email::fromString('maker@test.com'),
        );

        /** @var Piece[] $pieces */
        $pieces = [
            self::createAndApprovePiece($maker->getId(), PieceId::create(), ArticleNumber::create(), 'Example Piece 1'),
            self::createAndApprovePiece($maker->getId(), PieceId::create(), ArticleNumber::create(), 'Example Piece 2'),
        ];

        $user = self::createUser(
            UserId::create(),
            Email::fromString('user@test.com')
        );

        foreach ($pieces as $i => $piece) {
            static::dispatchCommand(
                new AddPieceToCart($user->getId(), PieceId::fromString($piece->id), 10)
            );

            static::dispatchCommand(
                new UpdateCartItemMetadata(
                    $user->getId(),
                    PieceId::fromString($piece->id),
                    'Lorem Ipsum dolor ' . $i,
                    ['Color: Red ' . $i, 'Material: something strong ' . $i * $i]
                )
            );
        }

        static::dispatchCommand(
            new SubmitCart($user->getId())
        );

        self::login('user@test.com');
        $response = self::get('/inquiry/list-my');
        self::assertSuccessful();
        $this->assertMatchesSnapshot($response);
    }

    /**
     * @covers \App\Infrastructure\Symfony\Controller\Inquiry\ListMyInquriesAction
     */
    public function testSuccessfulMultipleCarts(): void
    {
        $maker = self::createUser(
            UserId::create(),
            Email::fromString('maker@test.com'),
        );

        /** @var Piece[] $pieces */
        $pieces = [
            self::createAndApprovePiece($maker->getId(), PieceId::create(), ArticleNumber::create(), 'Example Piece 1'),
            self::createAndApprovePiece($maker->getId(), PieceId::create(), ArticleNumber::create(), 'Example Piece 2'),
            self::createAndApprovePiece($maker->getId(), PieceId::create(), ArticleNumber::create(), 'Example Piece 3'),
            self::createAndApprovePiece($maker->getId(), PieceId::create(), ArticleNumber::create(), 'Example Piece 4'),
            self::createAndApprovePiece($maker->getId(), PieceId::create(), ArticleNumber::create(), 'Example Piece 5'),
        ];

        $user = self::createUser(
            UserId::create(),
            Email::fromString('user@test.com')
        );

        static::dispatchCommand(new AddPieceToCart($user->getId(), PieceId::fromString($pieces[0]->id), 1));
        static::dispatchCommand(new AddPieceToCart($user->getId(), PieceId::fromString($pieces[1]->id), 2));
        static::dispatchCommand(new SubmitCart($user->getId())); // cart gets cleared

        static::dispatchCommand(new AddPieceToCart($user->getId(), PieceId::fromString($pieces[2]->id), 3));
        static::dispatchCommand(new AddPieceToCart($user->getId(), PieceId::fromString($pieces[3]->id), 4));
        static::dispatchCommand(new SubmitCart($user->getId())); // cart gets cleared

        static::dispatchCommand(new AddPieceToCart($user->getId(), PieceId::fromString($pieces[4]->id), 5));
        static::dispatchCommand(new SubmitCart($user->getId()));

        self::login('user@test.com');
        $response = self::get('/inquiry/list-my');
        self::assertSuccessful();
        $this->assertMatchesSnapshot($response);
    }
}
