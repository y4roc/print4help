<?php

declare(strict_types=1);

namespace App\Tests\Integration\Inquiry;

use App\Domain\Account\UserId;
use App\Domain\Email;
use App\Domain\Market\Cart\Command\AddPieceToCart;
use App\Domain\Market\Cart\Command\SubmitCart;
use App\Domain\Market\Piece\ArticleNumber;
use App\Domain\Market\Piece\PieceId;
use App\Infrastructure\ReadModel\Piece;
use App\Tests\Integration\BaseIntegrationTest;

class CloseInquiryTest extends BaseIntegrationTest
{
    private static function getInquiryId(array $inquiries, int $index): string
    {
        self::assertArrayHasKey('data', $inquiries);
        self::assertArrayHasKey($index, $inquiries['data']);
        self::assertArrayHasKey('id', $inquiries['data'][$index]);

        return $inquiries['data'][$index]['id'];
    }

    /**
     * @covers \App\Infrastructure\Symfony\Controller\Inquiry\CloseInquiryAction
     */
    public function testSuccessful(): void
    {
        $pieceId = PieceId::create();
        $maker = self::createUser(UserId::create(), Email::fromString('maker@test.com'));
        $piece = self::createAndApprovePiece($maker->getId(), $pieceId, ArticleNumber::create(), 'Example Piece');

        $user = self::createUser(UserId::create(), Email::fromString('test@test.com'));
        self::dispatchCommand(new AddPieceToCart($user->getId(), $pieceId, 10));
        static::dispatchCommand(new SubmitCart($user->getId()));

        self::login('test@test.com');
        $inquiries = self::get('/inquiry/list-my');
        $inquiryId = self::getInquiryId($inquiries, 0);

        $response = self::post(sprintf('/inquiry/%s/close', $inquiryId), [
            'inquiryId' => $inquiryId,
        ]);

        self::assertSuccessful();
        self::assertStatusCode(200);
        $this->assertMatchesSnapshot($response);
    }

    /**
     * @covers \App\Infrastructure\Symfony\Controller\Inquiry\CloseInquiryAction
     */
    public function testSuccessfulShowInMyList(): void
    {
        $pieceId = PieceId::create();
        $maker = self::createUser(UserId::create(), Email::fromString('maker@test.com'));
        $piece = self::createAndApprovePiece($maker->getId(), $pieceId, ArticleNumber::create(), 'Example Piece');

        $user = self::createUser(UserId::create(), Email::fromString('test@test.com'));
        self::dispatchCommand(new AddPieceToCart($user->getId(), $pieceId, 10));
        static::dispatchCommand(new SubmitCart($user->getId()));

        self::login('test@test.com');
        $inquiries = self::get('/inquiry/list-my');
        $inquiryId = self::getInquiryId($inquiries, 0);

        $closeResponse = self::post(sprintf('/inquiry/%s/close', $inquiryId), [
            'inquiryId' => $inquiryId,
        ]);
        self::assertSuccessful();
        self::assertStatusCode(200);

        $response = self::get('/inquiry/list-my');
        $this->assertMatchesSnapshot($response);
    }

    /**
     * @covers \App\Infrastructure\Symfony\Controller\Inquiry\ListAction
     */
    public function testRemoveClosedFromPublicList(): void
    {
        $maker = self::createUser(
            UserId::create(),
            Email::fromString('maker@test.com'),
        );

        /** @var Piece[] $pieces */
        $pieces = [
            self::createAndApprovePiece($maker->getId(), PieceId::create(), ArticleNumber::create(), 'Example Piece 1'),
            self::createAndApprovePiece($maker->getId(), PieceId::create(), ArticleNumber::create(), 'Example Piece 2'),
            self::createAndApprovePiece($maker->getId(), PieceId::create(), ArticleNumber::create(), 'Example Piece 3'),
            self::createAndApprovePiece($maker->getId(), PieceId::create(), ArticleNumber::create(), 'Example Piece 4'),
            self::createAndApprovePiece($maker->getId(), PieceId::create(), ArticleNumber::create(), 'Example Piece 5'),
        ];

        foreach ($pieces as $i => $piece) {
            $user = self::createUser(
                UserId::create(),
                Email::fromString(sprintf('user_%s@test.com', $i)),
            );

            static::dispatchCommand(
                new AddPieceToCart($user->getId(), PieceId::fromString($piece->id), 1 + $i)
            );
            static::dispatchCommand(
                new SubmitCart($user->getId())
            );
        }

        $responseListWithAllInquiries = self::get('/inquiry/list');
        $inquiryId = self::getInquiryId($responseListWithAllInquiries, 0);
        self::assertSuccessful();

        self::login('user_0@test.com');
        $closeResponse = self::post(sprintf('/inquiry/%s/close', $inquiryId), [
            'inquiryId' => $inquiryId,
        ]);
        self::assertSuccessful();
        self::assertStatusCode(200);

        $responseListWithOneMissing = self::get('/inquiry/list');
        $this->assertMatchesSnapshot($responseListWithOneMissing);
    }
}
