<?php

declare(strict_types=1);

namespace App\Tests\Integration\Inquiry;

use App\Domain\Account\UserId;
use App\Domain\Email;
use App\Domain\Market\Cart\Command\AddPieceToCart;
use App\Domain\Market\Cart\Command\SubmitCart;
use App\Domain\Market\Inquiry\InquiryId;
use App\Domain\Market\Piece\ArticleNumber;
use App\Domain\Market\Piece\PieceId;
use App\Tests\Integration\BaseIntegrationTest;

class UpdateInquiryMetadataTest extends BaseIntegrationTest
{
    private static function getInquiryId(array $inquiries, int $index): string
    {
        self::assertArrayHasKey('data', $inquiries);
        self::assertArrayHasKey($index, $inquiries['data']);
        self::assertArrayHasKey('id', $inquiries['data'][$index]);

        return $inquiries['data'][$index]['id'];
    }

    /**
     * @covers \App\Infrastructure\Symfony\Controller\Inquiry\UpdateInquiryMetadataAction
     */
    public function testSuccessful(): void
    {
        $pieceId = PieceId::create();
        $maker = self::createUser(UserId::create(), Email::fromString('maker@test.com'));
        $piece = self::createAndApprovePiece($maker->getId(), $pieceId, ArticleNumber::create(), 'Example Piece');

        $user = self::createUser(UserId::create(), Email::fromString('test@test.com'));
        self::dispatchCommand(new AddPieceToCart($user->getId(), $pieceId, 10));
        static::dispatchCommand(new SubmitCart($user->getId()));

        self::login('test@test.com');
        $inquiries = self::get('/inquiry/list-my');
        $inquiryId = self::getInquiryId($inquiries, 0);

        $response = self::post(sprintf('/inquiry/%s/update', $inquiryId), [
            'inquiryId' => $inquiryId,
            'quantity' => 15,
            'purpose' => 'My old one broke',
            'requirements' => [
                'Color: any',
                'Material: PETG',
            ],
        ]);

        self::assertSuccessful();
        self::assertStatusCode(200);
        $this->assertMatchesSnapshot($response);
    }

    /**
     * @covers \App\Infrastructure\Symfony\Controller\Inquiry\UpdateInquiryMetadataAction
     */
    public function testSuccessfulMultiple(): void
    {
        $pieceId = PieceId::create();
        $maker = self::createUser(UserId::create(), Email::fromString('maker@test.com'));
        $piece = self::createAndApprovePiece($maker->getId(), $pieceId, ArticleNumber::create(), 'Example Piece');

        $user = self::createUser(UserId::create(), Email::fromString('test@test.com'));
        self::dispatchCommand(new AddPieceToCart($user->getId(), $pieceId, 10));
        static::dispatchCommand(new SubmitCart($user->getId()));

        self::login('test@test.com');
        $inquiries = self::get('/inquiry/list-my');
        $inquiryId = self::getInquiryId($inquiries, 0);

        $response = self::post(sprintf('/inquiry/%s/update', $inquiryId), [
            'inquiryId' => $inquiryId,
            'quantity' => 15,
            'purpose' => 'My old one broke',
            'requirements' => [
                'Color: any',
                'Material: PETG',
            ],
        ]);
        self::assertSuccessful();
        self::assertStatusCode(200);

        $response = self::post(sprintf('/inquiry/%s/update', $inquiryId), [
            'inquiryId' => $inquiryId,
            'quantity' => 20,
            'purpose' => 'My old one broke because it was made with PLA',
            'requirements' => [
                'Color: blue if you have',
                'Material: PETG or ABS',
            ],
        ]);

        $this->assertMatchesSnapshot($response);
    }

    public function invalidData(): iterable
    {
        yield ['quantity' => 0, 'purpose' => 1234, 'requirements' => ''];
        yield ['quantity' => '1', 'purpose' => str_repeat('A', 1001), 'requirements' => ''];
        yield ['quantity' => 'test', 'purpose' => '', 'requirements' => ['test' => 'test']];
        yield ['quantity' => null, 'purpose' => '', 'requirements' => ['test' => 1234]];
        yield ['quantity' => 1000, 'purpose' => '', 'requirements' => ['test' => [1234]]];
        yield ['quantity' => -10, 'purpose' => '', 'requirements' => ['test' => ['test']]];
        yield ['quantity' => 10, 'purpose' => '', 'requirements' => ['valid', 'invalid.key' => 'invalid']];
        yield ['quantity' => 10, 'purpose' => '', 'requirements' => [str_repeat('A', 1001)]];
    }

    /**
     * @covers       \App\Infrastructure\Symfony\Controller\Inquiry\UpdateInquiryMetadataAction
     * @covers       \App\Infrastructure\Symfony\Request\Inquiry\UpdateInquiryMetadataRequest
     * @dataProvider invalidData
     */
    public function testValidationErrorPurposeAndRequirements(
        mixed $quantity,
        mixed $purpose,
        mixed $requirements
    ): void {
        $pieceId = PieceId::create();
        $maker = self::createUser(UserId::create(), Email::fromString('maker@test.com'));
        $piece = self::createAndApprovePiece($maker->getId(), $pieceId, ArticleNumber::create(), 'Example Piece');

        $user = self::createUser(UserId::create(), Email::fromString('test@test.com'));
        self::dispatchCommand(new AddPieceToCart($user->getId(), $pieceId, 10));
        static::dispatchCommand(new SubmitCart($user->getId()));

        self::login('test@test.com');
        $inquiries = self::get('/inquiry/list-my');
        $inquiryId = self::getInquiryId($inquiries, 0);

        $response = self::post(sprintf('/inquiry/%s/update', $inquiryId), [
            'inquiryId' => $inquiryId,
            'quantity' => $quantity,
            'purpose' => $purpose,
            'requirements' => $requirements,
        ]);

        self::assertClientError(422);
        $this->assertMatchesSnapshot($response);
    }

    /**
     * @covers \App\Infrastructure\Symfony\Controller\Inquiry\UpdateInquiryMetadataAction
     * @covers \App\Infrastructure\Symfony\Validator\Inquiry\InquiryBelongsToCurrentUser
     */
    public function testValidationFailedUserAccessForeignInquiry(): void
    {
        $pieceId = PieceId::create();
        $maker = self::createUser(UserId::create(), Email::fromString('maker@test.com'));
        $piece = self::createAndApprovePiece($maker->getId(), $pieceId, ArticleNumber::create(), 'Example Piece');

        $user1 = self::createUser(UserId::create(), Email::fromString('test@test.com'));
        self::dispatchCommand(new AddPieceToCart($user1->getId(), $pieceId, 10));
        static::dispatchCommand(new SubmitCart($user1->getId()));

        self::login('test@test.com');
        $inquiries = self::get('/inquiry/list-my');
        $inquiryId = self::getInquiryId($inquiries, 0);

        $user2 = self::createUser(UserId::create(), Email::fromString('test2@test.com'));
        self::login('test2@test.com');
        $response = self::post(sprintf('/inquiry/%s/update', $inquiryId), [
            'inquiryId' => $inquiryId,
            'quantity' => 5,
            'requirements' => [],
        ]);
        self::assertClientError(422);
        $this->assertMatchesSnapshot($response);
    }

    /**
     * @covers \App\Infrastructure\Symfony\Controller\Inquiry\UpdateInquiryMetadataAction
     * @covers \App\Infrastructure\Symfony\Validator\Inquiry\InquiryExists
     */
    public function testValidationFailedInquiryExists(): void
    {
        $user = self::createUser(UserId::create(), Email::fromString('test@test.com'));
        self::login('test@test.com');
        $inquiryId = InquiryId::create()->toString();

        $response = self::post(sprintf('/inquiry/%s/update', $inquiryId), [
            'inquiryId' => $inquiryId,
            'quantity' => 5,
            'requirements' => [],
        ]);
        self::assertClientError(422);
        $this->assertMatchesSnapshot($response);
    }
}
