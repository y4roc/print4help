<?php

declare(strict_types=1);

namespace App\Tests\Integration\Inquiry;

use App\Domain\Account\UserId;
use App\Domain\Email;
use App\Domain\Market\Cart\Command\AddPieceToCart;
use App\Domain\Market\Cart\Command\SubmitCart;
use App\Domain\Market\Cart\Command\UpdateCartItemMetadata;
use App\Domain\Market\Piece\ArticleNumber;
use App\Domain\Market\Piece\PieceId;
use App\Infrastructure\ReadModel\Piece;
use App\Tests\Integration\BaseIntegrationTest;

class ListInquiriesTest extends BaseIntegrationTest
{
    /**
     * @covers \App\Infrastructure\Symfony\Controller\Inquiry\ListAction
     */
    public function testSuccessful(): void
    {
        $maker = self::createUser(
            UserId::create(),
            Email::fromString('maker@test.com'),
        );

        /** @var Piece[] $pieces */
        $pieces = [
            self::createAndApprovePiece($maker->getId(), PieceId::create(), ArticleNumber::create(), 'Example Piece 1'),
            self::createAndApprovePiece($maker->getId(), PieceId::create(), ArticleNumber::create(), 'Example Piece 2'),
            self::createAndApprovePiece($maker->getId(), PieceId::create(), ArticleNumber::create(), 'Example Piece 3'),
            self::createAndApprovePiece($maker->getId(), PieceId::create(), ArticleNumber::create(), 'Example Piece 4'),
            self::createAndApprovePiece($maker->getId(), PieceId::create(), ArticleNumber::create(), 'Example Piece 5'),
        ];

        foreach ($pieces as $i => $piece) {
            $user = self::createUser(
                UserId::create(),
                Email::fromString(sprintf('user_%s@test.com', $i)),
            );

            static::dispatchCommand(
                new AddPieceToCart($user->getId(), PieceId::fromString($piece->id), 1 + $i)
            );
            static::dispatchCommand(
                new UpdateCartItemMetadata(
                    $user->getId(),
                    PieceId::fromString($piece->id),
                    'Lorem Ipsum dolor ' . $i,
                    ['Color: Red ' . $i, 'Material: something strong ' . $i * $i]
                )
            );
            static::dispatchCommand(
                new SubmitCart($user->getId())
            );
        }

        $response = self::get('/inquiry/list');
        self::assertSuccessful();
        $this->assertMatchesSnapshot($response);
    }
}
