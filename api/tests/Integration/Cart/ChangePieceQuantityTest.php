<?php

declare(strict_types=1);

namespace App\Tests\Integration\Cart;

use App\Domain\Account\UserId;
use App\Domain\Email;
use App\Domain\Market\Cart\Command\AddPieceToCart;
use App\Domain\Market\Cart\Command\RemovePieceFromCart;
use App\Domain\Market\Piece\ArticleNumber;
use App\Domain\Market\Piece\Command\ApprovePiece;
use App\Domain\Market\Piece\Command\RejectPiece;
use App\Domain\Market\Piece\PieceId;
use App\Tests\Integration\BaseIntegrationTest;

class ChangePieceQuantityTest extends BaseIntegrationTest
{
    /**
     * @covers \App\Infrastructure\Symfony\Controller\Cart\ChangePieceQuantityAction
     */
    public function testSuccessful(): void
    {
        $pieceId = PieceId::create();
        $user = self::createUser(UserId::create(), Email::fromString('test@test.com'), );
        $piece = self::createPiece($user->getId(), $pieceId, ArticleNumber::create(), 'Example Piece');
        self::dispatchCommand(new ApprovePiece($pieceId, ''));
        self::dispatchCommand(new AddPieceToCart($user->getId(), $pieceId, 10));
        self::login('test@test.com');

        $response = self::post('/user/cart/change-piece-quantity', [
            'pieceId' => $piece->id,
            'quantity' => 5,
        ]);

        self::assertSuccessful();
        self::assertStatusCode(200);
        $this->assertMatchesSnapshot($response);
    }

    /**
     * @covers \App\Infrastructure\Symfony\Controller\Cart\ChangePieceQuantityAction
     */
    public function testSuccessfulRemovePieceWithQuantityZero(): void
    {
        $pieceId = PieceId::create();
        $user = self::createUser(UserId::create(), Email::fromString('test@test.com'), );
        $piece = self::createPiece($user->getId(), $pieceId, ArticleNumber::create(), 'Example Piece');
        self::dispatchCommand(new ApprovePiece($pieceId, ''));
        self::dispatchCommand(new AddPieceToCart($user->getId(), $pieceId, 10));
        self::login('test@test.com');

        $response = self::post('/user/cart/change-piece-quantity', [
            'pieceId' => $piece->id,
            'quantity' => 0,
        ]);

        self::assertSuccessful();
        self::assertStatusCode(200);
        $this->assertMatchesSnapshot($response);
    }

    /**
     * @covers \App\Infrastructure\Symfony\Controller\Cart\ChangePieceQuantityAction
     * @covers \App\Infrastructure\Symfony\Validator\Cart\PieceExistsInCartValidator
     */
    public function testValidationFailedPieceIsNotInCart(): void
    {
        $pieceId = PieceId::create();
        $user = self::createUser(UserId::create(), Email::fromString('test@test.com'), );
        $piece = self::createPiece($user->getId(), $pieceId, ArticleNumber::create(), 'Example Piece');
        self::dispatchCommand(new ApprovePiece($pieceId, ''));
        self::dispatchCommand(new AddPieceToCart($user->getId(), $pieceId, 10));
        self::dispatchCommand(new RemovePieceFromCart($user->getId(), $pieceId));

        self::login('test@test.com');

        $response = self::post('/user/cart/change-piece-quantity', [
            'pieceId' => $piece->id,
            'quantity' => 10,
        ]);

        self::assertClientError(422);
        $this->assertMatchesSnapshot($response);
    }

    /**
     * @covers \App\Infrastructure\Symfony\Controller\Cart\ChangePieceQuantityAction
     * @covers \App\Infrastructure\Symfony\Validator\Cart\UserHasCartValidator
     */
    public function testValidationFailedUserHasNoCart(): void
    {
        $pieceId = PieceId::create();
        $user = self::createUser(UserId::create(), Email::fromString('test@test.com'), );
        $piece = self::createPiece($user->getId(), $pieceId, ArticleNumber::create(), 'Example Piece');
        self::dispatchCommand(new ApprovePiece($pieceId, ''));

        self::login('test@test.com');

        $response = self::post('/user/cart/change-piece-quantity', [
            'pieceId' => $piece->id,
            'quantity' => 10,
        ]);

        self::assertClientError(422);
        $this->assertMatchesSnapshot($response);
    }

    /**
     * @covers \App\Infrastructure\Symfony\Controller\Cart\AddPieceToCartAction
     * @covers \App\Infrastructure\Symfony\Validator\Piece\PieceIsApprovedValidator
     */
    public function testValidationFailedPieceNotApproved(): void
    {
        $pieceId = PieceId::create();
        $user = self::createUser(UserId::create(), Email::fromString('test@test.com'));
        $piece = self::createPiece($user->getId(), $pieceId, ArticleNumber::create(), 'Example Piece');
        self::dispatchCommand(new ApprovePiece($pieceId, ''));
        self::dispatchCommand(new AddPieceToCart($user->getId(), $pieceId, 10));
        self::dispatchCommand(new RejectPiece($pieceId, ''));

        self::login('test@test.com');

        $response = self::post('/user/cart/change-piece-quantity', [
            'pieceId' => $piece->id,
            'quantity' => 5,
        ]);

        self::assertClientError(422);
        $this->assertMatchesSnapshot($response);
    }

    /**
     * @covers \App\Infrastructure\Symfony\Controller\Cart\AddPieceToCartAction
     * @covers \App\Infrastructure\Symfony\Validator\Piece\PieceExistsValidator
     */
    public function testValidationFailedPieceNotFound(): void
    {
        $pieceId = PieceId::create();
        $user = self::createUser(UserId::create(), Email::fromString('test@test.com'));
        $piece = self::createPiece($user->getId(), $pieceId, ArticleNumber::create(), 'Example Piece');
        self::dispatchCommand(new ApprovePiece($pieceId, ''));
        self::dispatchCommand(new AddPieceToCart($user->getId(), $pieceId, 10));

        self::login('test@test.com');

        $response = self::post('/user/cart/change-piece-quantity', [
            'pieceId' => PieceId::create()->toString(),
            'quantity' => 10,
        ]);

        self::assertClientError(422);
        $this->assertMatchesSnapshot($response);
    }
}
