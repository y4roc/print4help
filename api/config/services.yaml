# This file is the entry point to configure your own services.
# Files in the packages/ subdirectory configure your dependencies.

# Put parameters here that don't need to change on each machine where the app is deployed
# https://symfony.com/doc/current/best_practices/configuration.html#application-related-configuration
parameters:

services:
    # default configuration for services in *this* file
    _defaults:
        autowire: true      # Automatically injects dependencies in your services.
        autoconfigure: true # Automatically registers your services as commands, event subscribers, etc.
        bind:
          Symfony\Component\Messenger\MessageBusInterface $commandBus: '@command.bus'

    _instanceof:
      App\Domain\CommandHandlerInterface:
        tags:
          - { name: messenger.message_handler, bus: command.bus }

    # makes classes in src/ available to be used as services
    # this creates a service per class whose id is the fully-qualified class name
    App\:
        resource: '../src/'
        exclude:
            - '../src/Symfony/Infrastructure/Kernel.php'
            - '../src/*/DependencyInjection/'
            - '../src/*/Entity/'
            - '../src/*/Tests/'

    # controllers are imported separately to make sure services can be injected
    # as action arguments even if you don't extend any base controller class
    App\Infrastructure\Symfony\Controller\:
        resource: '../src/Infrastructure/Symfony/Controller/'
        tags: ['controller.service_arguments']

    App\Infrastructure\Symfony\Listener\JsonExceptionListener:
      tags:
        - {name: kernel.event_listener, event: kernel.exception, method: onKernelException, priority: 200}

    App\Infrastructure\MongoDb\Connection:
      arguments:
        - '%env(MONGODB_DATABASE_URL)%'
        - '%env(MONGODB_DATABASE_NAME)%'

    # add more service definitions when explicit configuration is needed
    # please note that last definitions always *replace* previous ones

    App\Infrastructure\Symfony\Translation\Extractor\CategoryIdExtractor:
      tags:
        - { name: php_translation.visitor, type: php }

    App\Domain\Account\Repository\UserRepository: '@App\Infrastructure\Doctrine\ORM\Repository\UserRepository'
    App\Domain\Admin\Repository\AdminUserRepository: '@App\Infrastructure\Doctrine\ORM\Repository\AdminUserRepository'

    App\Domain\Market\Piece\PieceRepository: '@App\Infrastructure\EventSourcing\Repository\PieceRepository'
    App\Infrastructure\EventSourcing\Repository\PieceRepository:
      arguments:
        $repository: '@event_sourcing.piece_repository'

    App\Domain\Market\Cart\CartRepository: '@App\Infrastructure\EventSourcing\Repository\CartRepository'
    App\Infrastructure\EventSourcing\Repository\CartRepository:
      arguments:
        $repository: '@event_sourcing.cart_repository'

    App\Domain\Market\Inquiry\InquiryRepository: '@App\Infrastructure\EventSourcing\Repository\InquiryRepository'
    App\Infrastructure\EventSourcing\Repository\InquiryRepository:
      arguments:
        $repository: '@event_sourcing.inquiry_repository'
